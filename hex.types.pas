unit hex.types;

interface

uses
  qtx.sysutils;

type

  THexKeyMatrix = Array[0..11] of byte;

  THexRange = class
  private
    FLow:     int32;
    FHigh:    int32;
  protected
    function  GetValid: boolean;
    function  GetTop: int32;
    function  GetBottom: int32;
    procedure SetLow(const NewLow: int32);
    procedure SetHigh(const NewHigh: int32);
  public
    property  Left: int32 read FLow write SetLow;
    property  Right: int32 read FHigh write SetHigh;
    property  Top: int32 read GetTop;
    property  Bottom: int32 read GetBottom;
    property  Valid: boolean read GetValid;
    procedure Define(const FromValue, ToValue: int32);
    procedure Reset;

    function  Next(const Value: int32): int32;

    class function Range(const Left, Right: int32): THexRange;

    function  Within(const Value: int32): boolean;
    function  Inside(const Value: int32): boolean;
    constructor Create(const FromValue, ToValue: int32);
  end;

implementation

//############################################################################
// THexRange
//############################################################################

constructor THexRange.Create(const FromValue, ToValue: int32);
begin
  inherited Create;
  Define(FromValue, ToValue);
end;

class function THexRange.Range(const Left, Right: int32): THexRange;
begin
  result := THexRange.Create(Left,Right);
end;

procedure THexRange.Reset;
begin
  FLow := -1;
  FHigh := -1;
end;

procedure THexRange.Define(const FromValue, ToValue: int32);
begin
  if FromValue < ToValue then
  begin
    FLow := FromValue;
    FHigh := ToValue;
  end else
  if FromValue > ToValue then
  begin
    FLow := ToValue;
    FHigh := FromValue;
  end else
  begin
    FLow := FromValue;
    FHigh := ToValue;
  end;
end;

function THexRange.Inside(const Value: int32): boolean;
begin
  if GetValid then
  begin
    result := (Value>FLow) and (Value<FHigh);
  end else
  result := value = FLow;
end;

function THexRange.Next(const Value: int32): int32;
begin
  result := (value +1) mod Top;
end;

function THexRange.Within(const Value: int32): boolean;
begin
  if GetValid then
  begin
    result := (Value>=FLow) and (Value<=FHigh);
  end else
  result := value = FLow;
end;

procedure THexRange.SetLow(const NewLow: int32);
begin
  Define(NewLow, FHigh);
end;

procedure THexRange.SetHigh(const NewHigh: int32);
begin
  Define(FLow,NewHigh);
end;

function THexRange.GetValid: boolean;
begin
  result := (FLow < FHigh) or ( (FLow = FHigh) and (FLow>=0) );
end;

function THexRange.GetTop: int32;
begin
  result := FHigh + 1;
end;

function THexRange.GetBottom: int32;
begin
  result := FLow -1;
end;


end.
