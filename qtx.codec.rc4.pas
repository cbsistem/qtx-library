unit qtx.codec.rc4;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  W3C.TypedArray,
  qtx.codec,
  qtx.sysutils,
  qtx.memory,
  qtx.classes;

type

  TQTXCipherKeyRC4 = class
  private
    FReady:     boolean;
  public
    property    etShr: TManagedMemory;
    property    etMod: TManagedMemory;
    property    Ready: boolean read FReady;

    procedure   Build(const KeyData: TManagedMemory); overload;
    procedure   Build(const Stream: TStream); overload;
    procedure   Build(TextKey: string); overload;
    function    Clone: TQTXCipherKeyRC4;

    procedure   Reset;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TQTXCodecRC4Utils = class abstract
    class procedure EncDecStream(const Key: TQTXCipherKeyRC4; const FromStream, ToStream: TStream); overload; static;
    class function  EncDecStream(const Key: TQTXCipherKeyRC4; const FromStream: TStream): TStream; overload; static;
    class procedure EncDecBuffer(const Key: TQTXCipherKeyRC4; const Buffer: TManagedMemory); static;
    class function  EncDec(const Key: TQTXCipherKeyRC4; Bytes: TUint8Array): TUInt8Array; static;
  end;

  TQTXCodecRC4 = class(TQTXCodec)
  private
    FKey: TQTXCipherKeyRC4;
  protected
    // IMPLEMENTS :: ICodecProcess
    procedure EncodeData(const Source, Target: IManagedData); override;
    procedure DecodeData(const Source, Target: IManagedData); override;

    function MakeCodecInfo: TQTXCodecInfo; override;
  public
    property CipherKey: TQTXCipherKeyRC4 read FKey;

    function Encode(TextToEncode: string): string;
    function Decode(TextToDecode: string): string;

    constructor Create; override;
    destructor Destroy; override;
  end;

implementation

//############################################################################
// TQTXCodecRC4
//############################################################################

constructor TQTXCodecRC4.Create;
begin
  inherited;
  FKey := TQTXCipherKeyRC4.Create;
end;

destructor TQTXCodecRC4.Destroy;
begin
  FKey.free;
  inherited;
end;

function TQTXCodecRC4.MakeCodecInfo: TQTXCodecInfo;
begin
  result := TQTXCodecInfo.Create;

  var LVersion := TQTXCodecVersionInfo.Create(0, 1, 0);

  var LAccess := result as ICodecInfo;
  LAccess.SetName("RC4Codec");
  LAccess.SetMime("application/octet-stream");
  LAccess.SetVersion(LVersion);
  LAccess.SetDataFlow([cdRead, cdWrite]);

  LAccess.SetInput(cdBinary);   // Consumes binary
  LAccess.SetOutput(cdBinary);  // Emits binary
end;

function TQTXCodecRC4.Encode(TextToEncode: string): string;
begin
  var LCache := TString.EncodeUTF8(TextToEncode);
  LCache := TQTXCodecRC4Utils.EncDec(FKey, LCache);
  result := BytesToBase64(LCache);
end;

function TQTXCodecRC4.Decode(TextToDecode: string): string;
begin
  var LCache := Base64ToBytes(TextToDecode);
  LCache := TQTXCodecRC4Utils.EncDec(FKey, LCache);
  result := TString.DecodeUTF8(LCache);
end;

procedure TQTXCodecRC4.EncodeData(const Source, Target: IManagedData);
begin
  var LReader := TQTXReader.Create(Source);
  try
    var LWriter := TQTXWriter.Create(Target);
    try

      var LSize := LReader.Size;
      if LSize > 0 then
      begin
        var LCache := LReader.Read(LSize);
        LCache := TQTXCodecRC4Utils.EncDec(FKey, LCache);
        LWriter.Write(LCache);
      end;

    finally
      LWriter.free;
    end;
  finally
    LReader.free;
  end;
end;

procedure TQTXCodecRC4.DecodeData(const Source, Target: IManagedData);
begin
  EncodeData(Source, Target);
end;

//############################################################################
// TQTXCipherKeyRC4
//############################################################################

constructor TQTXCipherKeyRC4.Create;
begin
  inherited Create;
  etShr := TManagedMemory.Create;
  etMod := TManagedMemory.Create;
  etShr.Allocate(256);
  etMod.Allocate(256);
end;

destructor TQTXCipherKeyRC4.Destroy;
begin
  etShr.free;
  etMod.free;
  inherited;
end;

procedure TQTXCipherKeyRC4.Reset;
begin
  etShr.Release();
  etMod.Release();
  etShr.Allocate(256);
  etMod.Allocate(256);
  FReady := false;
end;

function TQTXCipherKeyRC4.Clone: TQTXCipherKeyRC4;
begin
  result := TQTXCipherKeyRC4.Create;
  result.etShr.assign(etShr as IManagedData);
  result.etMod.assign(etMod as IManagedData);
end;

procedure TQTXCipherKeyRC4.Build(const Stream: TStream);
begin
  Stream.Position := 0;
  var LBytesToRead := Stream.Size;
  if LBytesToRead >= 8 then
  begin
    // Key is cyclic, buffer is repeated to build
    // the 256 byte shift tables
    LBytesToRead := TInteger.EnsureRange(LBytesToRead, 8, 256);

    // Cache data in a buffer and encode
    var DataBlock := TManagedMemory.Create;
    try
      var Bytes := Stream.Read(LBytesToRead);
      var BytesRead := Bytes.Length;
      DataBlock.Append(Bytes);
      Build(DataBlock);
    finally
      DataBlock.free;
    end;
  end else
  raise ECodecError.Create('Failed to build cipher-key, invalid size (8 bytes lower boundary) error');
end;

procedure TQTXCipherKeyRC4.Build(const KeyData: TManagedMemory);
begin
  if FReady then
    Reset();

  if KeyData <> nil then
  begin
    if KeyData.Size > 0 then
    begin
      var J:=0;

      { Generate internal shift table based on key }
      for var I := 0 to 255 do
      begin
        etShr[i] := i;
        if J = KeyData.Size then
          J := 1
        else
          inc(J);
        etMod[i] := KeyData[J-1];
      end;

      { Modulate shift table }
      J := 0;
      for var i := 0 to 255 do
      begin
        j := (j + etShr[i] + etMod[i]) mod 256;
        var temp := etShr[i];
        etShr[i] := etShr[j];
        etShr[j] := Temp;
      end;

      FReady := True;
    end else
    raise ECodecError.Create('Failed to build cipher-key, buffer was empty error');
  end else
  raise ECodecError.Create('Failed to build cipher-key, buffer was nil error');
end;

procedure TQTXCipherKeyRC4.Build(TextKey: string);
begin
  TextKey := TextKey.trim();
  var KeyLen := TextKey.Length;
  if KeyLen > 0 then
  begin
    var DataBlock := TManagedMemory.Create;
    try
      DataBlock.Append( DataBlock.StringToBytes(TextKey) );
      Build(DataBlock);
    finally
      DataBlock.free;
    end;
  end else
  raise ECodecError.Create('Failed to build cipher-key, invalid string');
end;

//#############################################################################
// TQTXCodecRC4Utils
//#############################################################################

class function TQTXCodecRC4Utils.EncDecStream(const Key: TQTXCipherKeyRC4; const FromStream: TStream): TStream;
Begin
  result := TMemoryStream.Create;
  try
    EncDecStream(key, FromStream, result);
  except
    on e: exception do
    begin
      result.free;
      result := nil;
      raise;
    end;
  end;
  result.position := 0;
end;

class procedure TQTXCodecRC4Utils.EncDecBuffer(const Key: TQTXCipherKeyRC4; const Buffer: TManagedMemory);
begin
  if Buffer <> nil then
  begin
    if Buffer.Size > 0 then
    begin
      // Get content of buffer
      var LCache := Buffer.ToBytes();

      // Release current content
      Buffer.Release();

      // Encode cache and append
      Buffer.Append( EncDec(Key, LCache) );
    end else
    raise ECodecError.Create('Failed to encrypt, buffer was empty error');
  end else
  raise ECodecError.Create('Failed to encrypt, buffer was nil error');
end;

class function TQTXCodecRC4Utils.EncDec(const Key: TQTXCipherKeyRC4; Bytes: TUint8Array): TUint8Array;
begin
  var LByteLen := Bytes.Length;
  if LByteLen > 0 then
  begin
    if Key <> nil then
    begin
      if Key.Ready then
      begin
        // Clone encoding table
        var Spare := Key.Clone();
        try
          // init
          var i := 0;
          var j := 0;
          var xpos := 0;

          // Encrypt bytes
          while xpos < LByteLen do
          Begin
            i := (i + 1) mod 256;
            j := (j + Spare.etShr[i]) mod 256;
            var temp := Spare.etShr[i];
            Spare.etShr[i] := Spare.etShr[j];
            Spare.etShr[j] := temp;
            var t := (Spare.etShr[i] + (Spare.etShr[j] mod 256)) mod 256;
            var y := Spare.etShr[t];
            Bytes[xpos] := ( Bytes[xpos] xor y );
            inc(xpos);
          end;
          result := Bytes;
        finally
          Spare.free;
        end;
      end else
      raise ECodecError.Create('Failed to encrypt, cipher key not initialized error');
    end else
    raise ECodecError.Create('Failed to encrypt, cipher key not set error');
  end else
  raise ECodecError.Create('Failed to encrypt, buffer was empty error');
end;

class procedure TQTXCodecRC4Utils.EncDecStream(const Key: TQTXCipherKeyRC4; const FromStream, ToStream: TStream);
begin
  if FromStream <> nil then
  begin
    var LBytesToRead := FromStream.Size - FromStream.Position;
    if LBytesToRead > 0 then
    begin
      if ToStream <> nil then
      begin
        // Load bytes and encrypt ad-hoc
        var LByteCache := EncDec(Key, FromStream.Read(LBytesToRead));

        // Write cache to target
        ToStream.Write(LByteCache);

      end else
      raise ECodecError.Create('Failed to encrypt, target stream was nil error');
    end else
    raise ECodecError.Create('Failed to encrypt, source stream yield no data (position?) error');
  end else
  raise ECodecError.Create('Failed to encrypt, source stream was nil error');
end;

initialization
begin
  CodecManager.RegisterCodec(TQTXCodecRC4);
end;

end.
