unit qtx.btree;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils,
  qtx.classes,
  qtx.json;

type

  // BTree leaf object
  TQTXBTreeNode = class(JObject)
  public
    Identifier: integer;
    Data:       variant;
    Left:       TQTXBTreeNode;
    Right:      TQTXBTreeNode;
  end;
  TQTXBTreeNodeArray = array of TQTXBTreeNode;

  // This datatype is used when serializing a tree
  // for storage inside a stream or file
  TQTXBTreeFileItem = record
    fiIdent:  integer;
    fiData: variant;
  end;

  TQTXBTreeProcessCB = procedure (const Node: TQTXBTreeNode; var Cancel: boolean);

  EBTreeError = class(EException);

  TQTXBTree = class(TObject)
  private
    FRoot:  TQTXBTreeNode;
    FCurrent: TQTXBTreeNode;
  public
    property  Root: TQTXBTreeNode read FRoot;
    property  Empty: boolean read ( FRoot = nil );

    function  Add(const Ident: integer; const Data: variant): TQTXBTreeNode; overload; virtual;
    function  Add(const Ident: string; const Data: variant): TQTXBTreeNode; overload; virtual;

    function  &Contains(const Ident: integer): boolean; overload; virtual;
    function  &Contains(const Ident: string): boolean; overload; virtual;

    function  Remove(const Ident: integer): boolean; overload; virtual;
    function  Remove(const Ident: string): boolean; overload; virtual;

    function  Read(const Ident: integer): variant; overload; virtual;
    function  Read(const Ident: string): variant; overload; virtual;

    procedure Write(const Ident: string; const NewData: variant); overload; virtual;
    procedure Write(const Ident: integer; const NewData: variant); overload; virtual;

    procedure Clear; overload; virtual;
    procedure Clear(const Process: TQTXBTreeProcessCB); overload; virtual;

    function  ToArray: TVariantArray;
    function  ToString: string;
    function  Size: integer;

    function  ToJSON: string;
    procedure FromJSON(const Data: string);

    procedure SaveToStream(const Stream: TStream);
    procedure LoadFromStream(const Stream: TStream);

    procedure ForEach(const Process: TQTXBTreeProcessCB);

    //constructor Create;
    destructor Destroy; override;
  end;

implementation


(* These are the IO signatures used for storage *)
const
  CNT_BTREE_STREAM_HEADER = $BABE2109;
  CNT_BTREE_ITEM_HEADER   = $2109BABE;

//#############################################################################
// TQTXBTree
//#############################################################################

destructor TQTXBTree.Destroy;
begin
  Clear();
  inherited;
end;

procedure TQTXBTree.Clear;
begin
  // just decouple the references and let the GC handle it
  FCurrent := nil;
  FRoot := nil;
end;

procedure TQTXBTree.Clear(const Process: TQTXBTreeProcessCB);
begin
  ForEach(Process);
  Clear();
end;

function TQTXBTree.ToJSON: string;
begin
  if FRoot <> nil then
    result := JSON.stringify(FRoot);
end;

procedure TQTXBTree.FromJSON(const Data: string);
begin
  if not empty then
    Clear();
  FRoot := TQTXBTreeNode( JSON.Parse(data) );
end;

procedure TQTXBTree.SaveToStream(const Stream: TStream);
var
  LWriter: TQTXStreamWriter;
  LCache: array of TQTXBTreeFileItem;
begin

  ForEach( procedure (const Node: TQTXBTreeNode; var Cancel: boolean)
  var
    LItem: TQTXBTreeFileItem;
  begin
    LItem.fiIdent := Node.Identifier;
    LItem.fiData := Node.Data;
    LCache.add(LItem);
  end);

  LWriter := TQTXStreamWriter.Create(Stream);
  try
    LWriter.WriteUint32(CNT_BTREE_STREAM_HEADER);
    LWriter.WriteUint32(LCache.Count);

    for var Leaf in LCache do
    begin
      LWriter.WriteUint32(CNT_BTREE_ITEM_HEADER);

      var LRaw := LWriter.VariantToBytes(Leaf.fiData);
      LWriter.WriteUint32(Leaf.fiIdent);
      LWriter.WriteUint32(LRaw.Count);
      if LRaw.length > 0 then
        LWriter.Write(LRaw);
    end;

    LCache.Clear();
  finally
    LWriter.free;
  end;
end;

procedure TQTXBTree.LoadFromStream(const Stream: TStream);
//var
//  LRaw: TBytes;
begin
  if not Empty then
    Clear();

  var LReader := TQTXStreamReader.Create(Stream);
  try
    var LHead := LReader.ReadUInt32();
    if LHead = CNT_BTREE_STREAM_HEADER then
    begin
      var LCount := LReader.ReadUInt32();
      while LCount > 0 do
      begin
        var ElHead := LReader.ReadUInt32();
        if ElHead = CNT_BTREE_ITEM_HEADER then
        begin
          var LId := LReader.ReadUInt32();
          var LBytes := LReader.ReadUInt32();
          if LBytes > 0 then
          begin
            var LRaw := LReader.read(LBytes);
            var LValue := LReader.BytesToVariant(LRaw);
            self.Add(Lid, LValue);
          end;
        end else
        raise EBTreeError.CreateFmt('Invalid item header, expected %d not %d', [CNT_BTREE_ITEM_HEADER, ElHead]);
        dec(LCount);
      end;
    end else
    raise EBTreeError.CreateFmt('Invalid stream header, expected %d not %d', [CNT_BTREE_STREAM_HEADER,LHead]);
  finally
    LReader.free;
  end;
end;

function TQTXBTree.Size: integer;
var
  LCount: integer;
begin
  ForEach( procedure (const Node: TQTXBTreeNode; var Cancel: boolean)
    begin
      inc(LCount);
      Cancel  := false;
    end);
  result := LCount;
end;

function TQTXBTree.ToArray: TVariantArray;
var
  Data: TVariantArray;
begin
  ForEach( procedure (const Node: TQTXBTreeNode; var Cancel: boolean)
    begin
      Data.add(Node.data);
      Cancel := false;
    end);
  result := data;
end;

function TQTXBTree.ToString: string;
begin
  var LData := ToArray();
  for var el in LData do
  begin
    result += TVariant.AsString(el) + #13;
  end;
end;

function TQTXBTree.Add(const Ident: string; const Data: variant): TQTXBTreeNode;
begin
  result := Add( TString.CalcCRC(Ident), Data);
end;

function TQTXBTree.Add(const Ident: integer; const Data: variant): TQTXBTreeNode;
begin
  var LNode := new TQTXBTreeNode;
  LNode.Identifier := Ident;
  LNode.Data := data;

  if FRoot = nil then
    FRoot := LNode;

  FCurrent := FRoot;

  while true do
  begin
    if (Ident < FCurrent.Identifier) then
    begin
      if (FCurrent.left = nil) then
      begin
        FCurrent.left := LNode;
        break;
      end else
      FCurrent := FCurrent.left;
    end else
    if (Ident > FCurrent.Identifier) then
    begin
      if (FCurrent.right = nil) then
      begin
        FCurrent.right := LNode;
        break;
      end else
      FCurrent := FCurrent.right;
    end else
    break;
  end;
  result := LNode;
end;

function TQTXBTree.Read(const Ident: string): variant;
begin
  result := Read( TString.CalcCRC(Ident) );
end;

function TQTXBTree.Read(const Ident: integer): variant;
begin
  FCurrent := FRoot;
  while FCurrent <> nil do
  begin
    if (Ident < FCurrent.Identifier) then
      FCurrent := Fcurrent.left
    else
    if (Ident > Fcurrent.Identifier) then
      FCurrent := FCurrent.Right
    else
    begin
      result := FCUrrent.Data;
      break;
    end
  end;
end;

procedure TQTXBTree.Write(const Ident: string; const NewData: variant);
begin
  Write( TString.CalcCRC(Ident), NewData);
end;

procedure TQTXBTree.Write(const Ident: integer; const NewData: variant);
begin
  FCurrent := FRoot;
  while (FCurrent <> nil) do
  begin
    if (Ident < FCurrent.Identifier) then
      FCurrent := Fcurrent.left
    else
    if (Ident > Fcurrent.Identifier) then
      FCurrent := FCurrent.Right
    else
    begin
      FCurrent.Data := NewData;
      break;
    end
  end;
end;

function  TQTXBTree.&Contains(const Ident: string): boolean;
begin
  result := &Contains(TString.CalcCRC(Ident));
end;

function TQTXBTree.&Contains(const Ident: integer): boolean;
begin
  if FRoot <> nil then
  begin
    FCurrent := FRoot;

    while ( (not Result) and (FCurrent <> nil) ) do
    begin
      if (Ident < FCurrent.Identifier) then
        FCurrent := Fcurrent.left
      else
      if (Ident > Fcurrent.Identifier) then
        FCurrent := FCurrent.Right
      else
      begin
        Result := true;
      end
    end;
  end;
end;

function TQTXBTree.Remove(const Ident: string): boolean;
begin
  result := Remove(TString.CalcCRC(Ident));
end;

function TQTXBTree.Remove(const Ident: integer): boolean;
var
  LFound: boolean;
  LParent: TQTXBTreeNode;
  LReplacement,
  LReplacementParent: TQTXBTreeNode;
begin
  FCurrent := FRoot;

  while (not LFound) and (FCurrent<>nil) do
  begin
    if (Ident < FCurrent.Identifier) then
    begin
      LParent := FCurrent;
      FCurrent:= FCurrent.left;
    end else
    if (Ident > FCurrent.Identifier) then
    begin
      LParent := FCurrent;
      FCurrent := FCurrent.right;
    end else
      LFound := true;

    if (LFound) then
    begin
      var LChildCount := 0;
      if (FCurrent.left <> nil) then
        inc(LChildCount);
      if (FCurrent.right <> nil) then
        inc(LChildCount);

      if (FCurrent = FRoot) then
      begin
        case (LChildCOunt) of
        0:  begin
              FRoot := nil;
            end;
        1:  begin
              FRoot := if FCurrent.right = nil then
                FCurrent.left
              else
                FCurrent.Right;
            end;
        2:  begin
              LReplacement := FRoot.left;
              while (LReplacement.right <> nil) do
              begin
                LReplacementParent := LReplacement;
                LReplacement := LReplacement.right;
              end;

            if (LReplacementParent <> nil) then
            begin
              LReplacementParent.right := LReplacement.Left;
              LReplacement.right := FRoot.Right;
              LReplacement.left := FRoot.left;
            end else
            LReplacement.right := FRoot.right;
          end;
        end;

        FRoot := LReplacement;
      end else
      begin
        case LChildCount of
        0:  if (FCurrent.Identifier < LParent.Identifier) then
            Lparent.left  := nil else
            LParent.right := nil;
        1:  if (FCurrent.Identifier < LParent.Identifier) then
            begin
              if (FCurrent.Left = NIL) then
              LParent.left := FCurrent.Right else
              LParent.Left := FCurrent.Left;
            end else
            begin
              if (FCurrent.Left = NIL) then
              LParent.right := FCurrent.Right else
              LParent.right := FCurrent.Left;
            end;
        2:  begin
              LReplacement := FCurrent.left;
              LReplacementParent := FCurrent;

              while LReplacement.right <> nil do
              begin
                LReplacementParent := LReplacement;
                LReplacement := LReplacement.right;
              end;
              LReplacementParent.right := LReplacement.left;

              LReplacement.right := FCurrent.right;
              LReplacement.left := FCurrent.left;

              if (FCurrent.Identifier < LParent.Identifier) then
                LParent.left := LReplacement
              else
                LParent.right := LReplacement;
            end;
          end;
        end;
      end;
  end;

  result := LFound;
end;

procedure TQTXBTree.ForEach(const Process: TQTXBTreeProcessCB);

  function ProcessNode(const Node: TQTXBTreeNode): boolean;
  begin
    if Node <> nil then
    begin

      if node.left <> nil then
      begin
        result := ProcessNode(Node.left);
        if result then
          exit;
      end;

      Process(Node, result);
      if result then
        exit;

      if (Node.right <> nil) then
      begin
        result := ProcessNode(Node.right);
        if result then
          exit;
      end;
    end;
  end;

begin
  ProcessNode(FRoot);
end;



end.
