unit qtx.network.bindings;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils,
  qtx.classes,
  qtx.network.membership;

type

  // Forward declarations
  TQTXHostBindings  = class;
  TQTXHostBinding   = class;

  //Exception classes
  EQTXHostBindingError = class(EQTXException);

  TQTXHostBinding = class(TQTXOwnedLockedObject)
  private
    FHost:      string;
    FPort:      uint16;
    FExt:       boolean;
    FMembers:   TQTXHostMemberships;
  protected
    function    GetMemberships: IQTXHostMemberships;
    function    GetOwner: TQTXHostBindings; reintroduce;
    function    AcceptOwner(const CandidateObject: TObject): boolean; override;

    procedure   SetExclusive(const NewValue: boolean); virtual;
    procedure   SetPort(const NewPort: uint16); virtual;
    procedure   SetHost(const NewHost: string); virtual;
  public
    property    Owner: TQTXHostBindings read GetOwner;
    property    Port: uint16 read FPort write SetPort;
    property    Host: string read FHost write SetHost;
    property    Exclusive: boolean read FExt write SetExclusive;
    property    Memberships: IQTXHostMemberships read GetMemberships;

    constructor Create(const AOwner: TQTXHostBindings); reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TQTXHostBindingList = array of TQTXHostBinding;

  IQTXHostBindings = interface
    ['{2C227CD1-BC69-4107-A8FB-D0E3A50CD3C0}']
    function    GetBindingCount: int32;
    function    GetBinding(const index: int32): TQTXHostBinding;

    function    Add: TQTXHostBinding;
    function    AddEx(Addresse: string; Port: uint16): TQTXHostBinding;
    function    IndexOf(const Item: TQTXHostBinding): int32;

    function    FindByHost(Address: string): TQTXHostBinding;
    function    FindByPort(const Port: uint16): TQTXHostBinding;

    procedure   Delete(const index: int32);
    procedure   DeleteEx(const Item: TQTXHostBinding);
    procedure   Clear;
  end;

  TQTXHostBindings = class(TQTXLockedObject, IQTXHostBindings)
  private
    FItems:     TQTXHostBindingList;
    function    GetBindingCount: int32;
    function    GetBinding(const index: int32): TQTXHostBinding;

  protected
    procedure   ObjectLocked; override;
    procedure   ObjectUnLocked; override;

  public
    property    Locked;
    property    Items[const index: int32]: TQTXHostBinding read GetBinding; default;
    property    Count: int32 read GetBindingCount;

    function    Add: TQTXHostBinding; virtual;
    function    AddEx(Addresse: string; Port: uint16): TQTXHostBinding; virtual;
    function    IndexOf(const Item: TQTXHostBinding): int32; virtual;

    function    FindByHost(Address: string): TQTXHostBinding; virtual;
    function    FindByPort(const Port: uint16): TQTXHostBinding; virtual;

    procedure   Delete(const index: uint32); virtual;
    procedure   DeleteEx(const Item: TQTXHostBinding); virtual;
    procedure   Clear; virtual;

    destructor  Destroy; override;
  end;


implementation

//############################################################################
// TQTXHostBindings
//############################################################################

destructor TQTXHostBindings.Destroy;
begin
  if FItems.Count > 0 then
    Clear();
  inherited;
end;

procedure TQTXHostBindings.Clear;
begin
  if not Locked then
  begin
    try
      for var x := 0 to FItems.Count-1 do
      begin
        FItems[x].free;
        FItems[x] := nil;
      end;
    finally
      FItems.Clear();
    end;
  end else
  raise EQTXLockError.Create('Bindings cannot be altered while active error');
end;

function TQTXHostBindings.GetBindingCount: int32;
begin
  result := FItems.Count;
end;

function TQTXHostBindings.GetBinding(const index: int32): TQTXHostBinding;
begin
  result := FItems[index];
end;

function TQTXHostBindings.FindByPort(const Port: uint32): TQTXHostBinding;
begin
  for var x := 0 to FItems.Count-1 do
  begin
    var LItem := FItems[x];
    if LItem.Port = Port then
    begin
      result := LItem;
      break;
    end;
  end;
end;

function TQTXHostBindings.FindByHost(Address: String): TQTXHostBinding;
begin
  Address := Address.ToLower().Trim();
  if Address.length > 0 then
  begin
    for var x := 0 to FItems.Count-1 do
    begin
      var LItem := FItems[x];
      if LItem.FHost = Address then
      begin
        result := LItem;
        break;
      end;
    end;
  end;
end;

function TQTXHostBindings.IndexOf(const Item: TQTXHostBinding): int32;
begin
  if Item <> nil then
  begin
    result := FItems.indexOf(Item);
  end else
  raise EQTXHostBindingError.CreateFmt('%s failed, item was NIL error', [{$I %FUNCTION%}]);
end;

function TQTXHostBindings.Add: TQTXHostBinding;
begin
  if not Locked then
  begin
    result := TQTXHostBinding.Create(self);
    FItems.add(result);
  end else
  raise EQTXHostBindingError.CreateFmt('%s failed, bindings cannot be altered while active error', [{$I %FUNCTION%}]);
end;

function TQTXHostBindings.AddEx(Addresse: string; Port: uint16): TQTXHostBinding;
begin
  if not Locked then
  begin
    if FindByPort(Port) <> nil then
      raise EQTXHostBindingError.CreateFmt('Failed to add binding, port [%d] is already bound', [port]);

    result := TQTXHostBinding.Create(self);
    result.Host := Addresse;
    result.Port := port;
    FItems.add(result);
  end else
  raise EQTXHostBindingError.CreateFmt('%s failed, bindings cannot be altered while active error', [{$I %FUNCTION%}]);
end;

procedure TQTXHostBindings.Delete(const index: int32);
begin
  if not Locked then
  begin
    try
      FItems.Delete(Index);
    except
      on e: exception do
      raise EQTXHostBindingError.CreateFmt
      ("Failed to delete binding, system threw exception %s with message %s", [e.classname, e.message]);
    end;
  end else
  raise EQTXLockError.Create('Bindings cannot be altered while active error');
end;

procedure TQTXHostBindings.DeleteEx(const Item: TQTXHostBinding);
begin
  if Item <> nil then
  begin
    Delete( FItems.IndexOf(Item) );
  end else
  raise EQTXHostBindingError.CreateFmt('%s failed, item was NIL error', [{$I %FUNCTION%}]);
end;

procedure TQTXHostBindings.ObjectLocked;
begin
  for var x := 0 to FItems.Count-1 do
  begin
    var LAccess := (FItems[x] as IQTXLockObject);
    LAccess.DisableAlteration();
  end;
end;

procedure TQTXHostBindings.ObjectUnLocked;
begin
  for var x := 0 to FItems.Count-1 do
  begin
    var LAccess := (FItems[x] as IQTXLockObject);
    LAccess.EnableAlteration();
  end;
end;

//############################################################################
// TQTXHostBinding
//############################################################################

constructor TQTXHostBinding.Create(const AOwner: TQTXHostBindings);
begin
  inherited Create(AOwner);
  FMembers := TQTXHostMemberships.Create();
end;

destructor TQTXHostBinding.Destroy;
begin
  FMembers.clear();
  FMembers.free;
  inherited;
end;

function TQTXHostBinding.GetMemberships: IQTXHostMemberships;
begin
  result := FMembers as IQTXHostMemberships;
end;

function TQTXHostBinding.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := assigned(CandidateObject) and (CandidateObject is TQTXHostBindings);
end;

function TQTXHostBinding.GetOwner: TQTXHostBindings;
begin
  result := TQTXHostBindings(inherited Owner);
end;

procedure TQTXHostBinding.SetPort(const NewPort: uint16);
begin
  if NewPort <> FPort then
  begin
    if not Locked then
      FPort := NewPort
    else
      raise EQTXLockError.Create('Port property cannot be altered while active error');
  end;
end;

procedure TQTXHostBinding.SetExclusive(const NewValue: boolean);
begin
  if NewValue <> FExt then
  begin
    if not Locked then
      FExt := NewValue
    else
      raise EQTXLockError.Create('Exclusive property cannot be altered while active error');
  end;
end;

procedure TQTXHostBinding.SetHost(const NewHost: string);
begin
  if NewHost <> FHost then
  begin
    if not Locked then
      FHost := NewHost
    else
      raise EQTXLockError.Create('Property cannot be altered while active error');
  end;
end;

end.
