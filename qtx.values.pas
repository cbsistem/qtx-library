unit qtx.values;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses 
  qtx.sysutils;

type

  TValue        = class;
  TIntegerValue = class;
  TFloatValue   = class;
  TBoolValue    = class;
  TStringValue  = class;
  TPercentValue = class;
  TPixelValue   = class;
  TValueClass = class of TValue;

  IValueProvider = interface
    function  ValueAllowed(const NewValue: variant): boolean;
    procedure ValueChanged(const NewValue: variant);
  end;

  TPropertyBinding = class(TObject, IValueProvider)
  private
    FElement:   THandle;
    FPropName:  string;
    FValue:     TValue;

  protected
    // Implements:: IValueProvider
    function    ValueAllowed(const NewValue: variant): boolean; virtual;
    procedure   ValueChanged(const NewValue: variant); virtual;

  public
    property    Bound: boolean read ( FValue <> nil );
    property    BoundProperty: string read FPropName;
    procedure   BindToProperty(Element: THandle; PropertyName: string; ValueClass: TValueClass);
    procedure   BindToStyle(Element: THandle; PropertyName: string; ValueClass: TValueClass);
    procedure   UnBind;

    property    Value: TValue read FValue;

    destructor  Destroy; override;
  end;

  TValue = class(TObject)
  private
    FValue:     variant;
    FProvider:  IValueProvider;
    FUpdates:   integer;
    FCopy:      variant;
  protected
    function    GetAsInt: integer;
    function    GetAsFloat: float;
    function    GetAsStr: string;
    function    GetAsBool: boolean;

    procedure   SetAsInt(const NewValue: integer);
    procedure   SetAsFloat(const NewValue: float);
    procedure   SetAsStr(const NewValue: string);
    procedure   SetAsBool(const NewValue: boolean);

    function    GetValue: variant; virtual;
    procedure   SetValue(const NewValue: variant); virtual;
  public
    property    Value: variant read GetValue write SetValue;
    property    AsInteger: integer read GetAsInt write SetAsInt;
    property    AsFloat: float read GetAsFloat write SetAsFloat;
    property    AsBool: boolean read GetAsBool write SetAsBool;
    property    AsString: string read GetAsStr write SetAsStr;

    property    Updating: boolean read (FUpdates > 0);
    procedure   BeginUpdate;
    procedure   EndUpdate;

    function    ToString: string; virtual;

    constructor Create(const Provider: IValueProvider);
  end;

  TIntegerValue = class(TValue)
  protected
    function  GetValue: integer; reintroduce; virtual;
    procedure SetValue(const NewValue: integer); reintroduce; virtual;
  public
    property  Value: integer read GetValue write SetValue;

    function  ToPercent: string;
    function  ToPx: string;
  end;

  TFloatValue = class(TValue)
  protected
    function  GetValue: float; reintroduce; virtual;
    procedure SetValue(const NewValue: float); reintroduce; virtual;
  public
    property  Value: float read GetValue write SetValue;
  end;

  TBoolValue = class(TValue)
  protected
    function  GetValue: boolean; reintroduce; virtual;
    procedure SetValue(const NewValue: boolean); reintroduce; virtual;
  public
    property  Value: boolean read GetValue write SetValue;
  end;

  TStringValue = class(TValue)
  protected
    function  GetValue: string; reintroduce; virtual;
    procedure SetValue(const NewValue: string); reintroduce; virtual;
  public
    property  Value: string read GetValue write SetValue;
  end;

  TPercentValue = class(TIntegerValue)
  protected
    procedure SetValue(const NewValue: integer); override;
  public
    function  ToString: string; override;
  end;

  TPixelValue = class(TIntegerValue)
  public
    function  ToString: string; override;
  end;


implementation

//############################################################################
// TPixelValue
//############################################################################

destructor TPropertyBinding.Destroy;
begin
  if Bound then
    UnBind();
  inherited;
end;

function TPropertyBinding.ValueAllowed(const NewValue: variant): boolean;
begin
  result := true;
end;

procedure TPropertyBinding.ValueChanged(const NewValue: variant);
begin
  writeln("value changed to:" + TVariant.AsString(NewValue));
  FElement.style[FPropName] := TVariant.AsString(NewValue) + 'px';
end;

procedure TPropertyBinding.BindToStyle(Element: THandle; PropertyName: string; ValueClass: TValueClass);
begin
  if Bound then
    UnBind();

  if (Element) then
  begin
    PropertyName := propertyname.trim();
    if PropertyName.length > 0 then
    begin
      if Element.style.hasOwnProperty(PropertyName) then
      begin
        FElement := Element;
        FPropName := PropertyName;

        if ValueClass <> nil then
          FValue := ValueClass.Create(self as IValueProvider)
        else
          begin
            FElement := nil;
            FPropName := '';
            raise EException.Create('Failed to bind to element, value-class was nil error');
          end;

      end else
      raise EException.CreateFmt('Failed to bind to element, property [%s] is not exposed by the object', [PropertyName]);
    end else
    raise EException.Create('Failed to bind to element, property-name was empty error');
  end else
  raise EException.Create('Failed to bind to element, handle was nil or unassigned error');
end;

procedure TPropertyBinding.BindToProperty(Element: THandle; PropertyName: string; ValueClass: TValueClass);
begin
  if Bound then
    UnBind();

  if (Element) then
  begin
    PropertyName := propertyname.trim();
    if PropertyName.length > 0 then
    begin
      if Element.hasOwnProperty(propertyName) then
      begin
        FElement := Element;
        FPropName := PropertyName;

        if ValueClass <> nil then
          FValue := ValueClass.Create(self as IValueProvider)
        else
          begin
            FElement := nil;
            FPropName := '';
            raise EException.Create('Failed to bind to element, value-class was nil error');
          end;

      end else
      raise EException.CreateFmt('Failed to bind to element, property [%s] is not exposed by the object', [PropertyName]);
    end else
    raise EException.Create('Failed to bind to element, property-name was empty error');
  end else
  raise EException.Create('Failed to bind to element, handle was nil or unassigned error');
end;

procedure TPropertyBinding.UnBind;
begin
  if FValue <> nil then
  begin
    try
      try
        FValue.free;
      finally
        FValue := nil;
        FElement := unassigned;
        FPropName := '';
      end;
    except
      // sink any exceptions
      on e: exception do;
    end;
  end;
end;


//############################################################################
// TPixelValue
//############################################################################

function TPixelValue.ToString: string;
begin
  result := inherited ToString();
  result += 'px';
end;

//############################################################################
// TPercentValue
//############################################################################

procedure TPercentValue.SetValue(const NewValue: integer);
begin
  inherited SetValue( if NewValue < 0 then 0 else if value > 100 then 100 else NewValue );
end;

function TPercentValue.ToString: string;
begin
  result := inherited ToString() + '%';
end;

//############################################################################
// TBoolValue
//############################################################################

function TBoolValue.GetValue: boolean;
begin
  result := TVariant.AsBool( inherited GetValue() );
end;

procedure TBoolValue.SetValue(const NewValue: boolean);
begin
  inherited SetValue(NewValue);
end;

//############################################################################
// TStringValue
//############################################################################

function TStringValue.GetValue: string;
begin
  result := TVariant.AsString( inherited GetValue() );
end;

procedure TStringValue.SetValue(const NewValue: string);
begin
  inherited SetValue(NewValue);
end;

//############################################################################
// TFloatValue
//############################################################################

function TFloatValue.GetValue: float;
begin
  result := TVariant.AsFloat( inherited GetValue() );
end;

procedure TFloatValue.SetValue(const NewValue: float);
begin
  inherited SetValue(NewValue);
end;

//############################################################################
// TIntegerValue
//############################################################################

function TIntegerValue.GetValue: integer;
begin
  result := TVariant.AsInteger( inherited GetValue() );
end;

procedure TIntegerValue.SetValue(const NewValue: integer);
begin
  inherited SetValue(NewValue);
end;

function TIntegerValue.ToPercent: string;
begin
  var LTemp := if Value < 0 then 0 else if value > 100 then 100 else Value;
  result := LTemp.ToString();
  result += '%';
end;

function TIntegerValue.ToPx: string;
begin
  result := ToString();
  result += 'px';
end;

//############################################################################
// TValue
//############################################################################

constructor TValue.Create(const Provider: IValueProvider);
begin
  inherited Create;

  if Provider = nil then
    raise EException.Create('Failed to create value object, provider was nil error')
  else
    FProvider := Provider;
end;

procedure TValue.BeginUpdate;
begin
  if FUpdates = 0 then
  begin
    FCopy := FValue;
    inc(FUpdates);
  end else
  inc(FUpdates);
end;

procedure TValue.EndUpdate;
begin
  if FUpdates > 0 then
  begin
    dec(FUpdates);
    if FUpdates = 0 then
    begin
      if FValue <> FCopy then
        FProvider.ValueChanged(FValue);
      FCopy := unassigned;
    end;
  end;
end;

function TValue.ToString: string;
begin
  // Default to JSVM native implementation
  if (FValue) then
    result := FValue.toString();
end;

function TValue.GetValue: variant;
begin
  result := FValue;
end;

procedure TValue.SetValue(const NewValue: variant);
begin
  if NewValue <> FValue then
  begin
    if FProvider.ValueAllowed(NewValue) then
    begin
      FValue := NewValue;

      if not Updating then
        FProvider.ValueChanged(NewValue);
    end;
  end;
end;

function TValue.GetAsInt: integer;
begin
  result := TVariant.AsInteger(FValue);
end;

procedure TValue.SetAsInt(const NewValue: integer);
begin
  FValue := NewValue;
end;

function TValue.GetAsFloat: float;
begin
  result := TVariant.AsFloat(FValue);
end;

procedure TValue.SetAsFloat(const NewValue: float);
begin
  FValue := NewValue;
end;

function TValue.GetAsStr: string;
begin
  result := TVariant.AsString(FValue);
end;

procedure TValue.SetAsStr(const NewValue: string);
begin
  FValue := NewValue;
end;

function TValue.GetAsBool: boolean;
begin
  result := TVariant.AsBool(FValue);
end;

procedure TValue.SetAsBool(const NewValue: boolean);
begin
  FValue := NewValue;
end;

end.
