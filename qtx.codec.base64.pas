unit qtx.codec.base64;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  W3C.TypedArray,
  qtx.codec,
  qtx.sysutils,
  qtx.memory,
  qtx.classes;

type

  TBase64Codec = class(TQTXCodec)
  protected
    (* IMPLEMENTS :: ICodecProcess *)
    procedure EncodeData(const Source, Target: IManagedData); override;
    procedure DecodeData(const Source, Target: IManagedData); override;

    function MakeCodecInfo: TQTXCodecInfo; override;
  public
    function Encode(TextToEncode: string): string; virtual;
    function Decode(TextToDecode: string): string; virtual;
  end;

implementation

//############################################################################
// TBase64Codec
//############################################################################

function TBase64Codec.MakeCodecInfo: TQTXCodecInfo;
begin
  result := TQTXCodecInfo.Create;

  var LVersion := TQTXCodecVersionInfo.Create(0, 1, 0);
  LVersion.viMajor := 0;
  LVersion.viMinor := 1;
  LVersion.viRevision := 0;

  var LAccess := result as ICodecInfo;
  LAccess.SetName("Base64Codec");
  LAccess.SetMime("text/base64");
  LAccess.SetVersion(LVersion);
  LAccess.SetDataFlow([cdRead, cdWrite]);

  LAccess.SetInput(cdText);     // Consumes text
  LAccess.SetOutput(cdBinary);  // Emits binary
end;

function TBase64Codec.Encode(TextToEncode: string): string;
begin
  result := TBase64EncDec.StringToBase64(TextToEncode);
end;

function TBase64Codec.Decode(TextToDecode: string): string;
begin
  result := TBase64EncDec.Base64ToString(TextToDecode);
end;

procedure TBase64Codec.EncodeData(const Source, Target: IManagedData);
{ var
  LReader: TStreamReader;
  LWriter: TStreamWriter; }
begin
  { LReader := TStreamReader.Create(Source);
  try
    LWriter := TStreamWriter.Create(Target);
    try
      var BytesToRead := LReader.Size - LReader.Position;
      if BytesToRead > 0 then
      begin
        var RawBytes := LReader.Read(BytesToRead);
        var Base64Text := TBase64EncDec.BytesToBase64(RawBytes);
        LWriter.WriteStr(Base64Text);
      end;
    finally
      LWriter.free;
    end;
  finally
    LReader.free;
  end; }
end;

procedure TBase64Codec.DecodeData(const Source, Target: IManagedData);
{var
  LReader: TReader;
  LWriter: TWriter; }
begin
  { LReader := TReader.create(Source);
  try
    LWriter := TWriter.Create(Target);
    try
      var BytesToRead := LReader.Size - LReader.Position;
      var RawText := LReader.ReadStr(BytesToRead);
      if RawText.Length > 0 then
      begin
        var RawBytes := TBase64EncDec.Base64ToBytes(RawText);
        if RawBytes.Length > 0 then
          LWriter.Write(RawBytes);
      end;
    finally
      LWriter.free;
    end;
  finally
    LReader.free;
  end; }
end;

initialization
begin
  CodecManager.RegisterCodec(TBase64Codec);
end;

end.
