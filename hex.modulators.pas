unit hex.modulators;

interface

uses
  qtx.sysutils;

type
  THexModulationTable = array [0..255] of int32;

  THexNumberModulator = class(TObject)
  private
    FCache:     THexModulationTable;
  protected
    function    BuildNumberSeries: THexModulationTable; virtual; abstract;
  public
    property    Data[index: int32]: uint8 read (FCache[index]) write (FCache[index] := Value); default;
    property    Count: int32 read (FCache.Count);
    function    ToString: string; virtual;
    function    ToNearest(const Value: int32): int32; virtual;
    constructor Create; virtual;
  end;

  THexLucasModulator = class(THexNumberModulator)
  protected
    function BuildNumberSeries: THexModulationTable; override;
    function DoFindNearest(const Value: int32): int32; virtual;
  public
    function ToNearest(const Value: int32): int32; override;
  end;

  THexFibonacciModulator = class(THexNumberModulator)
  protected
    function  BuildNumberSeries: THexModulationTable; override;
  public
    function ToNearest(const Value: int32): int32; override;
  end;

  THexLeonardoModulator = class(THexNumberModulator)
  protected
    function  BuildNumberSeries: THexModulationTable;override;
  public
    function ToNearest(const Value: int32): int32; override;
  end;

implementation

//############################################################################
// THexLeonardoModulator
//############################################################################

function THexLeonardoModulator.BuildNumberSeries: THexModulationTable;
begin
  result[low(result)] := 1;
  result[low(result)+1] := 1;
  for var x := low(result) + 2 to high(result) do
  begin
    var a:= result[x-2];
    var b:= result[x-1];
    result[x] := a + b + 1;
  end;
end;

function THexLeonardoModulator.ToNearest(const Value: int32): int32;
begin
  if (value = 1) then
    result := 1
  else
  if (value = 2) then
    result := 1
  else
    result := ToNearest(value-1) + ToNearest(value-2) + 1;
end;

//############################################################################
// THexFibonacciModulator
//############################################################################

function THexFibonacciModulator.BuildNumberSeries: THexModulationTable;
begin
  result[low(result)] := 0;
  result[low(result)+1] := 1;
  for var x := low(result)+2 to high(result) do
  begin
    var a:= result[x-2];
    var b:= result[x-1];
    result[x] := a + b;
  end;
end;

function THexFibonacciModulator.ToNearest(const Value: int32): int32;
var
  LForward: int32;
  LBackward: int32;
  LForwardDistance: int32;
  LBackwardsDistance: int32;
begin
  (* Note: the round() function always rounds upwards to the closest
           whole number. Which in a formula can result in the routine
           returning the next number even though the previous number
           is closer.
           To remedy this we do a distance compare between "number" and
           "number-1", so make sure we pick the closest match in distance *)
  LForward := round( TFloat.Power( ( (1 + SQRT(5)) / 2), value) / SQRT(5) );
  LBackward := round( TFloat.Power( ( (1 + SQRT(5)) / 2), value-1) / SQRT(5) );

  if LForward <> LBackward then
  begin
    LForwardDistance := LForward - value;
    LBackwardsDistance := Value - LBackward;

    if (LForwardDistance < LBackwardsDistance) then
    result := LForward else
    result := LBackward;
  end else
  result := LForward;
end;

//############################################################################
// THexLucasModulator
//############################################################################

function THexLucasModulator.DoFindNearest(const Value: int32): int32;
begin
  var LBestDiff := MAXINT;
  var LMatch := -1;
  var c: int32;

  var a := 2;
  var b := 1;
  repeat
    c := a + b;
    var LDistance := c - value;
    if (LDistance >= 0) then
    begin
      if (LDistance < LBestDiff) then
      begin
        LBestDiff := LDistance;
        LMatch := c;
      end;
    end;

    a := b;
    b := c;
  until (c > value) or (LBestDiff = 0);

  if (LMatch > 0) then
    result := LMatch
  else
    result := value;
end;

function THexLucasModulator.ToNearest(const Value: int32): int32;
var
  LForward: int32;
  LBackward: int32;
  LForwardDistance: int32;
  LBackwardsDistance: int32;
begin
  (* Note: Lucas is a bit harder to work with than fibonacci. So instead
     of using a formula we have to actually search a bit.
     It is however important to search both ways, since the distance
     backwards can be closer to the number than forward *)
  LForward := DoFindNearest(Value);
  LBackward := DoFindNearest(value-1);

  if LForward <> LBackward then
  begin
    LForwardDistance := LForward - value;
    LBackwardsDistance := Value - LBackward;

    if (LForwardDistance < LBackwardsDistance) then
    result := LForward else
    result := LBackward;
  end else
  result := LForward;
end;

function THexLucasModulator.BuildNumberSeries: THexModulationTable;
var
  x: int32;
  a,b : int32;
begin
  result[low(result)] := 2;
  result[low(result)+1] := 1;
  for x:=low(result)+2 to high(result) do
  begin
    a:= result[x-2];
    b:= result[x-1];
    result[x] := a + b;
  end;
end;

//############################################################################
// THexNumberModulator
//############################################################################

constructor THexNumberModulator.Create;
begin
  inherited create;
  FCache := BuildNumberSeries();
end;

function THexNumberModulator.ToNearest(const Value: int32): int32;
begin
  if Value > 1 then
    result := ToNearest( Value - 1 ) + ToNearest( Value - 2 )
  else
  if Value = 0 then
    result := 2
  else
    result := 1;
end;

function THexNumberModulator.ToString: string;
begin
  if FCache.length > 0 then
  begin
    for var x := low(FCache) to high(FCache) do
    begin
      if ( x < high(FCache) ) then
        result := result + FCache[x].toString() + ', '
      else
        result := result + FCache[x].toString();
    end;
  end;
end;

end.
