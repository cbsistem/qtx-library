unit qtx.orm;

interface

uses
  System.Types,
  System.Types.Convert,
  System.db,
  System.dataset;

type

  TQTXORMReadFieldsCB   = procedure (TagValue: variant; const Error: exception);
  TQTXORMWriteFieldsCB  = procedure (TagValue: variant; const Error: exception);

  TQTXORMObject = class(TObject)
  private
    FChanged:   boolean;
    FDB:        TW3Database;
    FTable:     string;
  protected
    procedure   SetChanged(const Value: boolean);
  public
    property    Database: TW3Database read FDB;
    property    TableName: string read FTable;
    property    Changed: boolean read FChanged;
    procedure   Read(const TagValue: variant; const CB: TQTXORMReadFieldsCB); virtual; abstract;
    procedure   Write(const TagValue: variant; const CB: TQTXORMWriteFieldsCB); virtual; abstract;
    procedure   Reset; virtual; abstract;
    constructor Create(const DB: TW3Database; const TableName: string); virtual;
  end;


implementation

//#############################################################################
// TQTXORMObject
//#############################################################################

constructor TQTXORMObject.Create(const DB: TW3Database; const TableName: string);
begin
  inherited Create;
  FDB := DB;
  FTable := TableName.trim();
end;

procedure  TQTXORMObject.SetChanged(const Value: boolean);
begin
  FChanged := Value;
end;


end.
