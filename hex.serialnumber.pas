unit hex.serialnumber;

interface

uses
  qtx.sysutils,
  //system.types,
  //system.types.convert,
  hex.types,
  hex.partition,
  hex.obfuscation,
  hex.serialmatrix,
  hex.modulators;

type


  THexCustomSerialNumber = class(TObject)
  public
    function Validate(SerialNumber: string): boolean;virtual;abstract;
  end;

  THexIronWoodSerialNumber = class(THexCustomSerialNumber)
  private
    FGateCount:   int32;
    FRootData:    THexKeyMatrix;
    FModulator:   THexNumberModulator;
    FObfuscator:  THexObfuscation;
    FPartitions:  THexIronwoodPartitions;
  protected
    function    CanUseRootData: boolean;
    procedure   SetGateCount(const Value: int32); virtual;
    procedure   RebuildPartitionTable;
  public
    property    Modulator: THexNumberModulator read FModulator write FModulator;
    property    Obfuscaton: THexObfuscation read FObfuscator write FObfuscator;
    property    Partitions: THexIronwoodPartitions read FPartitions;
    procedure   Build(const Rootkey: THexKeyMatrix);
    function    GetSignature: string;

    function    Validate(SerialNumber: string): boolean; override;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

//############################################################################
// THexIronWoodSerialNumber
//############################################################################

constructor THexIronWoodSerialNumber.Create;
begin
  inherited create;
  // Out of 256 gates, we allow 64 to be actually used
  FGateCount := 64;
end;

destructor THexIronWoodSerialNumber.Destroy;
begin
  try
    for var x := low(FPartitions) to high(FPartitions) do
    begin
      try
        FPartitions[x].free;
      except
        on e: exception do;
      end;
      FPartitions[x] := nil;
    end;
  finally
    FPartitions.clear;
  end;
  inherited;
end;

function THexIronWoodSerialNumber.CanUseRootData: boolean;
begin
  var LFilled := 0;
  var LTop := FRootData.length;
  dec(LTop, 2);

  for var x := low(FRootData) to high(FRootData) do
  begin
    if FRootData[x] <> 0 then
      inc(LFilled);
  end;

  // No more than 2 slots can have zero.
  result := ( LFilled >= LTop );
end;

procedure THexIronWoodSerialNumber.SetGateCount(const Value: int32);
begin
  FGateCount := TInteger.EnsureRange(Value, 16, 128);
end;

procedure THexIronWoodSerialNumber.RebuildPartitionTable;
begin
  FPartitions.Clear();
  if CanUseRootData() then
  begin
    for var x := 1 to 12 do
    begin
      var LPartition := THexIronwoodPartition.Create(FGateCount);
      LPartition.Modulator := Modulator;
      FPartitions.Add(LPartition);

      // Only issue a rebuild if a modulator has been assigned
      if (Modulator <> nil) then
      begin
        // Rebuild gate array
        LPartition.Build( FRootData[x-1] );
      end else
      raise Exception.Create('No modulator assigned, failed to rebuild partition-table error');
    end;
  end else
  raise Exception.Create('Unsuitable root-key, failed to rebuild partition-table error');
end;

procedure THexIronWoodSerialNumber.Build(const Rootkey: THexKeyMatrix);
begin
  FRootData := Rootkey;
  RebuildPartitionTable();
end;

function THexIronWoodSerialNumber.GetSignature: string;
begin
  if FPartitions.Count > 0 then
  begin
    for var x := low(FPartitions) to high(FPartitions) do
    begin
      result += FPartitions[x].ToString();
      if x < high(FPartitions) then
        result += #13;
    end;
  end;
end;

function THexIronWoodSerialNumber.Validate(SerialNumber: string): boolean;
begin
  SerialNumber := StrReplace(SerialNumber,'-','');
  if length(SerialNumber) = FPartitions.length * 2 then
  begin
    var x := 1;
    var LVector := 0;
    var LLock := 0;

    while serialnumber.length > 0 do
    begin
      var LBlock := '$' + copy(Serialnumber, 1, 2);
      delete(serialnumber, 1, 2);
      inc(x, 2);

      var LRaw := TInteger.EnsureRange( TString.HexStrToInt(LBlock), 0, 255);

      if FPartitions[LVector].Valid(LRaw) then
        inc(LLock);

      inc(LVector);
    end;

    result := LLock = FPartitions.Length;
  end;
end;


end.
