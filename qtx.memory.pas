unit qtx.memory;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  W3C.TypedArray,
  qtx.Sysutils;

type

  EManagedMemory = class(Exception);

  TManagedMemory  = class;

  IManagedData = interface
    function  ToBytes: TUInt8Array;
    procedure FromBytes(const Bytes: TUInt8Array);
    function  GetSize: int64;
    function  GetPosition: int64;
    function  ReadBuffer(Offset, ReadLen: integer): TUInt8Array;
    procedure WriteBuffer(Offset: integer; Data: TUInt8Array);
    procedure Grow(const BytesToGrow: integer);
    procedure Shrink(const BytesToShrink: integer);
    procedure Assign(const Memory: IManagedData);
    procedure Append(const Data: TUInt8Array);
  end;

  TManagedMemory = class(TDatatypeConverter, IManagedData)
  private
    FBuffer:    JArrayBuffer;
    FView:      JDataView;
    FArray:     TStdBufferType;
  protected
    procedure   BeforeRelease; virtual;
    procedure   BeforeAllocate(var NewSize: integer); virtual;
    procedure   AfterAllocate; virtual;
    procedure   AfterRelease; virtual;

    function    GetSize: int64; virtual;
    function    GetPosition: int64; virtual;
  public
    property    Handle: TMemoryHandle read (variant(FArray));
    property    &Empty: boolean read (FBuffer = nil);
    property    Size: int64 read (if FBuffer <> nil then FBuffer.byteLength else 0);
    property    View: JDataView read FView;
    property    Buffer: JArrayBuffer read FBuffer;
    property    TypedArray: JUint8Array read FArray;

    property    Item[const Index: int64]: byte
                read ( FArray.Items[index] )
                write( FArray.Items[index] := Value ); default;

    procedure   Allocate(BytesToAllocate: int64);
    procedure   Release;

    procedure   Grow(const BytesToGrow: integer);
    procedure   Shrink(const BytesToShrink: integer);

    procedure   ScaleTo(NewSize: int64);

    procedure   Assign(const Memory: IManagedData);
    procedure   Append(const Data: TUInt8Array);

    function    ToBytes: TUInt8Array;
    procedure   FromBytes(const Bytes: TUInt8Array);

    function    Clone: TManagedMemory;

    procedure   FillByte(Offset: int64; ByteLen: integer; Value: byte);

    procedure   Push(const Data: TUInt8Array);
    function    Pop(BlockSize: integer): TUInt8Array;

    function    ReadBuffer(Offset, ReadLen: int64): TUInt8Array;
    procedure   WriteBuffer(Offset: int64; Data: TUInt8Array);

    destructor  Destroy; override;
  published
    property    OnMemoryAllocated: TNotifyEvent;
    property    OnMemoryReleased: TNotifyEvent;
  end;

  TTypedDataFactory = class static
  private
    class var __Resolved: boolean = false;
    class var __SupportCTA: boolean = false;

    // function that checks if the system supports "clamped typed arrays" (CTA)
    class function  QuerySystemCTASupport: boolean; static;
  public
    class property  SupportCTA: boolean read QuerySystemCTASupport;
    class function  AllocUint08(const Size: integer): JIntegerTypedArray; overload;
    class function  AllocUInt08(const DefaultData: TUint8Array): JIntegerTypedArray; overload;
  end;

implementation

// This methods checks if the JSVM supports the UInt8ClampedArray type.
// Sadly not all browsers have this native JSVM object. Safari for iOS did not
// support it when I last checked (Q3:2018)
class function TTypedDataFactory.QuerySystemCTASupport: boolean;
begin
  if __Resolved then
    result := __SupportCTA
  else
  begin
    __Resolved := true;

    var LTemp: THandle;
    try
      LTemp  := new JUint8ClampedArray(10);
    except
      __SupportCTA := false;
      exit(false);
    end;

    __SupportCTA := true;
    result := __SupportCTA;
  end;
end;

class function TTypedDataFactory.AllocUint08(const Size: integer): JIntegerTypedArray;
begin
  case __SupportCTA of
  true:   result := JIntegerTypedArray( new JUint8ClampedArray(Size) );
  false:  result := JIntegerTypedArray( new JUint8Array(Size) );
  end;
end;

class function TTypedDataFactory.AllocUInt08(const DefaultData: TUint8Array): JIntegerTypedArray;
begin
  case __SupportCTA of
  true:   result := JIntegerTypedArray( new JUint8ClampedArray(DefaultData) );
  false:  result := JIntegerTypedArray( new JUint8Array(DefaultData) );
  end;
end;

//############################################################################
// TManagedMemory
//############################################################################

destructor TManagedMemory.Destroy;
begin
  if FBuffer <> nil then
    Release();
  inherited;
end;

function TManagedMemory.GetPosition: int64;
begin
  result := 0;
end;

function TManagedMemory.GetSize: int64;
begin
  result := if FBuffer <> nil then FBuffer.byteLength else 0;
end;

procedure TManagedMemory.Shrink(const BytesToShrink: integer);
begin
  if BytesToShrink > 0 then
  begin
    var LNewSize := Size - BytesToShrink;

    if LNewSize <= 0 then
    begin
      Release();
      exit;
    end;

    var LCache := ReadBuffer(0, LNewSize);
    Allocate(LNewSize);
    WriteBuffer(0, LCache);
  end else
  raise EManagedMemory.Create('Invalid shrink value, expected 1 or above error');
end;

function TManagedMemory.Clone: TManagedMemory;
begin
  result := TManagedMemory.Create;
  if not &Empty then
    result.Assign(self as IManagedData);
end;

procedure TManagedMemory.Assign(const Memory: IManagedData);
begin
  if Memory = nil then
  begin
    Release();
    exit;
  end;

  var LSize := Memory.GetSize();
  if LSize < 1 then
  begin
    Release();
    exit;
  end;

  Allocate(LSize);
  FArray.Set(Memory.ToBytes(), 0);
end;

procedure TManagedMemory.Append(const Data: TUInt8Array);
begin
  if Data.length > 0 then
  begin
    var LOffset := if (FBuffer <> nil) then Size else 0;
    Grow(Data.length);
    WriteBuffer(LOffset, Data);
  end;
end;

procedure TManagedMemory.Push(const Data: TUInt8Array);
begin
  var LPushSize := Data.length;
  if LPushSize > 0 then
  begin
    // Check if buffer is empty, in that case we just
    // do a clean append and exit
    if FBuffer = nil then
    begin
      Append(Data);
      exit;
    end;

    // Cache existing data
    var LOldData: TUInt8Array;
    if FBuffer <> nil then
      LOldData := ToBytes();

    // Allocate a new buffer with enough space
    // to house new-data + old-data
    Allocate(Size + LPushSize);

    // write new data first
    // +--------+--------------+
    // | -> NEW |              |
    // +--------+--------------+
    WriteBuffer(0, Data);

    // Write old-data forward
    // +--------+--------------+
    // |        |  -> OLD      |
    // +--------+--------------+
    if LOldData.length > 0 then
      WriteBuffer(LPushSize, LOldData);

  end;
end;

function TManagedMemory.Pop(BlockSize: integer): TUInt8Array;
begin
  if BlockSize > Size then
    BlockSize := Size;

  if BlockSize > 0 then
  begin
    result := ReadBuffer(0, BlockSize);

    if Size - BlockSize < 1 then
      Release()
    else
    begin
      var LTemp := ReadBuffer(BlockSize, Size -BlockSize);
      WriteBuffer(0, LTemp);
      Shrink(BlockSize);
    end;
  end;
end;

procedure TManagedMemory.ScaleTo(NewSize: int64);
begin
  if NewSize > 0 then
  begin
    if NewSize <> Size then
    begin
      case (NewSize > Size) of
      true:   Grow(NewSize - Size);
      false:  Shrink(Size - NewSize);
      end;
    end;
  end else
    Release();
end;

procedure TManagedMemory.Grow(const BytesToGrow: integer);
begin
  if BytesToGrow > 0 then
  begin
    var LOldData: TUInt8Array;
    if FBuffer <> nil then
      LOldData := ToBytes();

    Allocate(Size + BytesToGrow);

    if LOldData.length > 0 then
      WriteBuffer(0, LOldData);

  end else
    raise EManagedMemory.Create('Invalid growth value, expected 1 or above error');
end;

procedure TManagedMemory.FromBytes(const Bytes: TUInt8Array);
begin
  if FBuffer <> nil then
    Release();

  var LLen := Bytes.length;
  if LLen > 0 then
  begin
    Allocate(LLen);
    asm
      (@FArray).set(@bytes, 0);
    end;
  end;
end;

function TManagedMemory.ToBytes: TUInt8Array;
begin
  if FBuffer <> nil then
  begin
    asm
      @result = Array.prototype.slice.call(@FArray);
    end;
  end;
end;

function TManagedMemory.ReadBuffer(Offset, ReadLen: int64): TUInt8Array;
begin
  if FBuffer <> nil then
  begin
    if ReadLen > 0 then
    begin
      if (Offset >=0) and (Offset < Size) then
      begin
        var LTemp := FArray.SubArray(Offset, Offset + ReadLen);
        asm
          @result = Array.prototype.slice.call(@LTemp);
        end;
      end else
      raise EManagedMemory.Create('Invalid offset, expected 0..' + (Size-1).ToString() + ' not ' + Offset.ToString() + ' error');
    end;
  end;
end;

procedure TManagedMemory.WriteBuffer(Offset: int64; Data: TUInt8Array);
begin
  if FBuffer <> nil then
  begin
    if Data.length > 0 then
    begin
      if (Offset >=0) and (Offset < Size) then
        FArray.Set(data, Offset)
      else
        raise EManagedMemory.Create('Invalid offset, expected 0..' + (Size-1).ToString() + ' not ' + Offset.ToString() + ' error');
    end;
  end;
end;

procedure TManagedMemory.BeforeRelease;
begin
end;

procedure TManagedMemory.BeforeAllocate(var NewSize: integer);
begin
end;

procedure TManagedMemory.AfterAllocate;
begin
  if assigned(OnMemoryAllocated) then
    OnMemoryAllocated(self);
end;

procedure TManagedMemory.AfterRelease;
begin
  if assigned(OnMemoryReleased) then
    OnMemoryReleased(self);
end;

procedure TManagedMemory.Allocate(BytesToAllocate: int64);
begin
  if FBuffer <> nil then
    Release();

  if BytesToAllocate > 0 then
  begin
    BeforeAllocate(BytesToAllocate);
    try
      FBuffer := new JArrayBuffer(BytesToAllocate);
      FView := new JDataView(FBuffer);
      FArray := new JUint8Array(FBuffer);
    except
      on e: exception do
      begin
        FBuffer := nil;
        FView := nil;
        FArray := nil;
        raise;
      end;
    end;

    AfterAllocate();
  end else
  raise EManagedMemory.Create('Invalid size to allocate, value must be positive');
end;

procedure TManagedMemory.Release;
Begin
  if FBuffer <> nil then
  begin
    try
      try
        BeforeRelease();
      finally
        FArray := nil;
        FView := nil;
        FBuffer := nil;
      end;
    finally
      AfterRelease();
    end;
  end;
end;

procedure TManagedMemory.FillByte(Offset: int64; ByteLen: integer; Value: Byte);
begin
  if not &Empty then
  begin
    var LTotalSize := FBuffer.byteLength;

    if  ( Offset >=0 ) and ( Offset < LTotalSize) then
    begin
      // clip the offset so we dont over-penetrate the buffer
      if Offset + ByteLen > LTotalSize then
        ByteLen := LTotalSize - Offset;
      inc(ByteLen, Offset);

      FArray.fill( InitUint08(Value), offset, ByteLen);
    end else
    raise EManagedMemory.Create('Memory fill failed, invalid offset error');
  end else
  raise EManagedMemory.Create('Memory fill failed, buffer is empty error');
end;

initialization
begin

  // Polyfill that maps the fill() method of Array, so that it
  // also works for typed arrays. Better safe than sorry
  asm
    if (!Uint8Array.prototype.fill) {
      Uint8Array.prototype.fill = Array.prototype.fill;
    }
  end;
end;


end.
