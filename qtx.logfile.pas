unit qtx.logfile;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils,
  qtx.classes,
  NodeJS.fs;

type
  // Exception types
  EQTXLogFile     = class(EException);
  EQTXLogObject   = class(EException);

  TQTXLogEmitterWriteEvent  = procedure (Sender: TObject; Text: string);
  TQTXLogEmitterOpenEvent   = procedure (Sender: TObject);
  TQTXLogEmitterCloseEvent  = procedure (Sender: TObject);

  TQTXLogEmitterOptions = set of (
    leDeleteOld,  // delete old log if present
    leAutoCRLF,   // Auto append #13#10 if missing
    leTime,       // Write timestamp
    leToConsole   // Emit text to console too
    );

  IQTXLogClient = interface
    ['{0313AD63-8A55-4333-B57F-3FBC103CD6C8}']
    function    GetActive: boolean;
    procedure   Write(Text: string);
    procedure   WriteF(Text: string; const Values: array of const);
  end;

  IQTXLogHost = interface
    ['{32B931C3-E718-478A-A6CC-71D7E799B91E}']
    procedure   Open(LogURI: string);
    procedure   Close;
    function    GetActive: boolean;
    function    GetOptions: TQTXLogEmitterOptions;
    procedure   Write(Text: string);
    procedure   WriteF(Text: string; const Values: array of const);
  end;

  TQTXLogEmitter = class(TObject, IQTXLogClient, IQTXLogHost)
  private
    FLogFile:   JWriteStream;
    FLogFilePath: string;
    FOptions:   TQTXLogEmitterOptions;
    FFileSys:   Jfs_Exports;
    procedure   RemoveOlderLog(Filename: string);
  protected
    function    GetActive: boolean;
    function    GetOptions: TQTXLogEmitterOptions;
    procedure   SetOptions(NewOptions: TQTXLogEmitterOptions);

  public
    property    Active: boolean read ( assigned(FLogFile) );
    property    LogURI: string read FLogFilePath;
    property    Options: TQTXLogEmitterOptions read FOptions write SetOptions;

    procedure   Open(LogURI: string);
    procedure   Close;

    procedure   Write(Text: string);
    procedure   WriteF(Text: string; const Values: array of const);

    constructor Create; virtual;
    destructor  Destroy; override;
  published
    property    OnLogWritten: TQTXLogEmitterWriteEvent;
    property    OnLogOpen: TQTXLogEmitterOpenEvent;
    property    OnLogClosed: TQTXLogEmitterCloseEvent;
  end;

  // Ordinary base-class for classes that wants logging
  TQTXLogObject = class(TObject)
  private
    FEmitter:   IQTXLogClient;

  protected
    function    GetLogObject: IQTXLogClient; virtual;
    procedure   SetLogObject(const NewLogObj: IQTXLogClient); virtual;

    procedure   WriteToLog(Text: string); overload; virtual;
    procedure   WriteToLog(Text: string; const Values: array of const); overload; virtual;

  public
    property    Logging: IQTXLogClient read FEmitter write SetLogObject;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  // Base-class for handle-oriented classes that wants logging
  TQTXLogHandleObject = class(TQTXHandleObject)
  private
    FEmitter:   IQTXLogClient;
  protected
    function    GetLogObject: IQTXLogClient; virtual;
    procedure   SetLogObject(const NewLogObj: IQTXLogClient); virtual;

    procedure   WriteToLog(Text: string); overload; virtual;
    procedure   WriteToLog(Text: string; const Values: array of const); overload; virtual;
  public
    property    Logging: IQTXLogClient read FEmitter write SetLogObject;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

resourcestring
  CNT_ERR_LOGFILE_OPTIONS_ACTIVE  = 'Options cannot be altered while a logfile is active error';
  CNT_ERR_LOGFILE_REMOVE_EXISTING = 'Failed to delete existing logfile (%s): %s';
  CNT_ERR_LOGFILE_CREATE_FAILED   = 'Failed to create logfile (%s): %s';
  CNT_ERR_LOGFILE_CLOSE_FAILED    = 'Failed to close logfile (%s): %s';
  CNT_ERR_LOGFILE_WRITE_NOTOPEN   = 'Failed to emit data, no logfile is open';
  CNT_ERR_LOGFILE_WRITE_ERROR     = 'Failed to emit data: %s';


//############################################################################
// TQTXLogHandleObject
//############################################################################

constructor TQTXLogHandleObject.Create;
begin
  inherited Create;
end;

destructor TQTXLogHandleObject.Destroy;
begin
  FEmitter := nil;
  inherited;
end;

function TQTXLogHandleObject.GetLogObject: IQTXLogClient;
begin
  result := FEmitter;
end;

procedure TQTXLogHandleObject.SetLogObject(const NewLogObj: IQTXLogClient);
begin
  FEmitter := NewLogObj;
end;

procedure TQTXLogHandleObject.WriteToLog(Text: string);
begin
  if FEmitter <> nil then
    FEmitter.Write(Text)
  else
    writeln(Text);
end;

procedure TQTXLogHandleObject.WriteToLog(Text: string; const Values: array of const);
begin
  if FEmitter <> nil then
    FEmitter.WriteF(Text, Values)
  else
    writelnf(Text, Values);
end;

//############################################################################
// TQTXLogObject
//############################################################################

constructor TQTXLogObject.Create;
begin
  inherited Create;
end;

destructor TQTXLogObject.Destroy;
begin
  FEmitter := nil;
  inherited;
end;

function TQTXLogObject.GetLogObject: IQTXLogClient;
begin
  result := FEmitter;
end;

procedure TQTXLogObject.SetLogObject(const NewLogObj: IQTXLogClient);
begin
  FEmitter := NewLogObj;
end;

procedure TQTXLogObject.WriteToLog(Text: string);
begin
  if FEmitter <> nil then
    FEmitter.Write(Text)
end;

procedure TQTXLogObject.WriteToLog(Text: string; const Values: array of const);
begin
  if FEmitter <> nil then
    FEmitter.WriteF(Text, Values)
end;

//############################################################################
// TQTXLogEmitter
//############################################################################

constructor TQTXLogEmitter.Create;
begin
  inherited Create;

  FOptions := [
    //lfDeleteOld,
    leAutoCRLF,   // we want CRLF
    leTime,       // we want timestamp
    leToConsole   // we want output to console
    ];

  // Get a PTR for the Filesystem API
  FFileSys := NodeFsAPI();
end;

destructor TQTXLogEmitter.Destroy;
begin

  // Close file if any
  if FLogFile <> nil then
  begin
    try
      Close();
    except
      // sink exception
    end;
  end;

  // Release filesystem
  FFilesys := nil;
  inherited;
end;

function TQTXLogEmitter.GetActive: boolean;
begin
  result := FLogFile <> nil;
end;

function TQTXLogEmitter.GetOptions: TQTXLogEmitterOptions;
begin
  result := FOptions;
end;

procedure TQTXLogEmitter.SetOptions(NewOptions: TQTXLogEmitterOptions);
begin
  if not Active then
    FOptions := NewOptions
  else
  begin
    if (leToConsole in FOptions) then
      writeln(CNT_ERR_LOGFILE_OPTIONS_ACTIVE);
    raise EQTXLogFile.Create(CNT_ERR_LOGFILE_OPTIONS_ACTIVE);
  end;
end;

procedure TQTXLogEmitter.RemoveOlderLog(Filename: string);
begin
  try
    FFileSys.unlinkSync(Filename);
  except
    on e: exception do
    begin
      var LError := Format(CNT_ERR_LOGFILE_REMOVE_EXISTING, [Filename, e.message]);
      if (leToConsole in FOptions) then
        writeln(LError);
      raise EQTXLogFile.Create(LError);
    end;
  end;
end;

procedure TQTXLogEmitter.Open(LogURI: String);

  function FileExists__(const Filename: String): boolean;
  begin
    if FFileSys.existsSync(filename) then
    begin
      var stats := JStats( FFileSys.lstatSync(Filename) );
      result := stats.isFile();
    end;
  end;

begin
  // If a logfile is active, shut that down first
  if FLogFile <> nil then
    Close();

  // Delete old logfile if it exists
  if (leDeleteOld in FOptions) then
  begin
    if FileExists__(LogURI) then
      RemoveOlderLog(LogURI);
  end;

  // Setup writestream with "a" (append) flag
  // Note: options include UTF8 by default (!)
  var options := new TCreateWriteStreamOptions;
  options.flags := 'a';

  try
    FLogFile := FFileSys.createWriteStream(LogURI, options);
  except
    on e: exception do
    begin
      FLogFile := nil;
      var LError := Format(CNT_ERR_LOGFILE_CREATE_FAILED, [FLogFilePath,e.message]);
      if (leToConsole in FOptions) then
        writeln(LError);
      raise EQTXLogFile.Create(LError);
    end;
  end;

  // Keep full path
  FLogFilePath := LogURI;

  if assigned(OnLogOpen) then
    OnLogOpen(self);
end;

procedure TQTXLogEmitter.Close;
begin
  if FLogFile <> nil then
  begin
    try
      try
        FLogFile.destroy();
      except
        on e: exception do
        begin
          var LError := Format(CNT_ERR_LOGFILE_CLOSE_FAILED, [FLogFilePath, e.message]);
          if (leToConsole in FOptions) then
            writeln(LError);
        end;
      end;
    finally
      FLogFile := nil;
      FLogFilePath := '';

      if assigned(OnLogClosed) then
        OnLogClosed(self);
    end;
  end;
end;

procedure TQTXLogEmitter.Write(Text: String);
begin
  // Make sure logfile is active
  if not Active then
  begin
    var LError := CNT_ERR_LOGFILE_WRITE_NOTOPEN;
    if (leToConsole in FOptions) then
      writeln(LError);
    raise EQTXLogFile.Create(LError);
  end;

  // Copy the text here for our event
  var LBaseText := Text;
  var LOutput := '';

  // If timestamp is in the option, we start the output
  // string with that. If not, just the text
  if (leTime in FOptions) then
    LOutput := TimeToStr(now) + ' ' + Text
  else
    LOutput := Text;

  // CR + LF Append?
  if (leAutoCRLF in FOptions) then
  begin
    if not Text.EndsWith(#13#10) then
    begin
      // Unix can have just #13, in that case we
      // manually add just the missing #10. We have already
      // eliminated a full #13#10 above so its safe.
      // If neither #13#10 or #13 is there, add full CRLF
      if Text.EndsWith(#13) then
        LOutput += #10
      else
        LOutput += #13#10;
    end;
  end;

  // Emit text to
  try
    FLogFile.write(LOutput, 'utf8');
  except
    on e: exception do
    begin
      var LError := format(CNT_ERR_LOGFILE_WRITE_ERROR, [e.message]);
      if (leToConsole in FOptions) then
        writeln(LError);
      raise EQTXLogFile.Create(LError);
    end;
  end;

  if (leToConsole in FOptions) then
    writeln(LBaseText);

  if assigned(OnLogWritten) then
    OnLogWritten(self, LBaseText);
end;

procedure TQTXLogEmitter.WriteF(Text: String; const Values: array of const);
begin
  if Text.length > 0 then
    Write(Format(Text, Values));
end;


end.
