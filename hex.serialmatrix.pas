unit hex.serialmatrix;

interface

uses 
  qtx.sysutils,
  hex.types;

type

  { Access interface for TFMXHexCustomLicenseStorage}
  IHexSerialMatrix = interface
    ['{D562EC1E-6254-45E6-87C7-18CF16CF84B3}']
    function GetSerialMatrix(var Value: THexKeyMatrix): boolean;
  end;

  THexGetSerialMatrixEvent = procedure (sender: TObject; var Matrix: THexKeyMatrix);

  THexSerialMatrix = class(TObject, IHexSerialMatrix)
  protected
    function  GetSerialMatrix(var Value: THexKeyMatrix): boolean;
  published
    property  OnGetMatrix: THexGetSerialMatrixEvent;
  end;


implementation

//############################################################################
// THexSerialMatrix
//############################################################################

function THexSerialMatrix.GetSerialMatrix(var Value: THexKeyMatrix): boolean;
begin
  result := assigned(OnGetMatrix);
  if result then
  begin
    var LTemp: THexKeyMatrix;
    OnGetMatrix(self, LTemp);
    Value := LTemp;
  end;
end;



end.
