unit qtx.components;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses 
  qtx.values,
  qtx.sysutils;

type

  TControlHandle    = variant;
  TDOMHandle        = variant;

  EComponent  = class(EException);
  EControl    = class(EException);

  TStdCallback = procedure ();

  TPersistent = class(TObject)
  end;

  TComponentState = (
    csCreating,
    csDestroying,
    csReady);

  TComponentLayout = (
    cpInitial,
    cpRelative,
    cpAbsolute,
    cpCalculated);

  TComponentDisplay = (
    cdInitial,
    cdInlineBlock,
    cdBlock,
    cdFlex,
    cdTable,
    cdTableCaption,
    cdTableCell,
    cdTableRow,
    cdTableColumn,
    cdRunIn,
    cdListItem);

  TComponent = class(TPersistent)
  private
    FState:     TComponentState;
    FOwner:     TComponent;
  protected
    procedure   SetOwner(const NewOwner: TComponent); virtual;
    procedure   SetComponentState(const NewState: TComponentState);

    function    ValidateOwner(OwnerCandidate: TObject): boolean; virtual;

    procedure   InitializeComponent(const CB: TStdCallback); virtual;
    procedure   FinalizeComponent; virtual;
  public
    property    Owner: TComponent read FOwner;
    property    ComponentState: TComponentState read FState;
    procedure   AfterConstruction; virtual;
    procedure   BeforeDestruction; virtual;
    constructor Create(AOwner: TComponent);
    destructor  Destroy; override;
  end;

  TQTXObject = class(TComponent)
  end;

  TQTXShape = class(TQTXObject)
  private
    class var _ElementId: integer;
    class var _ElementLUT: variant;

    FHandle:    TControlHandle;
    FLayout:    TComponentLayout  = cpInitial;
    FDisplay:   TComponentDisplay = cdInitial;

    procedure   RegisterElement;
    procedure   UnRegisterElement;
  protected
    function    GetElementType: string; virtual;
    function    GetElementId: string; virtual;
    function    GetOwnerHandle: TControlHandle;

    procedure   SetLayout(const NewLayout: TComponentLayout);
    procedure   SetDisplay(const NewDisplay: TComponentDisplay);

    function    GetLeft: integer;
    procedure   SetLeft(const NewLeft: integer);

    function    GetTop: integer;
    procedure   SetTop(const NewTop: integer);

    function    GetWidth: integer;
    procedure   SetWidth(const NewWidth: integer);

    function    GetHeight: integer;
    procedure   SetHeight(const NewHeight: integer);

    function    ValidateOwner(OwnerCandidate: TObject): boolean; override;

    procedure   AttachToParent(const OwnerHandle: TControlHandle);
    procedure   DetachFromParent;

    procedure   InitializeComponent(CB: TStdCallback); override;
    procedure   FinalizeComponent; override;
  public
    class function GetComputedStyle: TDOMHandle;

    constructor Create(Parent: TControlHandle); overload; virtual;

    procedure   SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);

    property    Handle: TControlHandle read FHandle;
    property    OwnerHandle: TControlHandle read GetOwnerHandle;

    property    LayoutMode: TComponentLayout read FLayout write SetLayout;
    property    DisplayMode: TComponentDisplay read FDisplay write SetDisplay;

    property    Left: integer read GetLeft write SetLeft;
    property    Top: integer read GetTop write SetTop;
    property    Width: integer read GetWidth write SetWidth;
    property    Height: integer read GetHeight write SetHeight;
  end;

  TButton = class(TQTXShape)
  private
    function    GetCaption: string;
    procedure   SetCaption(const NewCaption: string);
  protected
    function    GetElementType: string; override;
  public
    property    Caption: string read GetCaption write SetCaption;
  end;

implementation


var

  // Lookup table for HTML->ELEMENT-STYLE->display
  __DisplayStyles: array[TComponentDisplay] of string = (
  'inline-block',
  'inline-block',
  'block',
  'flex',
  'table',
  'table-caption',
  'table-cell',
  'table-row',
  'table-column',
  'run-in',
  'list-item'
  );

  // Lookup table for HTML->ELEMENT->STYLE->position
  __PositionModes: array[TComponentLayout] of string = (
  'absolute', // initial
  'absolute', // absolute
  'relative', // Relative
  'absolute'  // Calculated
  );

//############################################################################
// TButton
//############################################################################

function  TButton.GetElementType: string;
begin
  result := 'button';
end;

function TButton.GetCaption: string;
begin
  result := Handle.innerHTML;
end;

procedure TButton.SetCaption(const NewCaption: string);
begin
  Handle.innerHTML := NewCaption;
end;

//############################################################################
// TComponent
//############################################################################

constructor TQTXShape.Create(Parent: TControlHandle);
begin
  inherited Create(NIL);
  AttachToParent(Parent);
end;

procedure TQTXShape.InitializeComponent(CB: TStdCallback);
begin
  inherited InitializeComponent(
    procedure ()
    begin
      // create new element
      var LType := GetElementType();
      asm
        (@self.FHandle) = document.createElement(@LType);
      end;
      FHandle.id := GetElementId();

      FLayout := TComponentLayout.cpAbsolute;
      FHandle.style.position := 'absolute';

      FDisplay := TComponentDisplay.cdInlineBlock;
      FHandle.style.display := 'inline-block';

      // Register element in control lookup-table
      RegisterElement();

      if assigned(CB) then
        CB();
    end);
end;

procedure TQTXShape.FinalizeComponent;
begin
  if (FHandle) then
  begin
    // Remove control from lookup-table
    UnRegisterElement();

    // Detach from owner
    DetachFromParent();
  end;
end;

class function TQTXShape.GetComputedStyle: TDOMHandle;
begin
  asm
    @result = window.getComputedStyle(@self.Handle);
  end;
end;

procedure TQTXShape.RegisterElement;
begin
  // First item? Create lookup table
  if not (_ElementLUT) then
  begin
    asm
      @_ElementLUT = new Object();
    end;
  end;

  _ElementLUT[FHandle] := self;

  if Owner <> nil then
    AttachToParent(TQTXShape(Owner).Handle);
end;

procedure TQTXShape.UnRegisterElement;
begin
  var LRef := self;
  asm
    delete @_ElementLUT[@LRef];
  end;

  if Owner <> nil then
    DetachFromParent();
end;

function TQTXShape.ValidateOwner(OwnerCandidate: TObject): boolean;
begin
  result := (OwnerCandidate <> nil) and (OwnerCandidate is TQTXShape);
end;

function TQTXShape.GetElementType: string;
begin
  result := 'div';
end;

function TQTXShape.GetElementId: string;
begin
  inc(_ElementId);
  result := 'ctrl_' + _ElementId.ToString();
end;

function TQTXShape.GetOwnerHandle: TControlHandle;
begin
  if (FHandle) then
    result := variant( FHandle.parentElement );
end;

procedure TQTXShape.AttachToParent(const OwnerHandle: THandle);
begin
  if (OwnerHandle) then
  begin
    if (FHandle) then
    begin
      if ( FHandle.parentElement ) then
        DetachFromParent();

      OwnerHandle.appendChild(FHandle);
    end else
    raise EComponent.CreateFmt
    ('%s failed, invalid or unsuitable handle error', [{$I %FUNCTION%}]);
  end else
  raise EComponent.CreateFmt
  ('%s failed, invalid or unsuitable owner handle error', [{$I %FUNCTION%}]);
end;

procedure TQTXShape.DetachFromParent;
begin
  if (FHandle) then
  begin
    var LParent := FHandle.parentElement();
    if (LParent) then
    begin
      LParent.removeChild(FHandle);
      FHandle := unassigned;
    end;
  end;
end;


procedure TQTXShape.SetDisplay(const NewDisplay: TComponentDisplay);
begin
  FDisplay := NewDisplay;
  if (FHandle) then
    FHandle.style.display := __DisplayStyles[NewDisplay];
end;


procedure TQTXShape.SetLayout(const NewLayout: TComponentLayout);
begin
  FLayout := NewLayout;
  if (FHandle) then
    FHandle.style.position := __PositionModes[NewLayout];
end;

procedure TQTXShape.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
  if (FHandle) then
  begin
    Handle.style.left := TInteger.ToPxStr(NewLeft);
    Handle.style.top := TInteger.ToPxStr(NewTop);
    Handle.style.width := TInteger.ToPxStr(NewWidth);
    Handle.style.height := TInteger.ToPxStr(NewHeight);
  end;
end;

function TQTXShape.GetLeft: integer;
begin
  if (FHandle) then
  begin
    var LHandle := FHandle;
    asm
      var LObj = document.defaultView.getComputedStyle(@LHandle, null);
      var LData = LObj.getPropertyValue("left");
      if (typeof(LData) === "number") {
        @result = LData;
      } else {
        if (typeof(LData) === "string") {
          LData = parseInt(mData.trim());

          if (typeof(LData) === "number")
            @result = LData;
        }
      }
    end;
  end;
end;

procedure TQTXShape.SetLeft(const NewLeft: integer);
begin
  if (FHandle) then
  begin
    FHandle.styles.left := TInteger.ToPxStr(NewLeft);
  end;
end;

function TQTXShape.GetTop: integer;
begin
  if (FHandle) then
  begin
    var LHandle := FHandle;
    asm
      var LObj = document.defaultView.getComputedStyle(@LHandle, null);
      var LData = LObj.getPropertyValue("top");
      if (typeof(LData) === "number") {
        @result = LData;
      } else {
        if (typeof(LData) === "string") {
          LData = parseInt(mData.trim());

          if (typeof(LData) === "number")
            @result = LData;
        }
      }
    end;
  end;
end;

procedure TQTXShape.SetTop(const NewTop: integer);
begin
  if (FHandle) then
  begin
    FHandle.styles.top := TInteger.ToPxStr(NewTop);
  end;
end;

function TQTXShape.GetWidth: integer;
begin
  if (Handle) then
    result := Handle.offsetWidth;
end;

procedure TQTXShape.SetWidth(const NewWidth: integer);
begin
  if (FHandle) then
  begin
    FHandle.styles.width := TInteger.ToPxStr(NewWidth);
  end;
end;

function TQTXShape.GetHeight: integer;
begin
  if (Handle) then
    result := Handle.offsetHeight;
end;

procedure TQTXShape.SetHeight(const NewHeight: integer);
begin
  if (FHandle) then
  begin
    FHandle.styles.height := TInteger.ToPxStr(NewHeight);
  end;
end;

//############################################################################
// TComponent
//############################################################################

constructor TComponent.Create(AOwner: TComponent);
begin
  inherited Create;
  FState := csCreating;

  case ValidateOwner(AOwner) of
  true:   SetOwner(AOwner);
  false:
    begin
      if AOwner = nil then
        SetOwner(nil)
      else
        raise EComponent.CreateFmt
        ('%s failed, invalid or unsuitable owner error', [{$I %FUNCTION%}]);
    end;
  end;

  InitializeComponent(
    procedure ()
    begin
      FState := csReady;
      AfterConstruction();
    end);
end;

destructor TComponent.Destroy;
begin
  FState := csDestroying;
  BeforeDestruction();
  FinalizeComponent();
  inherited destroy;
end;

procedure TComponent.AfterConstruction;
begin
end;

procedure TComponent.BeforeDestruction;
begin
end;

procedure TComponent.InitializeComponent(const CB: TStdCallback);
begin
  if assigned(CB) then
    CB();
end;

procedure TComponent.FinalizeComponent;
begin
end;

function TComponent.ValidateOwner(OwnerCandidate: TObject): boolean;
begin
  result := true;
end;

procedure TComponent.SetOwner(const NewOwner: TComponent);
begin
  FOwner := NewOwner
end;

procedure TComponent.SetComponentState(const NewState: TComponentState);
begin
  FState := NewState;
end;


end.
