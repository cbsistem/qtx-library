unit qtx.codec;

interface

uses
  W3C.TypedArray,
  qtx.sysutils,
  qtx.memory,
  qtx.classes;

type

  // Exception classes
  ECodecError   = class(EException);
  ECodecBinding = class(ECodecError);
  ECodecManager = class(ECodecError);

  // Forward declarations
  TQTXCodec         = class;
  TQTXCodecBinding  = class;
  TQTXCodecManager  = class;

  // Codec version info
  TQTXCodecVersionInfo = record
    viMajor:    integer;
    viMinor:    integer;
    viRevision: integer;
    function ToString: string;
    function Equals(const Info: TQTXCodecVersionInfo): boolean;
    class function Create(const Major, Minor, Revision: integer): TQTXCodecVersionInfo; static;
  end;

  // Codec dataflow directions
  TCodecDataDirection = (
    cdRead  = 1,
    cdWrite = 2
  );

  TCodecDataFlow    = set of TCodecDataDirection;
  TCodecDataFormat  = (cdBinary, cdText);

  TCodecDataFlowHelper = helper for TCodecDataFlow
    function ToString: string;
    function Ordinal: integer;
    function Equals(const Flow: TCodecDataFlow): boolean;
  end;

  ICodecInfo = interface
    ['{1E2D60CD-3A01-41AE-BB7D-A2B5BA650A82}']
    procedure SetName(const coName: string);
    procedure SetMime(const coMime: string);
    procedure SetVersion(const coVersion: TQTXCodecVersionInfo);
    procedure SetDataFlow(const coFlow: TCodecDataFlow);
    procedure SetDescription(const coInfo: string);
    procedure SetInput(const InputType: TCodecDataFormat);
    procedure SetOutput(const OutputType: TCodecDataFormat);
  end;

  TQTXCodecInfo = class(TObject, ICodecInfo)
  private
    ciName:     string;
    ciMime:     string;
    ciAbout:    string;
    ciVersion:  TQTXCodecVersionInfo;
    ciDataFlow: TCodecDataFlow;
    ciInput:    TCodecDataFormat;
    ciOutput:   TCodecDataFormat;
  protected
    procedure   SetName(const coName: string);
    procedure   SetMime(const coMime: string);
    procedure   SetVersion(const coVersion: TQTXCodecVersionInfo);
    procedure   SetDataFlow(const coFlow: TCodecDataFlow);
    procedure   SetDescription(const coInfo: string);

    procedure   SetInput(const InputType: TCodecDataFormat);
    procedure   SetOutput(const OutputType: TCodecDataFormat);
  public
    property    Name: string read ciName;
    property    MimeType: string read ciMime;
    property    About: string read ciAbout;
    property    Version: TQTXCodecVersionInfo read ciVersion;
    property    DataFlow: TCodecDataFlow read ciDataFlow;
    property    Input: TCodecDataFormat read ciInput;
    property    Output: TCodecDataFormat read ciOutput;

    function    Equals(const Info: TQTXCodecInfo): boolean;
    function    ToString: string;
  end;

  TQTXCodecBinding = class(TObject)
  private
    FCodec: TQTXCodec;
  public
    property  Codec: TQTXCodec read FCodec;
    property  Input: IManagedData;
    property  Output: IManagedData;

    procedure Encode; overload;
    procedure Encode(const DataInput, DataOutput: IManagedData); overload;

    procedure Decode;overload;
    procedure Decode(const DataInput, DataOutput: IManagedData); overload;

    constructor Create(const EndPoint: TQTXCodec); virtual;
    destructor Destroy; override;
  end;

  TQTXCodecBindingList = array of TQTXCodecBinding;

  IQTXCodecBinding = interface
    ['{B60FB63B-4918-4E1A-98E9-91423A787573}']
    procedure RegisterBinding(const Binding: TQTXCodecBinding);
    procedure UnRegisterBinding(const Binding: TQTXCodecBinding);
  end;

  IQTXCodecProcess = interface
    ['{17BBF7A5-B860-40CC-A3E0-88627E6DFF15}']
    procedure EncodeData(const Source, Target: IManagedData);
    procedure DecodeData(const Source, Target: IManagedData);
  end;

  TQTXCodec = class(TDataTypeConverter, IQTXCodecBinding, IQTXCodecProcess )
  private
    FBindings:  TQTXCodecBindingList;
    FCodecInfo: TQTXCodecInfo;
  protected
    /* IMPLEMENTS :: ICodecBinding */
    procedure RegisterBinding(const Binding: TQTXCodecBinding);
    procedure UnRegisterBinding(const Binding: TQTXCodecBinding);

    /* IMPLEMENTS :: ICodecProcess */
    procedure EncodeData(const Source, Target: IManagedData); virtual; abstract;
    procedure DecodeData(const Source, Target: IManagedData); virtual; abstract;

    function  MakeCodecInfo: TQTXCodecInfo; virtual;
  public
    property Info: TQTXCodecInfo read FCodecInfo;
    constructor Create; override;
    destructor Destroy; override;
  end;

  TCodecClass = class of TQTXCodec;
  TCodecList = array of TQTXCodec;

  TQTXCodecManager = class(TObject)
  private
    FCodecs:    TCodecList;
  protected
    function    CodecByClass(const ClsType: TCodecClass): TQTXCodec;
    procedure   Reset;
  public
    procedure   RegisterCodec(const CodecClass: TCodecClass);
    procedure   UnRegisterCodec(const CodecClass: TCodecClass);
    function    QueryByType(MimeType: string; var List: TCodecList): boolean;
    function    QueryByName(Name: string; var List: TCodecList): boolean;
    destructor  Destroy; override;
  end;

function CodecManager: TQTXCodecManager;

implementation

var
__Manager: TQTXCodecManager;

function CodecManager: TQTXCodecManager;
begin
  if __Manager = nil then
    __Manager := TQTXCodecManager.Create();
  result := __Manager;
end;

//############################################################################
// TQTXCodecManager
//############################################################################

destructor TQTXCodecManager.Destroy;
begin
  if FCodecs.length > 0 then
    Reset();
  inherited;
end;

procedure TQTXCodecManager.Reset;
begin
  try
    for var codec in FCodecs do
    begin
      try
        codec.free;
      except
        // sink exception and continue
        // on e: exception do;
      end;
    end;
  finally
    FCodecs.Clear();
  end;
end;

function TQTXCodecManager.QueryByType(MimeType: string;
  var List: TCodecList): boolean;
begin
  MimeType := MimeType.ToLower().Trim();
  if MimeType.Length > 0 then
  begin
    List.Clear();
    for var LItem in FCodecs do
    begin
      var LText := LItem.Info.MimeType.ToLower();
      if  LText = MimeType then
        List.add(LItem);
    end;
    result := (List.length > 0);
  end;
end;

function TQTXCodecManager.QueryByName(Name: string; var List: TCodecList): boolean;
begin
  Name := Name.ToLower().Trim();
  if Name.Length > 0 then
  begin
    List.Clear();
    for var LItem in FCodecs do
    begin
      var LText := LItem.Info.Name.ToLower().Trim();
      if LText.StartsWith(Name) then
        List.add(LItem);
    end;
    result := (List.Length > 0);
  end;
end;

function TQTXCodecManager.CodecByClass(const ClsType: TCodecClass): TQTXCodec;
begin
  result := nil;
  for var LItem in FCodecs do
  begin
    if (LItem.ClassType = ClsType) then
    begin
      result := LItem;
      break;
    end;
  end;
end;

procedure TQTXCodecManager.RegisterCodec(const CodecClass: TCodecClass);
begin
  var LItem := CodecByClass(CodecClass);
  if LItem = nil then
  begin
    LItem := CodecClass.Create;
    FCodecs.add(LItem);
  end else
  raise ECodecManager.Create("Codec already registered error");
end;

procedure TQTXCodecManager.UnRegisterCodec(const CodecClass: TCodecClass);
begin
  for var x := 0 to FCodecs.length-1 do
  begin
    if FCodecs[x].classtype = CodecClass then
    begin
      FCodecs[x].free;
      FCodecs.delete(x,1);
      break;
    end;
  end;
end;

//############################################################################
// TQTXCodec
//############################################################################

constructor TQTXCodec.Create;
begin
  inherited Create();
  FCodecInfo := MakeCodecInfo();
  if FCodecInfo = nil then
    raise ECodecError.Create("Internal codec error, failed to obtain registration info error");
end;

destructor TQTXCodec.Destroy;
begin
  FCodecInfo.free;
  inherited;
end;

function TQTXCodec.MakeCodecInfo: TQTXCodecInfo;
begin
  result := nil;
end;

procedure TQTXCodec.RegisterBinding(const Binding: TQTXCodecBinding);
begin
  if FBindings.IndexOf(Binding) < 0 then
    FBindings.Add(Binding)
  else
  raise ECodecError.Create("Binding already connected to codec error");
end;

procedure TQTXCodec.UnRegisterBinding(const Binding: TQTXCodecBinding);
begin
  var LIndex := FBindings.IndexOf(Binding);
  if LIndex >=0 then
  begin
    FBindings.Delete(LIndex,1);
  end else
  raise ECodecError.Create("Binding not connected to this codec error");
end;

//############################################################################
// TQTXCodecBinding
//############################################################################

constructor TQTXCodecBinding.Create(const EndPoint: TQTXCodec);
begin
  inherited Create;
  if Endpoint <> nil then
  begin
    FCodec := Endpoint;
    var LAccess := FCodec as IQTXCodecBinding;
    LAccess.RegisterBinding(self);
  end else
  raise ECodecBinding.Create(
    "Binding failed, invalid endpoint error");
end;

destructor TQTXCodecBinding.Destroy;
begin
  var LAccess := FCodec as IQTXCodecBinding;
  LAccess.UnRegisterBinding(self);
  inherited;
end;

procedure TQTXCodecBinding.Encode(const DataInput, DataOutput: IManagedData);
begin
  self.Input := input;
  self.Output := output;
  Encode();
end;

procedure TQTXCodecBinding.Encode;
begin
  if assigned(Input) then
  begin
    if assigned(Output) then
    begin
      if assigned(FCodec) then
      begin
        var Access := FCodec as IQTXCodecProcess;
        Access.EncodeData(Input, Output);
      end else
      raise ECodecBinding.Create(
        "No codec associated with this binding error");
    end else
    raise ECodecBinding.Create(
    "Invalid output, IBinaryTransport is nil or unassigned error");
  end else
  raise ECodecBinding.Create(
  "Invalid input, IBinaryTransport is nil or unassigned error");
end;

procedure TQTXCodecBinding.Decode(const DataInput, DataOutput: IManagedData);
begin
  self.Input := input;
  self.Output := output;
  Decode();
end;

procedure TQTXCodecBinding.Decode;
begin
  if assigned(Input) then
  begin
    if assigned(Output) then
    begin
      if assigned(FCodec) then
      begin
        var Access := FCodec as IQTXCodecProcess;
        Access.DecodeData(Input, Output);
      end else
      raise ECodecBinding.Create(
      "No codec associated with this binding error");
    end else
    raise ECodecBinding.Create(
    "Invalid output, IBinaryTransport is nil or unassigned error");
  end else
  raise ECodecBinding.Create(
  "Invalid input, IBinaryTransport is nil or unassigned error");
end;

//############################################################################
// TCodecDataFlowHelper
//############################################################################

function TCodecDataFlowHelper.ToString: string;
begin
  result := Format('[%s, %s]',
    [if (cdRead in Self) then "read" else "",
    if (cdWrite in Self) then "write" else ""]);
end;

function TCodecDataFlowHelper.Ordinal: integer;
begin
  result := 0;
  if (cdRead in self) then inc(result, ord(cdRead) );
  if (cdWrite in self) then inc(result, ord(cdWrite) );
end;

function TCodecDataFlowHelper.Equals(const Flow: TCodecDataFlow): boolean;
begin
  result := self.ordinal() = flow.ordinal();
end;

//############################################################################
// TCodecVersionInfo
//############################################################################

class function TQTXCodecVersionInfo.Create(const Major, Minor, Revision: integer): TQTXCodecVersionInfo;
begin
  result.viMajor := Major;
  result.viMinor := Minor;
  result.viRevision := Revision;
end;

function TQTXCodecVersionInfo.ToString: string;
begin
  result:= Format("%d.%d.%d",[viMajor, viMinor, viRevision]);
end;

function TQTXCodecVersionInfo.Equals(const Info: TQTXCodecVersionInfo): boolean;
begin
  if self.viMajor = info.viMajor then
  begin
    if self.viMinor = info.viMinor then
    begin
      result := self.viRevision = info.viRevision;
    end;
  end;
end;

//############################################################################
// TQTXCodecInfo
//############################################################################

procedure TQTXCodecInfo.SetName(const coName: string);
begin
  self.ciName := coName;
end;

procedure TQTXCodecInfo.SetMime(const coMime: string);
begin
  self.ciMime := coMime;
end;

procedure TQTXCodecInfo.SetVersion(const coVersion: TQTXCodecVersionInfo);
begin
  self.ciVersion.viMajor := coVersion.viMajor;
  self.ciVersion.viMinor := coVersion.viMinor;
  self.ciVersion.viRevision := coVersion.viRevision;
end;

procedure TQTXCodecInfo.SetInput(const InputType: TCodecDataFormat);
begin
  ciInput := InputType;
end;

procedure TQTXCodecInfo.SetOutput(const OutputType: TCodecDataFormat);
begin
  ciOutput := OutputType;
end;

procedure TQTXCodecInfo.SetDescription(const coInfo: string);
begin
  self.ciAbout := coInfo;
end;

procedure TQTXCodecInfo.SetDataFlow(const coFlow: TCodecDataFlow);
begin
  self.ciDataFlow := coFlow;
end;

function TQTXCodecInfo.ToString: string;
begin
  result := "Codec: " + ciName + #13
          + "Version: " + ciVersion.ToString() + #13
          + "Dataflow: " + ciDataFlow.ToString() + #13
          + "About: " + ciAbout;
end;

function TQTXCodecInfo.Equals(const Info: TQTXCodecInfo): boolean;
begin
  if self.ciName = info.ciName then
  begin
    if self.ciMime = info.ciMime then
    begin
      if self.ciAbout = info.ciAbout then
      begin
        if self.ciVersion.Equals(info.ciVersion) then
        begin
          result := self.ciDataFlow.Equals(info.ciDataFlow);
        end;
      end;
    end;
  end;
end;



end.
