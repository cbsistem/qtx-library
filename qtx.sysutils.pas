unit qtx.sysutils;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses 
  W3C.TypedArray;

type

  // Standard pascal typenames
  extended  = float;
  float32   = float;
  float64   = variant;
  single    = float32;
  double    = float64;
  TDateTime = float64;
  TDate     = float32;
  TTime     = float32;
  real      = float64;

  // Standard cpu number types
  uint8     = integer;
  uint16    = integer;
  uint32    = integer;
  uint64    = integer;
  int8      = integer;
  int16     = integer;
  int32     = integer;

  byte      = uint8;
  tinyint   = int8;
  word      = uint16;
  smallint  = int16;
  longword  = uint32;
  int64     = integer;
  char      = string;
  pointer   = variant;

  TJSVMDataType =
    (
      dtUnknown = 0,
      dtBoolean,
      dtByte,
      dtChar,
      dtWord,
      dtLong,
      dtInt16,
      dtInt32,
      dtFloat32,
      dtFloat64,
      dtString
    );

  TJSVMEndianType = (
    stDefault       = 0,
    stLittleEndian  = 1,
    stBigEndian     = 2
    );

  TVariantExportType =
    ( vdUnknown  = 1,
      vdBoolean  = 2,
      vdinteger  = 3,   // covers 8 to 64 bits
      vdfloat    = 4,   // covers 16, 32 and 64 bits
      vdstring   = 5,
      vdSymbol   = 6,   // jsvm
      vdFunction = 7,
      vdObject   = 8,
      vdArray    = 9,
      vdVariant  = 10
    );

  THandle       = variant;
  TMemoryHandle = variant;

  // Standard array types

  TUInt8Array     = array of uint8;
  TInt8Array      = array of int8;
  TUInt16Array    = array of uint16;
  TInt16Array     = array of int16;
  TUInt32Array    = array of uint32;
  TInt32Array     = array of int32;
  TStrArray       = array of string;
  TBoolArray      = array of boolean;
  TSysCharSet     = array of char;

  TVariantArray   = array of variant;
  TObjectArray    = array of TObject;
  TStringArray    = array of string;
  TFloatArray     = array of float;
  THandleArray    = array of THandle;

  TEnumState = ( esBreak, esContinue );

  TObjectExtender = class helper for TObject
    function QualifiedClassName: string;
  end;

  EException = class(Exception)
  public
    constructor CreateFmt(Message: string; const Values: array of const); virtual;
  end;

  JError = class external "Error"
  public
    property columnNumber: integer;
    property fileName: string;
    property lineNumber: integer;
    property message: string;
    property name: string;
    property code: variant;   // Datatype depends on the error, can be both number and string
    function stack: TStrArray;
  end;

  EConvertError               = class(EException);
  EConvertHexStringInvalid    = class(EConvertError);
  EConvertHexInvalidContent   = class(EConvertError);
  EConvertBinaryStringInvalid = class(EConvertError);
  EConvertBinaryInvalidChar   = class(EConvertError);

  JTextEncoder = class external "TextEncoder"
    property encoding: string;
    function encode(const text: string): JUint8Array;
    constructor Create(TextFormat: string); overload; //utf-8 | utf-16 | iso-8859-2 | koi8 | cp1261 | gbk | windows-1251
    constructor Create; overload;
  end;

  JTextDecoder = class external "TextDecoder"
    property  encoding: string;
    property  fatal: boolean;
    property  ignoreBOM: boolean;
    function  decode(const Data: JUint8Array): string; overload;
    constructor Create(TextFormat: string); overload; //utf-8 | utf-16 | iso-8859-2 | koi8 | cp1261 | gbk | windows-1251
    constructor Create; overload;
  end;

  TJSObjectKeyCB = function (KeyName: string; KeyData: variant): TEnumState;

  TNotifyEvent  = procedure (Sender: TObject);

  // Standard byte typed array (alternatively JUint8ClampedArray)
  TStdBufferType = JUint8Array;

  TDataTypeConverter = class(TObject)
  private
    FBuffer:    JArrayBuffer;
    FView:      JDataView;
    FTyped:     TStdBufferType;
    FEndian:    TJSVMEndianType;
  protected
    procedure   SetEndian(const NewEndian: TJSVMEndianType); virtual;
  public
    property    Endian: TJSVMEndianType read FEndian write SetEndian;
    function    UInt16ToBytes(const Value: uint16): TUInt8Array;
    function    Int16ToBytes(const Value: int16): TUInt8Array;
    function    UInt32ToBytes(const Value: uint32): TUInt8Array;
    function    Int32ToBytes(const Value: int32): TUInt8Array;
    function    Float64ToBytes(const Value: float64): TUInt8Array;
    function    Float32ToBytes(const Value: float32): TUInt8Array;
    function    StringToBytes(const Value: string): TUInt8Array;
    function    VariantToBytes(const Value: variant): TUInt8Array;

    function    BytesToString(const Data: TUInt8Array): string;
    function    BytesToVariant(Data: TUInt8Array): variant;
    function    BytesToInt16(const Data: TUInt8Array): smallint;
    function    BytesToInt32(const Data: TUInt8Array): integer;
    function    BytesToFloat32(const Data: TUInt8Array): float;
    function    BytesToFloat64(const Data: TUInt8Array): float;
    function    BytesToUInt16(const Data: TUInt8Array): word;
    function    BytesToUInt32(const Data: TUInt8Array): longword;
    function    BytesToBoolean(const Data: TUInt8Array): boolean;
    function    BytesToTypedArray(const Values: TUInt8Array): TMemoryHandle;

    // Returns the platform endian notation.
    // If unsuccessful, falls back on "use default, dont force anything".
    // It is very important to check this if you plan or working with
    // files on an OS that has a different endian than x86
    class function  SystemEndian: TJSVMEndianType; static;

    class function  SizeOfType(const Kind: TJSVMDataType): integer; static;
    class function  NameOfType(const Kind: TJSVMDataType): string; static;
    class function  TypeByName(TypeName: string): TJSVMDataType; static;

    class function  BooleanToBytes(const Value: boolean): TUInt8Array; static;
    class function  StrToBase64(Value: string): string; static;
    class function  Base64ToStr(Value: string): string; static;

    class function  ByteToChar(const Value: byte): char; static;
    class function  CharToByte(const Value: char): word; static;

    class function  BytesToChar(const Value: TUInt8Array): char; static;
    class function  CharToBytes(const Value: char): TUInt8Array; static;

    class function  BytesToBase64(const Bytes: TUInt8Array): string; static;
    class function  Base64ToBytes(const Base64: String): TUInt8Array; static;

    class function InitUint08(const Value: uint8): uint8; static;
    class function InitUint16(const Value: uint16): uint16; static;
    class function InitUint32(const Value: uint32): uint32; static;
    class function InitInt08(const Value: int8): int8; static;
    class function InitInt16(const Value: int16): int16; static;
    class function InitInt32(const Value: int32): int32; static;
    class function InitFloat32(const Value: float32): float32; static;
    class function InitFloat64(const Value: float64): float64; static;

    function  ByteToTypedArray(const Value: byte): TMemoryHandle;
    function  Int16ToTypedArray(const Value: word): TMemoryHandle;
    function  Int32ToTypedArray(const Value: longword): TMemoryHandle;
    function  Float32ToTypedArray(const Value: float32): TMemoryHandle;
    function  Float64ToTypedArray(const Value: float64): TMemoryHandle;
    function  BooleanToTypedArray(const Value: boolean): TMemoryHandle;
    function  StringToTypedArray(const Value: string): TMemoryHandle;

    function  TypedArrayToStr(const Value: TMemoryHandle): string;
    function  TypedArrayToFloat32(const Value: TMemoryHandle): float32;
    function  TypedArrayToUInt32(const Value: TMemoryHandle): uint32;
    class function TypedArrayToBytes(const Value: TMemoryHandle): TUInt8Array; static;

    constructor Create; virtual;
    destructor  Destroy; override;
  published
    property OnEndianChanged: TNotifyEvent;
  end;

  TInteger = partial class abstract
  public
    class function FromPxStr(const Value: string): integer; static;
    class function ToPxStr(const Value: integer): string; static;
    class function FromPsStr(const Value: string): integer; static;
    class function ToPsStr(const Percent: integer): string; static;
    class function ToHex(const Value: integer): string; static;
    class function Sum(const Domain: TInt32Array): integer; static;
    class function Average(const Domain: TInt32Array): integer; static;
    class function PercentOfValue(const Value, Total: integer): integer; static;
    class function Middle(const Primary, Secondary: integer): integer; static;
    class function Sign(const Value: integer): integer; deprecated;
    class function Diff(const Primary, Secondary: integer): integer; static;
    class function ToNearestSigned(const Value, Factor: integer): integer; static;
    class function ToNearest(const Value, Factor: integer): integer; static;
    class function Largest(const Primary, Secondary: integer): integer; overload;
    class function Largest(const Domain: TInt32Array): integer; overload;
    class function Smallest(const Primary, Secondary: integer): integer; overload;
    class function Smallest(const Domain: TInt32Array): integer; overload;
    class function WrapRange(const Value, LowRange, HighRange: integer): integer; static;
    class function EnsureRange(const Value, Lowest, Highest: integer): integer; static;
    class function SubtractSmallest(const First, Second: integer):integer; static;
    class function SubtractLargest(const First, Second: integer):integer; static;
    class function WithinRange(const Value, Lowest, Highest: integer): boolean; static;
    class function Power(const Value, Factor: integer): integer; static;

    class function GetBit(index: int32; const buffer: int32): boolean; static;
    class procedure SetBit(index: int32; Value: boolean; var buffer: int32); static;

    class procedure Swap(var Primary, Secondary: integer); static; // "use .swap() intrinsic function";
    class procedure Sort(var Domain: TInt32Array); // "Use .Sort() intrinsinc function";
  end;

  TVariant = static class abstract
  public
    class function AsInteger(const aValue: variant): integer; static;
    class function AsString(const aValue: variant): string; static;
    class function AsFloat(const aValue: variant): float; static;
    class function AsObject(const aValue: variant): TObject; static;
    class function AsBool(const aValue: variant): boolean; static;

    class function IsArray(const aValue: variant): boolean; static;
    class function IsNull(const aValue: variant): boolean; static;
    class function IsUnDefined(const aValue: variant): boolean; static;
    class function IsString(const aValue: variant): boolean; static;
    class function IsNumber(const aValue: variant): boolean; static;
    class function IsInteger(const aValue: variant): boolean; static;
    class function IsBool(const aValue: variant): boolean; static;
    class function IsNAN(const aValue: variant): boolean; static;
    class function ClassInstance(const instance: variant): boolean; static;
    class function CreateObject: variant; static;
    class function CreateArray: variant; static;

    class function ExamineType(const Value: variant): TVariantExportType; static;

    class function GetKeys(const aValue: variant): TStrArray; static;
    class function GetOwnPropertyNames(const aValue: variant): TStrArray; static;
    class function GetOwnPropertyByEnum(const aValue: variant): TStrArray; static;

    class function Properties(const Data: variant): TStrArray; static;
    class function PropertyExists(const Data: variant; const Id: string): boolean; static;
    class function PropertyRead(const Data: variant; const Id: string): variant; static;
    class function PropertyWrite(const Data: variant; const Id:string;const Value:variant): variant;static;
    class function PropertyDelete(const Data: variant; const Id: string): variant;static;

    class function FromObject(const &Object: TObject): variant; static;
    class function ToObject(const Value: variant): TObject; static;

    // Copy over values as long as the properties exist in both objects
    class function Sync(const target, prop: variant): variant; overload; static;
    class function Sync(target : JObject; const prop: variant): JObject; overload; static;

    // raw intrinsic assign from A to B [js object]
    class procedure Assign(const source: variant; var target: variant); static;

    // raw create() + assign() of js object
    class function Clone(const Source: variant): variant; static;

    class procedure ForEachProperty(const Data: variant;
        const CallBack: TJSObjectKeyCB); static;

    class function SearchAssociate(const Field: string; const Value: variant;
      const AssociateArray: variant): variant; static;
  end;

  TFloat = static class abstract
  public
    class function PercentOfValue(const Value: double; Total: double): double; static;
    class function Power(const Value: double; const Factor: double): double; static;
  end;


  TValuePrefixType = (
    vpNone = 0,   // Unknown prefix
    vpHexPascal,  // $
    vpHexC,       // 0x
    vpBinPascal,  // %
    vpBinC,       // 0d
    vpString      // "
    );

  TString = static class abstract
  private
    class var CRC_Table_Ready: boolean = false;
    class var CRC_Table: array [0..512] of integer;
    class procedure BuildCRCTable;
  public
    class function EncodeBase64(TextToEncode: string): string; static;
    class function DecodeBase64(TextToDecode: string): string; static;
    class function CalcCRC(Text: string): integer; static;

    class function CreateGUID: string; static;

    class function ExamineTypePrefix(const Text: string;
      var Prefix: TValuePrefixType): boolean; static;

    class function ValidHexChars(const Text: string): boolean; static;
    class function ValidBinChars(const Text: string): boolean; static;
    class function ValidDecChars(const Text: string): boolean; static;

    class function ExamineFloat(Text: string; var Value: float): boolean; static;
    class function ExamineBoolean(Text: string; var Value: boolean): boolean; static;
    class function ExamineInteger(Text: string; var Value: integer): boolean; static;
    class function ExamineQuotedString(Text: string; var Value: string): boolean; static;
    class function ExamineBinary(Text: string; var value: longword): boolean; static;

    class function ResolveDataType(Text: string;
          var Determined: TJSVMDatatype): boolean; static;

    class function HexStrToInt(HexStr: string): integer; static;
    class function BinaryStrToInt(BinStr: string): integer; static;

    class function QuoteString(const Text: string): string; static;
    class function GetStringIsQuoted(const Text: string): boolean; static;
    class function UnQuoteString(const Text: string): string; static;

    class function Equals(const First, Second: string): boolean; static;

    class function Split(const Value: string; const Delimiter: string): TStrArray; static;

    class function EncodeURI(const Text: string): string;
    class function DecodeURI(const Text: string): string;

    class function EncodeURIComponent(const Text: string): string; static;
    class function DecodeURIComponent(const Text: string): string; static;

    class function EncodeUTF8(TextToEncode: string): TUInt8Array; static;
    class function DecodeUTF8(const BytesToDecode: TUInt8Array): string; static;

    class function CharCodeAt(const Text: string; const Index: integer): Byte; static;
    class function CharCodeFor(const Character: char): integer; static;
    class function FromCharCode(const CharCode: uint8): Char; static;

    class function RepeatChar(Count: integer; const Charcode: uint8): string; static;

    class function LeftPad(Pad: char; Value: string; FitLength: integer): string; static;
  end;

  TBase64EncDec = static class abstract
  public
    class function  ExtractPlaceholderCount(const b64: string): integer; static;
    class function  TripletToBase64(const num: integer): string; static;
    class function  EncodeChunk(const Data: TUInt8Array; startpos, endpos: integer): string; static;

    class function  CalcByteLength(const b64: string): integer;  static;
    class function  Base64ToBytes(const b64: string): TUInt8Array;  static;
    class function  BytesToBase64(const Data: TUInt8Array): string;  static;
    class function  StringToBase64(const Text: string): string;  static;
    class function  Base64ToString(const b64: string): string;  static;
  end;

function TryStrToInt(Data: string; var Value: integer): boolean;
function TryStrToBool(Data: string; var Value: boolean): boolean;
function TryStrToFloat(Data: string; var Value: float): boolean;

procedure WriteLn(const value: Variant);
procedure WriteLnF(const Text: string;const Values: array of Const);

// Interop methods, maps directly to JSVM functions
function jsTypeOf(const reference: variant): string; external "typeof";
function jsInstanceOf(const reference: variant): variant; external "instanceof";
function jsThis: variant; external 'this' property;
function jsObject: variant; external 'Object' property;

implementation

uses
  qtx.codec.utf8;

const
  // These are the sizes in bytes of various datatypes.
  CNT_SIZEOF_UNKNOWN  = 0;
  CNT_SIZEOF_BOOLEAN  = 1;
  CNT_SIZEOF_BYTE     = 1;
  CNT_SIZEOF_CHAR     = 2;  //raw wide-char, must be encoded for transport
  CNT_SIZEOF_WORD     = 2;
  CNT_SIZEOF_LONG     = 4;
  CNT_SIZEOF_INT16    = 2;
  CNT_SIZEOF_INT32    = 4;
  CNT_SIZEOF_FLOAT32  = 4;  // single
  CNT_SIZEOF_FLOAT64  = 8;  // double
  CNT_SIZEOF_STRING   = CNT_SIZEOF_INT32 * 2;

  // Constants used by VariantToBytes() and BytesToVariant() methods
  cnt_datatype_root     = $10;
  cnt_datatype_boolean  = cnt_datatype_root + 1;
  cnt_datatype_byte     = cnt_datatype_root + 2;
  cnt_datatype_int16    = cnt_datatype_root + 3;
  cnt_datatype_int32    = cnt_datatype_root + 4;
  cnt_datatype_float32  = cnt_datatype_root + 5;
  cnt_datatype_float64  = cnt_datatype_root + 6;
  cnt_datatype_string   = cnt_datatype_root + 7;
  cnt_datatype_Uint16   = cnt_datatype_root + 8;
  cnt_datatype_Uint32   = cnt_datatype_root + 9;


// When checking for datatypes I use a lookup table. This table is populated
// when the unit is loaded, and allows faster type checking
type
  TTypeLookup = record
    &Boolean:  variant;
    &Number: variant;
    &String: variant;
    &Object: variant;
    &Undefined: variant;
    &Function: variant;
  end;

var
  __TYPE_MAP: TTypeLookup;

  // While JSVM operates with some esoteric types, the data always bakes down to
  // ordinary, intrinsic datatypes common to all languages. Here we have a quick
  // lookup table we use to get the size of a defined-type
  __SIZES: array[TJSVMDataType] of integer =
    ( CNT_SIZEOF_UNKNOWN,
      CNT_SIZEOF_BOOLEAN,
      CNT_SIZEOF_BYTE,
      CNT_SIZEOF_CHAR,
      CNT_SIZEOF_WORD,
      CNT_SIZEOF_LONG,
      CNT_SIZEOF_INT16,
      CNT_SIZEOF_INT32,
      CNT_SIZEOF_FLOAT32,
      CNT_SIZEOF_FLOAT64,
      CNT_SIZEOF_STRING );

  _NAMES: array[TJSVMDataType] of string =
    ( "Unknown",
      "Boolean",
      "Byte",
      "Char",
      "Word",
      "Longword",
      "Smallint",
      "Integer",
      "Single",
      "Double",
      "String" );

    __B64_Lookup: array [0..256] of char;
    __B64_RevLookup: array [0..256] of integer;
    CNT_B64_CHARSET: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';


resourcestring
  CNT_ERR_BITS_INDEX = 'Invalid bit index, expected 0..31';
  CNT_ERR_CONVERT_BYTESToVariant = 'Failed to convert bytes[] to intrinsic type, unknown identifier [%s] error';
  CNT_ERR_CONVERT_NUMBERID  = 'Invalid datatype, failed to identify number [integer] type error';
  CNT_ERR_CONVERT_DATATYPE = 'Invalid datatype, byte conversion failed error';

function TryStrToInt(Data: string; var Value: integer): boolean;
begin
  result := TString.ExamineInteger(Data, Value);
end;

function TryStrToBool(Data: string; var Value: boolean): boolean;
begin
  result := TString.ExamineBoolean(Data, Value);
end;

function TryStrToFloat(Data: string; var Value: float): boolean;
begin
  result := TString.ExamineFloat(Data, Value);
end;

procedure WriteLnF(const Text: string; const Values: Array of const);
begin
  try
    var FormText := Format(Text, Values);
    asm
    console.log(@FormText);
    end;
  except
    on e: exception do;
  end;
end;

procedure WriteLn(const value : Variant);
begin
  if TVariant.IsString(value) then
  begin
    if pos(#13, value) > 0 then
    begin
      var items := value.split(#13);
      for var litem in items do
      begin
        asm
        console.log(@litem);
        end;
      end;
      exit;
    end;
  end else
  if TVariant.IsArray(value) then
  begin
    var LArr: array of variant;
    asm
      @LArr = @value;
    end;
    for var item in Larr do
    begin
      writeln(item);
    end;
    exit;
  end;
  asm
  console.log(@value);
  end;
end;

//#############################################################################
// Tfloat
//#############################################################################

class function Tfloat.PercentOfValue(const Value: double; Total: double): double;
begin
  if (Value <= Total) then
    result := ( (Value / Total) * 100 );
end;

class function Tfloat.Power(const Value: double; const Factor: double): double;
begin
  asm
    @result = Math.pow(@value,@factor);
  end;
end;

//#############################################################################
// TString
//#############################################################################

class function TString.BinaryStrToInt(BinStr: string): integer;
begin
  if not ExamineBinary(BinStr, result) then
    raise EConvertBinaryStringInvalid.CreateFmt('Failed to convert binary string (%s)', [binstr]);
end;

class function TString.HexStrToInt(HexStr: string): integer;
begin
  if not ExamineInteger(HexStr, result) then
    raise EConvertHexStringInvalid.CreateFmt('Failed to convert hex string (%s)', [HexStr]);
end;

class function TString.ExamineTypePrefix(const Text: string; var Prefix: TValuePrefixType): boolean;
begin
  Prefix := vpNone;
  if Text.length > 0 then
  begin
    if Text.StartsWith('$' ) then Prefix := vpHexPascal else
    if Text.StartsWith('0x') then Prefix := vpHexC else
    if Text.StartsWith('%' ) then Prefix := vpBinPascal else
    if Text.StartsWith('0b') then Prefix := vpBinC else
    if Text.StartsWith('"' ) then Prefix := vpString;
    result := Prefix <> vpNone;
  end;
end;

class function TString.ResolveDataType(Text: string;
  var Determined: TJSVMDatatype): boolean;
var
  DummyBoolean: boolean;
  DummyFloat: float;
  Prefix: TValuePrefixType;
begin
  result := false;
  Determined := dtUnknown;

  // check boolean first
  if ExamineBoolean(Text, DummyBoolean) then
  begin
    Determined := TJSVMDatatype.dtBoolean;
    result := true;
    exit;
  end;

  // Check float
  if ExamineFloat(Text, DummyFloat) then
  begin
    Determined := TJSVMDatatype.dtFloat64;
    result := true;
    exit;
  end;

  // Check non-prefixed integer
  if ValidDecChars(Text) then
  begin
    Determined := TJSVMDatatype.dtInt32;
    result := true;
    exit;
  end;

  if ExamineTypePrefix(Text, Prefix) then
  begin
    case Prefix of
      vpString:
        begin
          Determined := TJSVMDatatype.dtString;
        end;
      vpHexPascal:
        begin
          // Remove $
          Text := Text.Right(Text.length-1);
          case Text.length of
          1:  Determined := TJSVMDatatype.dtByte;  //A
          2:  Determined := TJSVMDatatype.dtByte;  //AA
          4:  Determined := TJSVMDatatype.dtWord;  //AAAA
          8:  Determined := TJSVMDatatype.dtLong;  //AAAAAAAA
          end;
        end;
      vpHexC:
        begin
          // Remove 0x
          Text := Text.Right(Text.length-2);
          case Text.length of
          1:  Determined := TJSVMDatatype.dtByte;  //A, AA
          2:  Determined := TJSVMDatatype.dtByte;  //A, AA
          4:  Determined := TJSVMDatatype.dtWord;  //AAAA
          8:  Determined := TJSVMDatatype.dtLong;  //AAAAAAAA
          end;
        end;
      vpBinPascal:
        begin
          // Remove %
          Text := Text.Right(Text.length-1);
          if (Text.length <= 8) then
            Determined := TJSVMDatatype.dtByte
          else
          if ((Text.Length >8) and (Text.Length <=16)) then
            Determined :=  TJSVMDatatype.dtWord
          else
          if ((Text.Length >16) and (Text.Length <=32)) then
            Determined :=  TJSVMDatatype.dtLong;
        end;
      vpBinC:
        begin
          // Remove 0b
          Text := Text.Right(Text.length-2);
          if (Text.length <= 8) then
            Determined := TJSVMDatatype.dtByte
          else
          if ((Text.Length >8) and (Text.Length <=16)) then
            Determined :=  TJSVMDatatype.dtWord
          else
          if ((Text.Length >16) and (Text.Length <=32)) then
            Determined :=  TJSVMDatatype.dtLong;
        end;
    end;

    result := Determined <> dtUnknown;

  end;
end;

class function TString.ExamineQuotedString(Text: string; var Value: string): boolean;
begin
  Text := Text.trim();
  var TextLen := Text.Length;
  if TextLen > 0 then
  begin
    if Text.StartsWith('"') then
    begin
      if Text.EndsWith('"') then
      begin
        dec(TextLen, 2);
        value := Copy(Text, 2, TextLen);
        result := true;
      end;
    end;
  end;
end;

class function TString.ExamineBinary(Text: string; var Value: longword): boolean;
begin
  // Initialize result from a proper uint32
  Value := TDataTypeConverter.InitUInt32(0);

  // check pascal notation
  if text.StartsWith('%') then
    Text := Text.DeleteLeft(1)
  else
  // check C/C++ notation
  if text.StartsWith('0b') then
    Text := Text.DeleteLeft(2);

  // Validate that string contains only zeroes and ones.
  if not ValidBinChars(Text) then
  begin
    result := false;
    exit;
  end;

  // Setup bit-index
  var BitIndex := 0;

  // process from LSB to MSB
  for var x := high(Text) downto low(Text) do
  begin
    if  Text[x] = '1' then
      TInteger.SetBit(BitIndex, true, Value);

    inc(bitindex);
    if bitIndex > 31 then
      break;
  end;

  result := true;
end;

class function TString.ExamineFloat(Text: string; var Value: float): boolean;
begin
  Text := text.trim();
  var TextLen := Text.Length;
  if TextLen >= 1 then
  begin
    var scan := false;
    var offset := 0;

    for var character in Text do
    begin
      inc(offset);

      // we only allow one "." per floating point string
      if character = '.' then
      begin
        // Dot as first char, but no more data?
        if (offset = 1) and (TextLen = 1) then
        begin
          break;
        end;

        // dot as first char, but with more text afterwards
        if (offset = 1) and (TextLen > 1) then
        begin
          scan := true;
          continue;
        end;

        // dot somewhere in the middle, but not the last char!
        if (offset > 1) and (offset < TextLen) then
        begin
          if not scan then
          begin
            scan := true;
            continue;
          end else
          break;
        end else

        // its a dot but it doesnt follow the rules
        // so just exit, this doesnt validate
        break;
      end;

      result := character in ['0'..'9'];
      if not result then
        break;
    end;

    if result then
      Value := StrToFloat(Text);
  end;
end;

class function TString.ValidHexChars(const Text: string): boolean;
begin
  for var character in text do
  begin
    result := character in ['0'..'9', 'a'..'f', 'A'..'F'];
    if not result then
      break;
  end;
end;

class function TString.ValidDecChars(const Text: string): boolean;
begin
  for var character in text do
  begin
    result := character in ['0'..'9'];
    if not result then
      break;
  end;
end;

class function TString.ValidBinChars(const Text: string): boolean;
begin
  for var character in text do
  begin
    result := character in ["0", "1"];
    if not result then
      break;
  end;
end;

class function TString.ExamineInteger(Text: string; var Value: integer): boolean;
begin
  Text := Text.trim();
  var TextLen := Text.Length;
  if TextLen > 0 then
  begin
    var Prefix := TValuePrefixType.vpNone;
    if ExamineTypePrefix(Text, Prefix) then
    begin
      case Prefix of
      vpHexPascal:
        begin
          dec(TextLen);
          Text := Text.Right(TextLen);
          result := ValidHexChars(Text);
          if result then
            Value := HexToInt("0x" + Text);
        end;
      vpHexC:
        begin
          dec(TextLen, 2);
          Text := Text.Right(TextLen);
          result := ValidHexChars(Text);
          if result then
            Value := HexToInt('0x' + Text);
        end;
      vpBinPascal:
        begin
          dec(TextLen);
          Text := Text.Right(TextLen);
          result := ValidBinChars(Text);

          if result then
            Value := BinaryStrToInt(Text);
        end;
      vpBinC:
        begin
          dec(TextLen, 2);
          Text := Text.Right(TextLen);
          result := ValidBinChars(Text);

          if result then
            Value := BinaryStrToInt(Text);
        end;
      vpString:
        begin
          exit;
        end;
      else
        begin
          result := ValidDecChars(Text);
          if result then
            Value := StrToInt(Text);
        end;
      end;
    end else
    begin
      // No prefixing, validate normal decimal integer
      result := ValidDecChars(Text);
      if result then
        Value := StrToInt(Text);
    end;
  end;
end;

class function TString.ExamineBoolean(Text: string; var Value: boolean): boolean;
begin
  Text := Text.Trim();
  case Text.ToLower() of
  "true", "yes":
    begin
      result := true;
      value := true;
    end;
  "false", "no":
    begin
      result := true;
      value := false;
    end;
  end;
end;

class function TString.GetStringIsQuoted(const Text: string): boolean;
begin
  var TextLen := Text.Length;
  if TextLen > 0 then
    result := Text.StartsWith('"') and Text.EndsWith('"');
end;

class function TString.QuoteString(const Text: string): string;
begin
  if not GetStringIsQuoted(Text) then
    result := '"' + Text + '"'
  else
    result := Text;
end;

class function TString.UnQuoteString(const Text: string): string;
begin
  var TextLen := Text.Length;
  if TextLen > 0 then
  begin
    if Text.StartsWith('"') and Text.EndsWith('"') then
    begin
      dec(TextLen, 2);
      result := copy(Text, 2, TextLen);
    end else
      result := Text;
  end;
end;

// http://www.ietf.org/rfc/rfc4122.txt
class function TString.CreateGUID:String;
Begin
  asm
    var s = [];
    var hexDigits = "0123456789ABCDEF";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
    s[8] = s[13] = s[18] = s[23] = "-";

    @result = s.join("");
  end;
end;


class function TString.EncodeBase64(TextToEncode: string): string;
begin
  result := TBase64EncDec.StringToBase64(TextToEncode);
end;

class function TString.DecodeBase64(TextToDecode: string): string;
begin
  result := TBase64EncDec.Base64ToString(TextToDecode);
end;

class function TString.CalcCRC(Text: string): integer;
begin
  var LLen := Text.length;
  if LLen > 0 then
  begin

    if not CRC_Table_Ready then
    begin
      BuildCRCTable();
      CRC_Table_Ready := true;
    end;

    var LBytes := EncodeUTF8(text);

    var LResult:= $FFFFFFFF;
    for var x := 0 to LBytes.length-1 do
    begin
      var LCharCode := LBytes[x];
      LResult:= (LResult shr 8)
        xor CRC_TABLE[LCharCode
        xor (LResult and $000000FF)];
    end;

    asm
      @result = ( (@LResult ^ -1) >>> 0);
    end;
  end;
end;

class procedure TString.BuildCRCTable;
const
  CRCPOLY = $EDB88320;
var
  x, y, r: integer;
begin
  (* Build the classic CRC bytemap *)
  for x := 0 to 255 do
  begin
    r := x shl 1;
    for y := 8 downto 0 do
    begin
      if (r and 1) <> 0 then
      r := (r Shr 1) xor CRCPOLY else
      r := r shr 1;
      CRC_TABLE[x] := r;
    end;
  end;
end;

//#############################################################################
// TVariant
//#############################################################################

class function TVariant.Asinteger(const aValue: variant): integer;
begin
  if (aValue) then
    result := integer(aValue);
end;

class function TVariant.Asstring(const aValue: variant): string;
begin
  if (aValue) then
    result := string(aValue);
end;

class function TVariant.Asfloat(const aValue: variant): float;
begin
  if (aValue) then
    result := float(aValue);
end;

class function TVariant.AsObject(const aValue: variant): TObject;
begin
  if (aValue) then
  begin
    asm
      @result = @aValue;
    end;
  end;
end;

class function TVariant.AsBool(const aValue: variant): Boolean;
begin
  if (aValue) then
    result := boolean(aValue);
end;

class function TVariant.ClassInstance(const instance: variant): boolean;
begin
  result := true;
  asm
    if (typeof @instance === 'object') {
      if (@instance = {}) {
        @result = false;
      }
    } else {
      @result = false;
    }
  end;
end;

{$HINTS OFF}
class function TVariant.CreateObject: variant;
begin
  asm
    @result = new Object();
  end;
end;

class function TVariant.CreateArray: variant;
begin
  asm
    @result = new Array();
  end;
end;


class function TVariant.ExamineType(const Value: variant): TVariantExportType;
begin
  if (value) then
  begin
    case jsTypeOf(Value).ToLower() of
    "object": result := if value.hasOwnProperty("length") then vdArray else vdObject;
    "function": result := vdFunction;
    "symbol":   result := vdSymbol;
    "boolean":  result := vdBoolean;
    "string":   result := vdstring;
    "number":
      begin
        result := if not (value mod 1) = 0 then vdFloat else vdInteger;
      end;
    'array':  result := vdArray;
    else
      result := vdUnknown;
    end;
  end else
  result := vdUnknown;
end;

class function TVariant.IsUnDefined(const aValue: variant): boolean;
begin
  result := jsTypeOf(avalue) = "undefined";
end;

class function TVariant.IsNull(const aValue: variant): boolean;
begin
  result := (aValue = null);
end;

class function TVariant.IsString(const aValue: variant): boolean;
begin
  result := jsTypeOf(aValue) = __TYPE_MAP.String;
end;

class function TVariant.IsArray(const aValue: variant): boolean;
begin
  asm
    if (Array.isArray(@aValue) = true) {
      @result = true;
    }
  end;
end;

class function TVariant.IsNumber(const aValue: variant): boolean;
begin
  result := jsTypeOf(aValue) = __TYPE_MAP.Number;
end;

class function TVariant.Isinteger(const aValue: variant): boolean;
begin
  asm
    if (@aValue == null) return false;
    if (@aValue == undefined) return false;
    if (typeof(@aValue) === "number") if (parseInt(@aValue) === @aValue) return true;
  end;
end;

class function TVariant.IsBool(const aValue: variant): boolean;
begin
  result := jsTypeOf(aValue) = __TYPE_MAP.Boolean;
end;

class function TVariant.IsNAN(const aValue: variant): boolean;
begin
   result := Internal.IsNaN(aValue);
end;

class function TVariant.GetKeys(const aValue: variant): TStrArray;
begin
  asm
    if (Object.keys) {
      @result = Object.keys(@aValue);
    } else {
      @result = [];
    }
  end;
end;

class function TVariant.GetOwnPropertyNames(const aValue: variant): TStrArray;
begin
  asm
    @result = Object.getOwnPropertyNames(@aValue);
  end;
end;

class function TVariant.GetOwnPropertyByEnum(const aValue: variant): TStrArray;
var
  LName:  string;
begin
  asm
    for (@LName in @aValue) {
      if ( (@aValue).hasOwnProperty(@Lname) == true )
        (@result).push(@Lname);
    }
  end;
end;

class procedure TVariant.ForEachProperty(const Data: variant;
      const CallBack: TJSObjectKeyCB);
begin
  if Assigned(Callback) then
  begin
    var LObj: Variant;
    var Keys := TVariant.Properties(Data);
    for var LName in Keys do
    begin
      asm
        @LObj = @Keys[@LName];
      end;
      if not CallBack(LName, LObj) = TEnumState.esContinue then
        break;
    end;
  end;
end;

class function TVariant.Properties(const Data: variant): TStrArray;
begin
  if (data) then
  begin

    /* Does the engine support keys? */
    asm
      if (!(Object.keys === undefined)) {
        @result = Object.keys(@Data);
        return @result;
      }
    end;

    /* Does the engine support getOwnPropertyNames? */
    asm
      if (!(Object.getOwnPropertyNames === undefined)) {
          @result = Object.getOwnPropertyNames(@data);
          return @result;
      }
    end;

    /* Fall back to per-item enumeration */
    asm
      for (var qtxenum in @Data) {
        if ( (@data).hasOwnProperty(qtxenum) == true )
          (@result).push(qtxenum);
      }
      return @result;
    end;
  end;
end;

class function TVariant.PropertyExists(const Data:variant;
  const Id: string): boolean;
begin
  asm
    @result = (@data).hasOwnProperty(@Id);
  end;
end;

class function TVariant.PropertyRead(const Data: variant;
  const Id: string): variant;
begin
  asm
    @result = @Data[@Id];
  end;
end;

class function TVariant.PropertyWrite(const Data: variant;
  const Id: string;const Value: variant):Variant;
begin
  asm
    @result = @data;
    @result[@Id] = @value;
  end;
end;

class function TVariant.PropertyDelete(const Data: variant;
  const Id: string): variant;
begin
  if (data) then
  begin
    if (data[Id]) then
    begin
      asm
        delete @data[@Id];
      end;
    end;
  end;
  result := data;
end;

class function TVariant.FromObject(const &Object: TObject): variant;
begin
  Result := Variant(&Object);
end;

class function TVariant.ToObject(const Value: variant): TObject;
begin
  asm
    @Result = @value;
  end;
end;

class function TVariant.Clone(const Source: variant): variant;
begin
  if (source) then
  begin
    result := TVariant.CreateObject();
    result.assign(source);
  end;
end;

class procedure TVariant.Assign(const source: variant; var target: variant);
begin
  if (source) and TVariant.ExamineType(Source) = vdObject then
  begin
    if (Target) and TVariant.ExamineType(target) = vdObject then
      Target.assign(source);
  end;
end;

class function TVariant.Sync(const target, prop: variant) : variant;
var
  n : variant;
begin
  asm
    for (@n in @prop) if ((@prop).hasOwnProperty(@n)) @target[@n]=@prop[n];
    @Result = @target;
  end;
end;

class function TVariant.Sync(target: JObject; const prop: variant) : JObject;
var
   n : variant;
begin
  asm
    for (@n in @prop) if ((@prop).hasOwnProperty(@n)) @target[@n]=@prop[n];
    @Result = @target;
  end;
end;
{$HINTS ON}

class function TVariant.SearchAssociate(const Field: string; const Value: variant;
    const AssociateArray: variant): variant;
begin
  (* Setup search expansion *)
  var index    := 0;
  var LLongs   := AssociateArray.length shr 3;
  var LSingles := AssociateArray.length mod 8;

  (* Search batch of 8 items *)
  while (LLongs > 0) do
  begin
    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    if AssociateArray[index][Field] = Value then
    begin result := AssociateArray[index]; exit; end;
    inc(index);

    dec(LLongs);
  end;

  (* Search odd single items at the end *)
  while (LSingles > 0) do
  begin
    if AssociateArray[index][Field] = Value then
    begin
      result := AssociateArray[index];
      exit;
    end;
    inc(index);
    dec(LSingles);
  end;
end;

//#############################################################################
// TInteger
//#############################################################################

class procedure TInteger.SetBit(Index: int32; Value: boolean; var Buffer: int32);
begin
  if (index >= 0) and (index <= 31) then
  begin
    if Value then
       buffer := ( buffer or (1 shl index) )
    else
      buffer := ( buffer and not (1 shl index) );
  end else
    raise EException.Create(CNT_ERR_BITS_INDEX);
end;

class function TInteger.GetBit(index: int32; const buffer: int32): boolean;
begin
  if (index >= 0) and (index <= 31) then
    Result := (buffer and (1 shl index)) <> 0
  else
    raise EException.Create(CNT_ERR_BITS_INDEX);
end;

class function TInteger.FromPsStr(const Value: string): integer;
begin
  if Value.ToLower().EndsWith('%') then
  begin
    var LText := Copy(Value, 1, length(Value)-2 );
    if TVariant.IsNumber(LText) then
      result := StrToInt(LText);
  end;
end;

class function TInteger.ToPsStr(const Percent: integer): string;
begin
  result := IntToStr(Percent) + '%';
end;

class function TInteger.FromPxStr(const Value: string): integer;
begin
  if Value.ToLower().EndsWith('px') then
  begin
    var LText := Copy(Value, 1, length(Value)-2 );
    if TVariant.IsNumber(LText) then
      result := StrToInt(LText);
  end;
end;

class function TInteger.ToPxStr(const Value: integer): string;
begin
  Result := IntToStr(Value) + 'px';
end;

class function TInteger.ToHex(const Value: integer): string;
begin
  Result := IntToHex(Value, 16);
  if Length(Result) < 2 then
  begin
    repeat
      Result := ('0' + Result);
    until Length(Result)>=2;
  end;
  Result := Copy(Result, Length(Result) - 1, 2);
end;

class procedure TInteger.Sort(var Domain: TInt32Array);
begin
   Domain.Sort;
end;

class function TInteger.SubtractSmallest(const First,Second:integer):integer;
begin
  if First < Second then
    result := Second - First
  else
    result := First - Second;
end;

class function TInteger.SubtractLargest(const First,Second:integer):integer;
begin
  if First > Second then
    result := Second - First
  else
    result := First - Second;
end;

class function TInteger.Power(const Value:integer;const Factor: integer): integer;
begin
  asm
    @result = math.pow(@Value,@Factor);
  end;
end;

class function TInteger.WithinRange(const Value, Lowest, Highest: integer): boolean;
begin
  result := (Value >= Lowest) and (Value <= Highest);
end;

class function TInteger.EnsureRange(const Value, Lowest, Highest: integer): integer;
begin
  result := if Value < Lowest then Lowest else if Value > Highest then Highest else Value;
end;

class procedure TInteger.Swap(var Primary, Secondary: integer);
var
  temp: integer;
begin
  temp := Primary;
  Primary := Secondary;
  Secondary := temp;
end;

class function TInteger.WrapRange(const Value, LowRange, HighRange: integer): integer;
begin
  if Value > HighRange then
  begin
    result := LowRange + Diff(HighRange, (Value - 1));
    if result > HighRange then
      result := WrapRange(result, LowRange, HighRange);
  end else
  if Value < LowRange then
  begin
    result := HighRange - Diff(LowRange, Value + 1);
    if result < LowRange then
      result := WrapRange(result, LowRange, HighRange);
  end else
    result := Value;
end;

class function TInteger.ToNearest(const Value, Factor: integer): integer;
var
  FTemp: integer;
begin
  Result := Value;
  FTemp := Value mod Factor;
  if FTemp > 0 then
    Inc(result, Factor - FTemp);
end;

class function TInteger.ToNearestSigned(const Value, Factor: integer): integer;
begin
  Result := (1 + (Value - 1) div Factor) * Factor;
end;

class function TInteger.Diff(const Primary, Secondary: integer): integer;
begin
  if Primary <> Secondary then
  begin
    if Primary > Secondary then
      Result := Primary - Secondary
    else
      Result := Secondary - Primary;

    if Result < 0 then
      Result := Result - 1 xor -1;
  end else
    Result := 0;
end;

class function TInteger.Sign(const Value: integer): integer;
begin
   Result := Value.Sign;
end;

class function TInteger.Middle(const Primary, Secondary: integer): integer;
begin
  Result := (Primary + Secondary) div 2;
end;

class function TInteger.PercentOfValue(const Value, Total: integer): integer;
begin
  if Value <= Total then
    result := Trunc( (Value / Total) * 100 );
end;

class function TInteger.Average(const Domain: TInt32Array): integer;
begin
  var LCount := Domain.length();
  if LCount > 0 then
    result := Tinteger.Sum(Domain) div LCount;
end;

class function TInteger.Sum(const Domain: TInt32Array): integer;
begin
  result := 0;
  for var x in Domain do
    result += x;
end;

class function TInteger.Largest(const Primary, Secondary: integer): integer;
begin
  if Primary > Secondary then
    result := Primary
  else
    result := Secondary;
end;

class function TInteger.Largest(const Domain: TInt32Array): integer;
begin
   for var x in Domain do
   begin
    if x > result then
      result := x;
   end;
end;

class function TInteger.Smallest(const Primary,Secondary: integer): integer;
begin
  if Primary < Secondary then
    result := Primary
  else
    result := Secondary;
end;

class function TInteger.Smallest(const Domain: TInt32Array): integer;
begin
   result := High(Domain);
   if Result >= 0 then
   begin
      Result := Domain[0];
      for var x in Domain do
      begin
         if x < Result then
            Result := x;
      end;
   end;
end;

//#############################################################################
// TDataTypeConverter
//#############################################################################

constructor TDataTypeConverter.Create;
begin
  inherited Create;
  asm
    @FBuffer = new ArrayBuffer(16);
    @FView   = new DataView(@FBuffer);
  end;
  FTyped := TStdBufferType.Create(FBuffer, 0, 15);
end;

destructor TDataTypeConverter.Destroy;
begin
  FTyped := nil;
  FView := nil;
  FBuffer := nil;
  inherited;
end;

procedure TDataTypeConverter.SetEndian(const NewEndian: TJSVMEndianType);
begin
  if NewEndian <> FEndian then
  begin
    FEndian := NewEndian;

    if assigned(OnEndianChanged) then
      OnEndianChanged(self);
  end;
end;

class function TDataTypeConverter.SystemEndian: TJSVMEndianType;
begin
  // The constants may change so we just cache the values
  // in their ordinal form rather than hardcoding anything
  var LLittle := ord(stLittleEndian);
  var LBig := ord(stBigEndian);

  try
    asm
      var LBuffer = new ArrayBuffer(2);
      var L8Array = new Uint8Array(LBuffer);
      var L16array = new Uint16Array(LBuffer);
      L8Array[0] = 0xAA;
      L8Array[1] = 0xBB;
      if(L16array[0] === 0xBBAA) {
        @result = @LLittle;
      } else {
        if (L16array[0] === 0xAABB) @result = @LBig;
      }
    end;
  except
    // Sink exception
  end;
end;

class function TDataTypeConverter.SizeOfType(const Kind: TJSVMDatatype): integer;
begin
  result := __SIZES[KIND];
end;

class function TDataTypeConverter.NameOfType(const Kind: TJSVMDatatype): string;
begin
  result := _NAMES[Kind];
end;

class function TDataTypeConverter.TypeByName(TypeName: string): TJSVMDatatype;
begin
  result := dtUnknown;
  TypeName := TypeName.Trim().ToLower();
  if TypeName.Length > 0 then
  begin
    var x := 0;
    for var Name in _Names do
    begin
      if Name.ToLower() = TypeName then
      begin
        result := TJSVMDatatype(x);
        break;
      end;
      inc(x);
    end;
  end;
end;

class function TDataTypeConverter.InitFloat32(const Value: float32): float32;
begin
  var temp := new Jfloat32Array(1);
  temp[0] := Value;
  result := temp[0];
end;

class function TDataTypeConverter.InitFloat64(const Value: float64): float64;
begin
  var temp := new Jfloat64Array(1);
  temp[0] := Value;
  result := temp[0];
end;

class function TDataTypeConverter.InitInt32(const Value: integer): integer;
begin
  var temp := new Jint32Array(1);
  temp[0] := if Value < -2147483648 then -2147483648 else if Value > 2147483647 then 2147483647 else Value;
  result := temp[0];
end;

class function TDataTypeConverter.InitInt16(const Value: smallint): smallint;
begin
  var temp := new Jint16Array(1);
  temp[0] := if Value < -32768 then -32768 else if Value > 32767 then 32767 else Value;
  result := temp[0];
end;

class function TDataTypeConverter.InitInt08(const Value: byte): byte;
begin
  var temp := new Jint8Array(1);
  temp[0] := if value < -128 then -128 else if Value > 127 then 127 else Value;
  result := temp[0];
end;

class function TDataTypeConverter.InitUint32(const Value: longword): longword;
begin
  var temp := new JUint32Array(1);
  temp[0] := if Value < 0 then 0 else if value > $FFFFFFFF then $FFFFFFFF else value;
  result := temp[0];
end;

class function TDataTypeConverter.InitUint16(const Value: word): word;
begin
  var temp := new JUint16Array(1);
  temp[0] := if Value < 0 then 0 else if value > 65536 then 65536 else value;
  result := temp[0];
end;

class function TDataTypeConverter.InitUint08(const Value: byte): byte;
begin
  var LTemp := new JUint8Array(1);
  Ltemp[0] := if Value < 0 then 0 else if Value > 255 then 255 else Value;
  result := LTemp[0];
end;

function TDataTypeConverter.Float32ToTypedArray(const Value: float32): TMemoryHandle;
begin
  // write intrinsic value to pre-allocated buffer
  case Endian of
  stDefault:      FView.setFloat32(0, Value);
  stLittleEndian: FView.setFloat32(0, Value, true);
  stBigEndian:    FView.setFloat32(0, Value, false);
  end;
  result := FTyped.slice(0, __SIZES[dtFloat32]-1);
end;


function TDataTypeConverter.TypedArrayToFloat32(const Value: TMemoryHandle): float32;
begin
  // make sure handle is valid
  if not (Value) then
    raise EConvertError.Create('Failed to convert, handle was nil or unassigned error');

  // Get the length of the typed-array's buffer in bytes
  var LBuffer := TStdBufferType(Value).buffer;
  var LBytes := LBuffer.byteLength;

  // Check if there are enough bytes to represent the value
  if LBytes < __SIZES[dtFloat32] then
    raise EConvertError.Create('Failed to convert, insufficient data error');

  // Clip then length if it goes beyond needed bytes
  if LBytes > __SIZES[dtFloat32] then
    LBytes := __SIZES[dtFloat32];

  // Create a view into the buffer
  var LView := new JDataView(LBuffer);

  // Read out the float value
  case Endian of
  stDefault:      result := LView.getFloat32(0);
  stLittleEndian: result := LView.getFloat32(0, true);
  stBigEndian:    result := LView.getFloat32(0, false);
  end;

  LView := nil;
end;

function TDataTypeConverter.TypedArrayToUInt32(const Value: TMemoryHandle): longword;
begin
  // make sure handle is valid
  if not (Value) then
    raise EConvertError.Create('Failed to convert, handle was nil or unassigned error');

  // Get the length of the typed-array's buffer in bytes
  var LBuffer := TStdBufferType(Value).buffer;
  var LBytes := LBuffer.byteLength;

  // Check if there are enough bytes to represent the value
  if LBytes < __SIZES[dtInt32] then
    raise EConvertError.Create('Failed to convert, insufficient data error');

  // Clip then length if it goes beyond needed bytes
  if LBytes > __SIZES[dtInt32] then
    LBytes := __SIZES[dtInt32];

  // Create a view into the buffer
  var LView := new JDataView(LBuffer);

  // Read out the float value
  case Endian of
  stDefault:      result := LView.getUint32(0);
  stLittleEndian: result := LView.getUint32(0, true);
  stBigEndian:    result := LView.getUint32(0, false);
  end;

  LView := nil;
end;

function TDataTypeConverter.Int32ToBytes(const Value: Integer): TUInt8Array;
begin
  case Endian of
  stDefault:      FView.setInt32(0, Value);
  stLittleEndian: FView.setInt32(0, Value, true);
  stBigEndian:    FView.setInt32(0, Value, false);
  end;

  var LTypeSize := __SIZES[dtInt32];
  dec(LTypeSize);
  asm
    @result = Array.prototype.slice.call( (@self).FTyped, 0, @LTypeSize );
  end;
end;

function TDataTypeConverter.UInt32ToBytes(const Value: Longword): TUInt8Array;
begin
  case Endian of
  stDefault:      FView.setUInt32(0, Value);
  stLittleEndian: FView.setUInt32(0, Value, true);
  stBigEndian:    FView.setUInt32(0, Value, false);
  end;

  var LTypeSize := __SIZES[dtLong];
  dec(LTypeSize);
  asm
    @result = Array.prototype.slice.call( (@self).FTyped, 0, @LTypeSize );
  end;
end;

function TDataTypeConverter.BytesToUInt32(const Data: TUInt8Array): Longword;
begin
  FView.setUint8(0, data[0]);
  FView.setUint8(1, data[1]);
  FView.setUint8(2, data[2]);
  FView.setUint8(3, data[3]);

  case Endian of
  stDefault:      result := FView.getUint32(0);
  stLittleEndian: result := FView.getUint32(0, true);
  stBigEndian:    result := FView.getUint32(0, false);
  end;
end;

class function TDataTypeConverter.TypedArrayToBytes(const Value: TMemoryHandle): TUInt8Array;
begin
  if (Value) then
  begin
    asm
    @result = Array.prototype.slice.call(@value);
    end;
  end else
  raise EConvertError.Create('Failed to convert, handle is nil or unassigned error');
end;

function TDataTypeConverter.Float64ToBytes(const Value: float64): TUInt8Array;
begin
  case Endian of
  stDefault:      FView.setFloat64(0, Value);
  stLittleEndian: FView.setFloat64(0, Value, true);
  stBigEndian:    FView.setFloat64(0, Value, false);
  end;

  var LTypeSize := __Sizes[dtFloat64];
  dec(LTypeSize);
  asm
    @result = Array.prototype.slice.call( (@self).FTyped, 0, @LTypeSize );
  end;
end;

function TDataTypeConverter.StringToBytes(const Value: String): TUInt8Array;
begin
  if Value.length > 0 then
  begin
    // encode to buffer
    var Codec__ := new JTextEncoder("utf8");
    var rw := Codec__.encode(value);
    Codec__ := nil;

    // slice out bytes
    asm
      @result = Array.prototype.slice.call(@rw, 0, (@rw).byteLength);
    end;

    // release temporary buffer
    rw := nil;
  end else
    result := [];
end;

function TDataTypeConverter.TypedArrayToStr(const Value: TMemoryHandle): string;
begin
  if (Value) then
  begin
    if Value.length > 0 then
    begin
      var Codec__ := new JTextDecoder("utf8");
      result := Codec__.decode(JUint8Array(Value));
      Codec__ := nil;
    end;
  end;
end;

function TDataTypeConverter.StringToTypedArray(const Value: string): TMemoryHandle;
begin
  var Codec__ := new JTextEncoder("utf8");
  result := Codec__.encode(value);
  Codec__ := nil;
end;

function TDataTypeConverter.BytesToString(const Data: TUInt8Array): String;
begin
  if Data.length > 0 then
  begin
    var LTemp := TStdBufferType.Create(Data.length);
    asm
      (@LTemp).set(@Data, 0);
    end;

    var Codec__ := new JTextDecoder("utf8");
    result := Codec__.decode(LTemp);
    Codec__ := nil;
  end;
end;

function TDataTypeConverter.BytesToInt32(const Data: TUInt8Array): Integer;
begin
  FView.setUint8(0, data[0]);
  FView.setUint8(1, data[1]);
  FView.setUint8(2, data[2]);
  FView.setUint8(3, data[3]);

  case Endian of
  stDefault:      result := FView.getInt32(0);
  stLittleEndian: result := FView.getInt32(0, true);
  stBigEndian:    result := FView.getInt32(0, false);
  end;
end;

class function TDataTypeConverter.StrToBase64(Value: String): String;
begin
  asm
    @result = btoa(@Value);
  end;
end;

class function TDataTypeConverter.BytesToChar(const Value: TUInt8Array): Char;
begin
  result := TString.DecodeUTF8(Value);
end;

class function TDataTypeConverter.CharToBytes(const Value: Char): TUInt8Array;
begin
  result := TString.EncodeUTF8(Value[0]);
end;

function TDataTypeConverter.Int16ToTypedArray(const Value: Word): TMemoryHandle;
begin
  case Endian of
  stDefault:      FView.setInt16(0, Value);
  stLittleEndian: FView.setInt16(0, Value, true);
  stBigEndian:    FView.setInt16(0, Value, false);
  end;

  result := FTyped.slice(0, __SIZES[dtInt16]-1);
end;

function TDataTypeConverter.BytesToFloat64(const Data: TUInt8Array): Float;
begin
  FView.setUint8(0, data[0]);
  FView.setUint8(1, data[1]);
  FView.setUint8(2, data[2]);
  FView.setUint8(3, data[3]);
  FView.setUint8(4, data[4]);
  FView.setUint8(5, data[5]);
  FView.setUint8(6, data[6]);
  FView.setUint8(7, data[7]);

  case Endian of
  stDefault:      result := FView.getFloat64(0);
  stLittleEndian: result := FView.getFloat64(0, true);
  stBigEndian:    result := FView.getFloat64(0, false);
  end;
end;

function TDataTypeConverter.BytesToInt16(const Data: TUInt8Array): SmallInt;
begin
  FView.setUint8(0, data[0]);
  FView.setUint8(1, data[1]);

  case Endian of
  stDefault:      result := FView.getInt16(0);
  stLittleEndian: result := FView.getInt16(0, true);
  stBigEndian:    result := FView.getInt16(0, false);
  end;
end;

function TDataTypeConverter.BytesToTypedArray(const Values: TUInt8Array): TMemoryHandle;
begin
  var LLen := Values.length;
  result := TStdBufferType.Create(LLen);
  asm
    (@result).set(@values, 0);
  end;
end;

function TDataTypeConverter.BooleanToTypedArray(const Value: Boolean): TMemoryHandle;
begin
  result := new TStdBufferType(1);
  result[0] := if value then 1 else 0;
end;

class function TDataTypeConverter.BooleanToBytes(const Value: boolean): TUInt8Array;
begin
  result.add(if value then 1 else 0);
end;

function TDataTypeConverter.ByteToTypedArray(const Value: Byte): TMemoryHandle;
begin
  result := new TStdBufferType(1);
  result[0] := if Value < 0 then 0 else if value > 255 then 255 else Value;
end;

class function TDataTypeConverter.CharToByte(const Value: Char): Word;
begin
  asm
    @result = (@value).charCodeAt(0);
  end;
end;

function TDataTypeConverter.Float64ToTypedArray(const Value: float64): TMemoryHandle;
begin
  //write intrinsic value to pre-allocated buffer
  case Endian of
  stDefault:      FView.setFloat64(0, Value);
  stLittleEndian: FView.setFloat64(0, Value, true);
  stBigEndian:    FView.setFloat64(0, Value, false);
  end;
  result := FTyped.slice(0, __SIZES[dtFloat64]);
end;

function TDataTypeConverter.BytesToUInt16(const Data: TUInt8Array): Word;
begin
  FView.setUint8(0, data[0]);
  FView.setUint8(1, data[1]);

  case Endian of
  stDefault:      result := FView.getUint16(0);
  stLittleEndian: result := FView.getUint16(0, true);
  stBigEndian:    result := FView.getUint16(0, false);
  end;
end;

function TDataTypeConverter.Float32ToBytes(const Value: float32): TUInt8Array;
begin
  case Endian of
  stDefault:      FView.setFloat32(0 ,Value);
  stLittleEndian: FView.setFloat32(0 ,Value, true);
  stBigEndian:    FView.setFloat32(0 ,Value, false);
  end;
  asm
    @result = Array.prototype.slice.call( (@self).FTyped, 0, 4 );
  end;
end;

function TDataTypeConverter.BytesToVariant(Data: TUInt8Array): Variant;
begin
  var LType := Data[0];
  Data.delete(0, 1);

  case LType of
  cnt_datatype_boolean: result := BytesToBoolean(Data);
  cnt_datatype_byte:    result := Data[0];
  cnt_datatype_uint16:  result := BytesToUInt16(Data);
  cnt_datatype_uint32:  result := BytesToUInt32(Data);
  cnt_datatype_int16:   result := BytesToInt16(Data);
  cnt_datatype_int32:   result := BytesToInt32(Data);
  cnt_datatype_float32: result := BytesToFloat32(Data);
  cnt_datatype_float64: result := BytesToFloat64(Data);
  cnt_datatype_string:  result := TString.DecodeUTF8(Data);
  else
    begin
      raise EConvertError.CreateFmt(CNT_ERR_CONVERT_BYTESToVariant, [IntToHex(LType, 2)]);
    end;
  end;
end;

function TDataTypeConverter.VariantToBytes(const Value: Variant): TUInt8Array;

 function IsFloat32(const x: variant): boolean;
 begin
  asm
    @result = isFinite(@x) && @x == Math.fround(@x);
  end;
 end;

 function GetSignedIntType: word;
 begin
  if Value > -32768 then
    exit(cnt_datatype_int16);

  if Value > -2147483648 then
    result := cnt_datatype_int32
 end;

 function GetUnSignedIntType: word;
 begin
    if Value <= 255 then exit(cnt_datatype_byte);
    if Value <= 65536 then exit(cnt_datatype_Uint16);
    if value <= 2147483647 then
      result := cnt_datatype_Uint32;
 end;

begin
  case TVariant.ExamineType(Value) of
  vdBoolean:
    begin
      result := [cnt_datatype_boolean];
      result.add( BooleanToBytes( boolean(value) ));
    end;
  vdInteger:
    begin
      var LType: word;
      case (Value < 0) of
      true:   LType := GetSignedIntType();
      false:  LType := GetUnSignedIntType();
      end;

      if LType <> 0 then
      begin
        result := [LType];
        case LType of
        cnt_datatype_byte:    result.add( InitInt08(value) );
        cnt_datatype_Uint16:  result.add( UInt16ToBytes( InitUInt16(value)) );
        cnt_datatype_Uint32:  result.add( UInt32ToBytes( InitUInt32(value)) );
        cnt_datatype_int16:   result.add( Int16ToBytes( InitInt16(value)) );
        cnt_datatype_int32:   result.add( Int32ToBytes( InitInt32(value)) );
        end;
      end else
      raise EConvertError.Create(CNT_ERR_CONVERT_NUMBERID);
    end;
  vdFloat:
    begin
      case IsFloat32(value)of
      true:
        begin
          result := [cnt_datatype_float32];
          result.add( Float32ToBytes(Float(Value)) );
        end;
      false:
        begin
          result := [cnt_datatype_float64];
          result.add( Float64ToBytes(Float(value)) );
        end;
      end;
    end;
  vdString:
    begin
      result := [cnt_datatype_string];
      result.add(TString.EncodeUTF8(value));
    end;
  else
    raise EConvertError.Create(CNT_ERR_CONVERT_DATATYPE);
  end;
end;

function TDataTypeConverter.BytesToFloat32(const Data: TUInt8Array): Float;
begin
  FView.setUint8(0, data[0]);
  FView.setUint8(1, data[1]);
  FView.setUint8(2, data[2]);
  FView.setUint8(3, data[3]);

  case Endian of
  stDefault:      result := FView.getFloat32(0);
  stLittleEndian: result := FView.getFloat32(0, true);
  stBigEndian:    result := FView.getFloat32(0, false);
  end;
end;

class function TDataTypeConverter.Base64ToStr(Value: String): String;
begin
  asm
    @result = atob(@Value);
  end;
end;

class function TDataTypeConverter.Base64ToBytes(const Base64: String): TUInt8Array;
begin
  result := TBase64EncDec.Base64ToBytes(Base64);
end;

function TDataTypeConverter.Int16ToBytes(const Value: int16): TUInt8Array;
begin
  case Endian of
  stDefault:      FView.setInt16(0, Value);
  stLittleEndian: FView.setInt16(0, Value, true);
  stBigEndian:    FView.setInt16(0, Value, false);
  end;
  asm
    @result = Array.prototype.slice.call( (@self).FTyped, 0, 2 );
  end;
end;

function TDataTypeConverter.UInt16ToBytes(const Value: uint16): TUInt8Array;
begin
  case Endian of
  stDefault:      FView.setUint16(0, Value);
  stLittleEndian: FView.setUint16(0, Value, true);
  stBigEndian:    FView.setUint16(0, Value, false);
  end;
  asm
    @result = Array.prototype.slice.call( (@self).FTyped, 0, 2 );
  end;
end;

class function TDataTypeConverter.BytesToBase64(const Bytes: TUInt8Array): String;
begin
  result := TBase64EncDec.BytesToBase64(Bytes);
end;

function TDataTypeConverter.BytesToBoolean(const Data: TUInt8Array): Boolean;
begin
  result := Data[0] > 0;
end;

function TDataTypeConverter.Int32ToTypedArray(const Value: int32): TMemoryHandle;
begin
  case Endian of
  stDefault:      FView.setInt32(0, Value);
  stLittleEndian: FView.setInt32(0, Value, true);
  stBigEndian:    FView.setInt32(0, Value, false);
  end;

  var LTypeSize := __Sizes[dtInt32];
  dec(LTypeSize);
  result := FTyped.slice(0, LTypeSize);
end;

class function TDataTypeConverter.ByteToChar(const Value: Byte): Char;
begin
  asm
    @result = String.fromCharCode(@value);
  end;
end;

//#############################################################################
// EException
//#############################################################################

constructor EException.CreateFmt(Message: string; const Values: array of const);
begin
  inherited Create( Format(Message, Values ));
end;

//############################################################################
// TBase64EncDec
//############################################################################

class function TBase64EncDec.StringToBase64(const Text: string): string;
begin
  asm
    @result = btoa(@text);
  end;
end;

class function TBase64EncDec.Base64ToString(const b64: string): string;
begin
  asm
    @result = atob(@b64);
  end;
end;

class function TBase64EncDec.TripletToBase64(const Num: integer): string;
begin
  result := __B64_Lookup[num shr 18 and $3F]
    + __B64_Lookup[num shr 12 and $3F]
    + __B64_Lookup[num shr 6 and $3F]
    + __B64_Lookup[num and $3F];
end;

class function TBase64EncDec.EncodeChunk(const Data: TUInt8Array; startpos, endpos: integer): string;
begin
  while startpos < endpos do
  begin
    var temp := (Data[startpos] shl 16) + (Data[startpos + 1] shl 8) + Data[startpos + 2];

    result += __B64_Lookup[temp shr 18 and $3F]
      + __B64_Lookup[temp shr 12 and $3F]
      + __B64_Lookup[temp shr 6 and $3F]
      + __B64_Lookup[temp and $3F];

    inc(startpos, 3);
  end;
end;

class function TBase64EncDec.ExtractPlaceholderCount(const b64: string): integer;
begin
  var LLen := b64.length;
  if LLen > 0 then
  begin
    if (LLen mod 4) < 1 then
      result := if b64[LLen-1] = '=' then 2 else if b64[LLen] = '=' then 1 else 0;
  end;
end;

class function TBase64EncDec.CalcByteLength(const b64: string): integer;
begin
  var LLen := b64.Length;
  if LLen > 0 then
  begin
    var LPlaceholderCount := 0;
    if (LLen mod 4) < 1 then
      LPlaceholderCount := if b64[LLen-1] = '=' then 2 else if b64[LLen] = '=' then 1 else 0;
    result := ( (LLen * 3) div 4) - LPlaceholderCount;
  end;
end;

class function TBase64EncDec.Base64ToBytes(const b64: string): TUInt8Array;
begin
  var ASeg, BSeg, CSeg, DSeg: integer;
  var LTextLen := b64.length;
  if LTextLen > 0 then
  begin
    var LPlaceholderCount := 0;
    if (LTextLen mod 4) < 1 then
      LPlaceholderCount := if b64[LTextLen-1] = '=' then 2 else if b64[LTextLen] = '=' then 1 else 0;

    var BufferSize := ((LTextLen * 3) div 4) - LPlaceholderCount;
    result.SetLength(BufferSize);

    if LPlaceholderCount > 0 then
      dec(LTextLen, 4);

    var xpos := 1;
    var idx := 0;
    while xpos < LTextLen do
    begin
      ASeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 0]) ] shl 18;
      BSeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 1]) ] shl 12;
      CSeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 2]) ] shl 6;
      DSeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 3]) ];
      var temp := ASeg or BSeg or CSeg or DSeg;
    	result[idx] := (temp shr 16) and $FF; inc(idx);
      result[idx] := (temp shr 8) and $FF;  inc(idx);
      result[idx] := temp and $FF;          inc(idx);
      inc(xpos, 4);
    end;

    case LPlaceholderCount of
    1:begin
        ASeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 0]) ] shl 2;
        BSeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 1])] shr 4;
        var temp := ASeg or BSeg;
        result[idx] := temp and $FF;
      end;
    2:begin
        ASeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 0])] shl 10;
        BSeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 1])] shl 4;
        CSeg := __B64_RevLookup[ TDataTypeConverter.CharToByte(b64[xpos + 2])] shr 2;
        var temp := ASeg or BSeg or CSeg;
        result[idx] := temp shr 8 and $FF; inc(idx);
        result[idx] := temp and $FF;
      end;
    end;
  end;
end;

class function TBase64EncDec.BytesToBase64(const Data: TUInt8Array): string;
begin
  var LLen := Data.Length;
  if LLen > 0 then
  begin
    var LExtra := Data.Length mod 3;
    var LStrideLen := LLen - LExtra;
    var LMaxChunkLength := 16383;

    var i := 0;
    while i < LStrideLen do
    begin
      var Ahead := i + LMaxChunkLength;
      var SegSize := if (Ahead > LStrideLen) then LStrideLen else Ahead;
      result += EncodeChunk(Data, i, SegSize);
      inc(i, LMaxChunkLength);
    end;

    if LExtra > 0 then
      dec(LLen);

    var output := "";
    case LExtra of
    1:
      begin
        var LTemp := Data[LLen];
        output += __B64_Lookup[LTemp shr 2];
        output += __B64_Lookup[(LTemp shl 4) and $3F];
        output += '==';
      end;
    2:
      begin
        var LTemp := (Data[LLen-1] shl 8) + (Data[LLen]);
        output += __B64_Lookup[LTemp shr 10];
        output += __B64_Lookup[(LTemp shr 4) and $3F];
        output += __B64_Lookup[(LTemp shl 2) and $3F];
        output += "=";
      end;
    end;
    result += output;
  end;
end;


//#############################################################################
// TString
//#############################################################################

class function TString.Split(const Value: string; const Delimiter: string): TStrArray;
Begin
  asm
    @result = (@Value).split(@delimiter);
  end;
end;

class function TString.CharCodeAt(const Text: string; const Index: integer): integer;
begin
  // JS strings start at 0, object pascal at 1
  var LIndex := if index > 0 then index-1 else index;
  asm
    @result = (@Text).charCodeAt(@LIndex);
  end;
end;

class function TString.CharCodeFor(const Character: char): integer;
begin
  asm
    @result = (@Character).charCodeAt(0);
  end;
end;

class function TString.RepeatChar(Count: integer; const CharCode: integer): string;
begin
  if Count > 0 then
  begin
    var LCache: string;
    asm
      @LCache = String.fromCharCode(@CharCode);
    end;
    while Count > 0 do
    begin
      result += LCache;
      dec(Count);
    end;
  end;
end;

class function TString.FromCharCode(const CharCode: byte): char;
begin
  asm
    @result = String.fromCharCode(@CharCode);
  end;
end;

class function TString.Equals(const First, Second: string): boolean;
begin
  result := First.trim().lowercase() = Second.trim().lowercase();
end;

class function TString.LeftPad(Pad: char; Value: string; FitLength: integer): string;
begin
  result := Value;
  Pad := Pad[1];

  if FitLength > result.length then
  begin
    while result.length < FitLength do
    begin
      result := (Pad[1] + result);
    end;
  end;
end;

class function TString.EncodeUTF8(TextToEncode: string): TUInt8Array;
begin
  var LCodec := TQTXCodecUTF8.Create();
  try
    result := LCodec.Encode(TextToEncode);
  finally
    LCodec.free;
  end;
end;

class function TString.DecodeUTF8(const BytesToDecode: TUInt8Array): string;
begin
  var LCodec := TQTXCodecUTF8.Create();
  try
    result := LCodec.Decode(BytesToDecode);
  finally
    LCodec.free;
  end;
end;

class function TString.EncodeURI(const Text: string): string;
begin
  asm
    @result = encodeURI(Text);
  end;
end;

class function TString.DecodeURI(const Text: string): string;
begin
  asm
    @result = decodeURI(@Text);
  end;
end;

class function TString.EncodeURIComponent(const Text: string): string;
begin
  asm
    @result = encodeURIComponent(@Text);
  end;
end;

class function TString.DecodeURIComponent(const Text: string): string;
begin
  asm
    @result = decodeURIComponent(@Text);
  end;
end;

//#############################################################################
// TObjectExtender
//#############################################################################

function TObjectExtender.QualifiedClassName: string;
begin
  result := {$I %FILE%} + '.' + ClassName;
end;

//#############################################################################
// Unit Initialization procedures
//#############################################################################

procedure InitializeBase64;
begin
  for var i := 1 to CNT_B64_CHARSET.length do
  begin
    __B64_Lookup[i-1] := CNT_B64_CHARSET[i];
    __B64_RevLookup[TDataTypeConverter.CharToByte(CNT_B64_CHARSET[i])] := i-1;
  end;

  __B64_RevLookup[TDataTypeConverter.CharToByte('-')] := 62;
  __B64_RevLookup[TDataTypeConverter.CharToByte('_')] := 63;
end;

procedure InitializeTypeMap;
begin
  // Cache the string identifier for common types
  __TYPE_MAP.Boolean   := jsTypeOf(true);
  __TYPE_MAP.Number    := jsTypeOf(0);
  __TYPE_MAP.String    := jsTypeOf("");
  __TYPE_MAP.Object    := jsTypeOf(TVariant.CreateObject);
  __TYPE_MAP.Undefined := jsTypeOf(Undefined);
  __TYPE_MAP.Function  := jsTypeOf( variant(procedure () begin end) );
end;

initialization
begin
  InitializeTypeMap();
  InitializeBase64();
end;

end.
