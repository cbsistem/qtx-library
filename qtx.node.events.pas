unit qtx.node.events;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils;

type

  JModule = class external 'module'
  public
    id:       string;
    filename: string;
    loaded:   boolean;
    parent:   JModule;
    children: array of JModule;
    exports:  Variant;
    function  require(const ModuleName: string): THandle; overload;
  end;

  // various overloads for callback handlers
  TEventEmitterCB   = procedure ();
  TEventEmitterCB1  = procedure (data: Variant);
  TEventEmitterCB2  = procedure (data: variant; arg2: variant);
  TEventEmitterCB3  = procedure (data: variant; arg2, arg3: variant);
  TEventEmitterCB4  = procedure (data: variant; arg2, arg3, arg4: variant);
  TEventEmitterCB5  = procedure (data: variant; arg2, arg3, arg4, arg5: variant);
  TEventEmitterCB6  = procedure (data: variant; arg2, arg3, arg4, arg5, arg6: variant);

  // [event].listeners returns an array of callbacks
  JEventEmitterListeners = array of TEventEmitterCB;

  JNodeEventEmitter = class external 'EventEmitter'
  public
    procedure on(const eventName: string; cb: TEventEmitterCB); overload;
    procedure on(const eventName: string; cb: TEventEmitterCB1); overload;
    procedure on(const eventName: string; cb: TEventEmitterCB2); overload;
    procedure on(const eventName: string; cb: TEventEmitterCB3); overload;
    procedure on(const eventName: string; cb: TEventEmitterCB4); overload;
    procedure on(const eventName: string; cb: TEventEmitterCB5); overload;
    procedure on(const eventName: string; cb: TEventEmitterCB6); overload;

    procedure off(eventName: string; cb: TEventEmitterCB); overload;
    procedure off(eventName: string; cb: TEventEmitterCB1); overload;
    procedure off(eventName: string; cb: TEventEmitterCB2); overload;
    procedure off(eventName: string; cb: TEventEmitterCB3); overload;
    procedure off(eventName: string; cb: TEventEmitterCB4); overload;
    procedure off(eventName: string; cb: TEventEmitterCB5); overload;
    procedure off(eventName: string; cb: TEventEmitterCB6); overload;

    procedure once(const eventName: string; CB: TEventEmitterCB);
    procedure addListener(const eventName: string; CB: TEventEmitterCB); overload;
    procedure addListener(const eventName: string; CB: TEventEmitterCB1); overload;
    procedure addListener(const eventName: string; CB: TEventEmitterCB2); overload;
    procedure addListener(const eventName: string; CB: TEventEmitterCB3); overload;
    procedure addListener(const eventName: string; CB: TEventEmitterCB4); overload;
    procedure addListener(const eventName: string; CB: TEventEmitterCB5); overload;
    procedure addListener(const eventName: string; CB: TEventEmitterCB6); overload;

    procedure removeListener(const eventName: string; CB: TEventEmitterCB); overload;
    procedure removeListener(const eventName: string; CB: TEventEmitterCB1); overload;
    procedure removeListener(const eventName: string; CB: TEventEmitterCB2); overload;
    procedure removeListener(const eventName: string; CB: TEventEmitterCB3); overload;
    procedure removeListener(const eventName: string; CB: TEventEmitterCB4); overload;
    procedure removeListener(const eventName: string; CB: TEventEmitterCB5); overload;
    procedure removeListener(const eventName: string; CB: TEventEmitterCB6); overload;

    procedure removeAllListeners(const eventName: string);

    function  getMaxListeners: integer;
    procedure setMaxListeners(const NewMaxListeners: integer);

    function  listenerCount(const eventName: string): integer;
    function  listeners(const eventName: string): JEventEmitterListeners;

    procedure emit(eventName: string; arg1: variant); overload;
    procedure emit(eventName: string; arg1, arg2: variant); overload;
    procedure emit(eventName: string; arg1, arg2, arg3: variant); overload;
    procedure emit(eventName: string; arg1, arg2, arg3, arg4: variant); overload;
    procedure emit(eventName: string; arg1, arg2, arg3, arg4, arg5: variant); overload;
    procedure emit(eventName: string; arg1, arg2, arg3, arg4, arg5, arg6: variant); overload;

    constructor create;
  end;

  JEventEmitter = class external(JNodeEventEmitter)
  end;

  function  EventsModule: JModule;
  function  CreateEventEmitter: JNodeEventEmitter;

implementation

function RequireModule(modulename: string): THandle; external 'require';

var
  module_events_: THandle;

function CreateEventEmitter: JNodeEventEmitter;
begin
  asm
    @result = new (@module_events_).EventEmitter;
  end;
end;

function EventsModule: JModule;
begin
  result := JModule( module_events_);
end;

initialization
begin
  // Since events are fundamental, we create a reference when the unit
  // is loaded and keep that forever. Saves some cycles when the core need it.
  module_events_ := RequireModule('events');
end;

end.