unit hex.ironwood;

interface

uses
  qtx.sysutils,
  Hex.types,
  Hex.modulators,
  Hex.serialmatrix,
  Hex.serialnumber,
  Hex.partition;

type

  THexSerialGenerateMethod = (
    gmDispersed,
    gmCanonical
  );

  THexMintSerialNumberReadyEvent = procedure(sender: TObject; SerialNumber: string; var Accept: boolean);
  THexMintBeforeEvent = procedure (sender: TObject);
  THexMintAfterEvent = procedure (sender: TObject);

  THexIronwoodGenerator = class(THexIronWoodSerialNumber)
  private
    FOnAcceptSerial:  THexMintSerialNumberReadyEvent;
    FOnBeforeMint:    THexMintBeforeEvent;
    FOnAfterMint:     THexMintAfterEvent;
    FMethod:          THexSerialGenerateMethod;
  protected
    function    Deliver(SerialNumber: string): boolean; virtual;
    procedure   DoDispersed(Count: int32);
    procedure   DoCanonical(Count: int32);
  public
    procedure   Mint(Count: int32); overload; virtual;
    procedure   Mint(Count: int32; const &Method: THexSerialGenerateMethod);overload;
    procedure   Mint(Count: int32; const &Method: THexSerialGenerateMethod; const CB: THexMintSerialNumberReadyEvent);overload;
  published
    property    MintMethod: THexSerialGenerateMethod read FMethod write FMethod;
    property    OnBeforeMinting: THexMintBeforeEvent read FOnBeforeMint write FOnBeforeMint;
    property    OnAfterMinting: THexMintAfterEvent read FOnAfterMint write FOnAfterMint;
    property    OnAcceptSerialNumber: THexMintSerialNumberReadyEvent read FOnAcceptSerial write FOnAcceptSerial;
  end;

implementation

//############################################################################
// THexIronwoodGenerator
//############################################################################

function THexIronwoodGenerator.Deliver(SerialNumber: string): boolean;
begin
  result := false;
  if assigned(OnAcceptSerialNumber) then
  begin
    OnAcceptSerialNumber(self,Serialnumber, result);
  end;
end;

procedure THexIronwoodGenerator.Mint(Count: int32; const &Method: THexSerialGenerateMethod);
begin
  var LOld := FMethod;
  try
    Mint(Count);
  finally
    FMethod := LOld;
  end;
end;

procedure THexIronwoodGenerator.Mint(Count: int32; const &Method: THexSerialGenerateMethod; const CB: THexMintSerialNumberReadyEvent);
var
  LOldHandler: THexMintSerialNumberReadyEvent;
begin
  LOldHandler := @OnAcceptSerialNumber;
  OnAcceptSerialNumber := CB;
  try
    Mint(Count, &Method);
  finally
    OnAcceptSerialNumber := LOldHandler;
  end;
end;

var
  _hex_char_lut := ([
  '00','01','02','03','04','05','06','07','08','09','0a','0b','0c','0d','0e',
  '0f','10','11','12','13','14','15','16','17','18','19','1a','1b','1c','1d',
  '1e','1f','20','21','22','23','24','25','26','27','28','29','2a','2b','2c',
  '2d','2e','2f','30','31','32','33','34','35','36','37','38','39','3a','3b',
  '3c','3d','3e','3f','40','41','42','43','44','45','46','47','48','49','4a',
  '4b','4c','4d','4e','4f','50','51','52','53','54','55','56','57','58','59',
  '5a','5b','5c','5d','5e','5f','60','61','62','63','64','65','66','67','68',
  '69','6a','6b','6c','6d','6e','6f','70','71','72','73','74','75','76','77',
  '78','79','7a','7b','7c','7d','7e','7f','80','81','82','83','84','85','86',
  '87','88','89','8a','8b','8c','8d','8e','8f','90','91','92','93','94','95',
  '96','97','98','99','9a','9b','9c','9d','9e','9f','a0','a1','a2','a3','a4',
  'a5','a6','a7','a8','a9','aa','ab','ac','ad','ae','af','b0','b1','b2','b3',
  'b4','b5','b6','b7','b8','b9','ba','bb','bc','bd','be','bf','c0','c1','c2',
  'c3','c4','c5','c6','c7','c8','c9','ca','cb','cc','cd','ce','cf','d0','d1',
  'd2','d3','d4','d5','d6','d7','d8','d9','da','db','dc','dd','de','df','e0',
  'e1','e2','e3','e4','e5','e6','e7','e8','e9','ea','eb','ec','ed','ee','ef',
  'f0','f1','f2','f3','f4','f5','f6','f7','f8','f9','fa','fb','fc','fd','fe',
  'ff']);

procedure THexIronwoodGenerator.DoDispersed(Count: int32);
begin
  var LAtrophy := 0;
  var LPartition: THexIronwoodPartition;

  while Count > 0 do
  begin
    var LText := '';
    for var x := 0 to Partitions.Count-1 do
    begin
      LPartition := Partitions[x];
      var id := randomInt(LPartition.GateCount);
      LText += _hex_char_lut[ LPartition.Gates[id] ];
      if (x mod 4 = 3) and (x < Partitions.Count-1) then
        LText += '-';
    end;

    if Deliver(LText.ToUpper()) then
    begin
      dec(count);
    end else
    begin
      inc(LAtrophy);
    end;
  end;
end;

// Must use gate-> index
procedure THexIronwoodGenerator.DoCanonical(Count: int32);
begin
  while Count > 0 do
  begin
    var LAtrophy := 0;
    var LText := '';

    for var x := 0 to Partitions.Count-1 do
    begin
      var LPartition := Partitions[x];
      var id := randomInt(LPartition.GateCount);
      LText += _hex_char_lut[ LPartition.Gates[id] ];
      if (x mod 4 = 3) and (x < Partitions.Count-1) then
        LText += '-';
    end;

    if Deliver(LText.ToUpper()) then
      dec(count)
    else
      inc(LAtrophy);
  end;
end;

procedure THexIronwoodGenerator.Mint(Count: int32);
begin
  if assigned(OnBeforeMinting) then
    OnBeforeMinting(self);

  try
    case MintMethod of
    gmDispersed: DoDispersed(Count);
    gmCanonical: DoCanonical(Count);
    end;
  finally
    if assigned(OnAfterMinting) then
    OnAfterMinting(self);
  end;
end;


end.
