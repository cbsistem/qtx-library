unit qtx.classes;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils,
  qtx.memory;

type
  // Root QTX exception
  EQTXException = class(EException);

  // Exception types
  EQTXHandleError   = class(EQTXException);
  EQTXLockError     = class(EQTXException);
  EQTXOwnedObject   = class(EQTXException);

  EQTXReaderError   = class(EQTXException);
  EQTXWriterError   = class(EQTXException);


  // Callback method types
  TStreamReadCB     = procedure (TagValue: variant; const Error: Exception; Data: TUint8Array);
  TStreamWriteCB    = procedure (TagValue: variant; const Error: Exception);
  TStreamGetSizeCB  = procedure (TagValue: variant; const Error: Exception; Size: int64);
  TStreamSetSizeCB  = procedure (TagValue: variant; const Error: Exception; Size: int64);
  TStreamSeekCB     = procedure (TagValue: variant; const Error: Exception; CurrentPos: int64);
  TStreamGetPosCB   = procedure (TagValue: variant; const Error: Exception; CurrentPos: int64);

  // Async stream API
  IAsyncStream = interface
    ['{3FD18B7C-3C5F-445D-9316-2A845CCD1134}']
    procedure ReadA(TagValue: variant; BytesToRead: integer; const CB: TStreamReadCB);
    procedure WriteA(TagValue: variant; Data: TUint8Array; const CB: TStreamWriteCB);
    procedure GetSizeA(TagValue: variant; const CB: TStreamGetSizeCB);
    procedure SetSizeA(TagValue: variant; const NewSize: int64; const CB: TStreamSetSizeCB);
    procedure SeekA(TagValue: variant; NewPosition: int64; const CB: TStreamSeekCB);
    procedure ReadAll(TagValue: variant; const CB: TStreamReadCB);
    procedure GetPositionA(TagValue: variant; const CB: TStreamGetPosCB);
  end;

  // Access interface to handle-based-instance
  IQTXHandleObject = interface
    ['{3FD18B7C-3C5F-445D-1316-4A84599D1134}']
    procedure ObjHandleChanged(const OldHandle, NewHandle: THandle);
    function  AcceptObjHandle(const NewHandle: THandle): boolean;
    procedure SetObjHandle(const NewHandle: THandle);
    function  GetObjHandle: THandle;
  end;

  // Access interface to a lockable instance
  IQTXLockObject = interface
    ['{3FD18B7C-3C5F-445D-9316-2A845CCD1134}']
    procedure DisableAlteration;
    procedure EnableAlteration;
    function  GetLockState: boolean;
  end;

  // Access interface to owned object instance
  IQTXOwnedObjectAccess = interface
    ['{36E39DD5-0B21-4573-95ED-B50145BCF283}']
    function  AcceptOwner(const CandidateObject: TObject): boolean;
    procedure SetOwner(const NewOwner: TObject);
    function  GetOwner: TObject;
  end;


  // Based on Freepascal 3.1, compatible with Delphi
  TSeekOrigin = ( soFromBeginning, soFromCurrent, soFromEnd );

  // Based of Freepascal 3.1 streams
  // Compatible with both Delphi and Freepascal
  TStream = class(TDataTypeConverter, IAsyncStream, IManagedData)
  private
    // Implementation of ASYNC interface. This allows sync-streams to be
    // accessed through normal async methods
    procedure ReadA(TagValue: variant; BytesToRead: integer; const CB: TStreamReadCB);
    procedure WriteA(TagValue: variant; Data: TUint8Array; const CB: TStreamWriteCB);
    procedure GetSizeA(TagValue: variant; const CB: TStreamGetSizeCB);
    procedure SetSizeA(TagValue: variant; const NewSize: int64; const CB: TStreamSetSizeCB);
    procedure SeekA(TagValue: variant; NewPosition: int64; const CB: TStreamSeekCB);
    procedure ReadAll(TagValue: variant; const CB: TStreamReadCB);
    procedure GetPositionA(TagValue: variant; const CB: TStreamGetPosCB);

  protected
    function  GetPosition: int64; virtual; abstract;
    procedure SetPosition(NewPosition: int64); virtual; abstract;
    function  GetSize: int64; virtual; abstract;
    procedure SetSize(NewSize: int64); virtual; abstract;
    function  GetBOF: boolean;
    function  GetEOF: boolean;

  public
    function  Seek(const Offset: int64; Origin: TSeekOrigin): int64; virtual; abstract;
    function  ReadBuffer(Offset: int64; Count: int64): TUint8Array; virtual; abstract;
    procedure WriteBuffer(Offset: int64; Buffer: TUint8Array); virtual; abstract;
    function  CopyFrom(const Source: TStream; Count: int64): integer;
    function  Read(const Count: Int64): TUint8Array; virtual;
    function  Write(const Buffer: TUint8Array): int64; virtual;

    // Implementation of IManagedData
    function  ToBytes: TUInt8Array;
    procedure FromBytes(const Bytes: TUInt8Array);
    procedure Grow(const BytesToGrow: integer);
    procedure Shrink(const BytesToShrink: integer);
    procedure Assign(const Memory: IManagedData);
    procedure Append(const Data: TUInt8Array);

    Property  BOF: Boolean read GetBOF;
    Property  EOF: Boolean read GetEOF;

    property  Position: int64 read GetPosition write SetPosition;
    property  Size: int64 read GetSize write SetSize;
  end;

  TMemoryStream = class(TStream)
  private
    FOffset:    int64;
    FBuffer:    TManagedMemory;
  protected
    function    GetSize: int64; override;
    procedure   SetSize(NewSize: int64); override;
    function    GetPosition: int64; override;
    procedure   SetPosition(NewPosition: int64); override;
    procedure   SetBuffer(const Value: TManagedMemory); virtual;
  public
    function    Seek(const Offset: integer; Origin: TSeekOrigin): integer; override;
    function    ReadBuffer(Offset: int64; Count: int64): TUint8Array; override;
    procedure   WriteBuffer(Offset: int64; Buffer: TUint8Array); override;

    constructor Create; override;
    Destructor  Destroy; override;
  end;

  TAsyncStream = class(TDataTypeConverter, IAsyncStream)
  public
    procedure ReadA(TagValue: variant; BytesToRead: integer; const CB: TStreamReadCB); virtual; abstract;
    procedure WriteA(TagValue: variant; Data: TUint8Array; const CB: TStreamWriteCB); virtual; abstract;
    procedure GetSizeA(TagValue: variant; const CB: TStreamGetSizeCB); virtual; abstract;
    procedure SetSizeA(TagValue: variant; const NewSize: int64; const CB: TStreamSetSizeCB); virtual; abstract;
    procedure SeekA(TagValue: variant; NewPosition: int64; const CB: TStreamSeekCB); virtual; abstract;
    procedure ReadAll(TagValue: variant; const CB: TStreamReadCB);  virtual; abstract;
    procedure GetPositionA(TagValue: variant; const CB: TStreamGetPosCB); virtual; abstract;
  end;

  TAsyncMemoryStream = class(TAsyncStream)
  private
    FBuffer:    TManagedMemory;
    FPos:       int64;
  public
    procedure   ReadA(TagValue: variant; BytesToRead: integer; const CB: TStreamReadCB); override;
    procedure   WriteA(TagValue: variant; Data: TUint8Array; const CB: TStreamWriteCB);  override;
    procedure   GetSizeA(TagValue: variant; const CB: TStreamGetSizeCB);  override;
    procedure   SetSizeA(TagValue: variant; const NewSize: int64; const CB: TStreamSetSizeCB); override;
    procedure   SeekA(TagValue: variant; NewPosition: int64; const CB: TStreamSeekCB);  override;
    procedure   ReadAll(TagValue: variant; const CB: TStreamReadCB); override;
    procedure   GetPositionA(TagValue: variant; const CB: TStreamGetPosCB); override;

    constructor Create; override;
    destructor  Destroy; override;
  end;

  TQTXErrorOptions = set of (
    ooClearOnEntry,   // Clear error on entry in methods
    ooThrowException  // Throw exception under SetLastError()
    );

  TQTXErrorObject = class(TObject)
  private
    FLastError: string;
    FOptions: TQTXErrorOptions;
  protected
    procedure SetErrorOptions(const NewOptions: TQTXErrorOptions);

    procedure SetLastError(const Text: string);
    procedure SetLastErrorF(const Text: string; const Values: array of const);
    procedure ClearLastError;
  public
    property  ErrorOptions: TQTXErrorOptions read FOptions write SetErrorOptions;
    property  LastError: string read FLastError;
  end;

  TQTXReader = class(TDataTypeConverter)
  private
    FAccess:    IManagedData;
    FOffset:    integer;
    FTotalSize: integer;
    FBookmarks: array of integer;
    FCurEmulate: boolean;
  protected
    function    GetReadOffset: integer; virtual;
    function    GetTotalSize: integer; virtual;
    procedure   SetCursorEmulation(const Value: boolean);
  public
    property    CursorEmulation: boolean read FCurEmulate;
    property    Position: integer read GetReadOffset;
    property    Size: integer read GetTotalSize;

    property    BOF: boolean read ((GetTotalSize > 0) and (GetReadOffset < GetTotalSize));
    property    EOF: boolean read ((GetReadOffset >= GetTotalSize));

    function    CheckReadForEOF(const NumberOfBytes: integer): boolean;
    procedure   Bookmark;
    procedure   UnBookmark;
    function    ReadBool: boolean;
    function    ReadChar: char;
    function    ReadUint8: byte;
    function    ReadInt8: Int8;
    function    ReadUint16: Uint16;
    function    ReadInt16: Int16;
    function    ReadUint32: longword;
    function    ReadInt32: integer;
    function    ReadDateTime: TDateTime;
    function    ReadSingle: float;
    function    ReadDouble: double;
    function    Read(Count: integer): TUInt8Array;
    function    ReadStr(Count: Integer): string;
    function    ReadString: string;
    function    ReadVariant: variant;

    constructor Create(const Access: IManagedData); reintroduce; virtual;
  end;

  TQTXWriter = Class(TDataTypeConverter)
  private
    FAccess:    IManagedData;
    FOffset:    integer;
    FTotalSize: integer;
    FCurEmulate: boolean;
    FScaling:   boolean;
  protected
    function    GetOffset: integer;
    function    GetTotalSize: integer;
    function    GetTotalFree: integer;
    function    CheckWriteForEOF(const NumberOfBytes: Integer): boolean;
    procedure   SetCursorEmulation(const Value: boolean);
    procedure   SetScaling(const Value: boolean);
  public
    property    CursorEmulation: boolean read FCurEmulate;
    property    Scaling: boolean read FScaling;

    property    Position: integer read GetOffset;
    property    Size: integer read GetTotalSize;

    property    BOF: boolean read ((GetTotalSize > 0) and (GetOffset < GetTotalSize));
    property    EOF: boolean read ((GetOffset >= GetTotalSize));

    procedure   WriteBool(const Value: boolean);
    procedure   WriteChar(const Value: char);
    procedure   WriteUInt8(const Value: UInt8);
    procedure   WriteInt8(const Value: Int8);
    procedure   WriteUInt16(const Value: Uint16);
    procedure   WriteInt16(const Value: Int16);
    procedure   WriteUint32(const Value: UInt32);
    procedure   WriteInt32(const Value: Int32);
    procedure   WriteSingle(const Value: float);
    procedure   WriteDouble(const Value: Double);

    procedure   WriteDateTime(const Value: TDateTime);
    function    Write(const Data: TUInt8Array):Integer;

    Procedure   WriteStr(const Value: string);
    procedure   WriteString(const Value: string);
    procedure   WriteVariant(const Value: variant);

    constructor Create(const Access: IManagedData); reintroduce; virtual;
  end;

  TQTXStreamReader = class(TQTXReader)
  public
    constructor Create(const Stream: TStream); overload; virtual;
  end;

  TQTXStreamWriter = class(TQTXWriter)
  public
    constructor Create(const Stream: TStream); overload; virtual;
  end;

  TQTXHandleObject = class(TObject, IQTXHandleObject)
  private
    FHandle:  THandle;
  protected
    procedure ObjHandleChanged(const OldHandle, NewHandle: THandle); virtual;
    function  AcceptObjHandle(const NewHandle: THandle): boolean;
    procedure SetObjHandle(const NewHandle: THandle); virtual;
    function  GetObjHandle: THandle; virtual;
  public
    property  Handle: THandle read GetObjHandle;
  end;

  (* TW3OwnedObject implements a basic parent-child relationship *)
  TQTXOwnedObject = partial class(TObject, IQTXOwnedObjectAccess)
  private
    FOwner:     TObject;
  protected
    function    GetOwner: TObject; virtual;
    procedure   SetOwner(const NewOwner: TObject); virtual;
    function    AcceptOwner(const CandidateObject: TObject): boolean; virtual;
  public
    property    Owner: TObject read GetOwner;
    constructor Create(const AOwner: TObject); virtual;
  end;

  TQTXLockedObject = class(TObject, IQTXLockObject)
  private
    FLocked:    integer;
  protected
    function    GetLockState: boolean;
    procedure   DisableAlteration;
    procedure   EnableAlteration;

    procedure   ObjectLocked; virtual;
    procedure   ObjectUnLocked; virtual;

    property    OnObjectLocked: TNotifyEvent;
    property    OnObjectUnLocked: TNotifyEvent;
    property    Locked: boolean read GetLockState;
  end;

  TQTXOwnedLockedObject = class(TQTXOwnedObject, IQTXLockObject)
  private
    FLocked:    integer;
  protected
    function    GetLockState: boolean;
    procedure   DisableAlteration;
    procedure   EnableAlteration;

    procedure   ObjectLocked; virtual;
    procedure   ObjectUnLocked; virtual;

    property    OnObjectLocked: TNotifyEvent;
    property    OnObjectUnLocked: TNotifyEvent;
    property    Locked: boolean read GetLockState;
  end;

implementation

const
  CNT_STRING_SIGNATURE  = $BAAACEEE;

resourcestring
  // Errors for stream
  CNT_STREAM_ERROR_SEEK_EMPTY = "Seek failed, stream is empty error";

  // Errors for reader
  CNT_READER_ERROR_EXCEEDS_BOUNDS = "Read operation failed, %s bytes exceeds storage medium error";
  CNT_READER_ERROR_INVALID_SIGNATURE = "Read operation failed, invalid signature error [%s]";
  CNT_READER_ERROR_BOOKMARKS_NOTSUPPORTED = "Bookmarks not supported by medium error";
  CNT_READER_ERROR_BOOKMARKS_EMPTY = "No bookmarks to roll back error";

  // Errors for writer
  CNT_WRITER_ERROR_EXCEEDS_BOUNDS = "Invalid length, %s bytes exceeds storage boundaries error";
  CNT_WRITER_ERROR_INVALID_DATASIZE = "Write failed, invalid datasize [%d] error";

  // Errors for TQTXHandleObject
  CNT_ERR_HandleObject_Rejected = "'Invalid handle [%s], reference was rejected error";


//#############################################################################
// TQTXOwnedObject
//#############################################################################

constructor TQTXOwnedObject.Create(const AOwner: TObject);
begin
  inherited Create;
  SetOwner(AOwner)
end;

function TQTXOwnedObject.GetOwner: TObject;
begin
  result := FOwner;
end;

function TQTXOwnedObject.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := true;
end;

procedure TQTXOwnedObject.SetOwner(const NewOwner: TObject);
begin
  if (NewOwner <> FOwner) then
  begin
    if AcceptOwner(NewOwner) then
      FOwner := NewOwner
    else
      raise EQTXOwnedObject.CreateFmt('Owner was rejected in %s.%s error', [ClassName, {$I %FUNCTION%}]);
  end;
end;

//#############################################################################
// TQTXLockedObject
//#############################################################################

procedure TQTXLockedObject.DisableAlteration;
begin
  inc(FLocked);
  if FLocked = 1 then
    ObjectLocked();
end;

procedure TQTXLockedObject.EnableAlteration;
begin
  if FLocked > 0 then
  begin
    dec(FLocked);
    if FLocked = 0 then
      ObjectUnLocked();
  end;
end;

function TQTXLockedObject.GetLockState: boolean;
begin
  result := FLocked > 0;
end;

procedure TQTXLockedObject.ObjectLocked;
begin
  if assigned(OnObjectLocked) then
    OnObjectLocked(self);
end;

procedure TQTXLockedObject.ObjectUnLocked;
begin
  if assigned(OnObjectUnLocked) then
    OnObjectUnLocked(self);
end;

//#############################################################################
// TW3OwnedLockedObject
//#############################################################################

procedure TQTXOwnedLockedObject.DisableAlteration;
begin
  inc(FLocked);
  if FLocked = 1 then
    ObjectLocked();
end;

procedure TQTXOwnedLockedObject.EnableAlteration;
begin
  if FLocked > 0 then
  begin
    dec(FLocked);
    if FLocked = 0 then
      ObjectUnLocked();
  end;
end;

function TQTXOwnedLockedObject.GetLockState: boolean;
begin
  result := FLocked > 0;
end;

procedure TQTXOwnedLockedObject.ObjectLocked;
begin
  if assigned(OnObjectLocked) then
    OnObjectLocked(self);
end;

procedure TQTXOwnedLockedObject.ObjectUnLocked;
begin
  if assigned(OnObjectUnLocked) then
    OnObjectUnLocked(self);
end;

//############################################################################
// TQTXHandleObject
//############################################################################

function TQTXHandleObject.AcceptObjHandle(const NewHandle: THandle): boolean;
begin
  result := (NewHandle);
  //result := true;
end;

function TQTXHandleObject.GetObjHandle: THandle;
begin
  result := FHandle;
end;

procedure TQTXHandleObject.SetObjHandle(const NewHandle: THandle);
begin
  if not AcceptObjHandle(NewHandle) then
    raise EQTXHandleError.CreateFmt(CNT_ERR_HandleObject_Rejected, [{$I %FUNCTION%}]);

  var LTemp := FHandle;
  FHandle := NewHandle;
  ObjHandleChanged(LTemp, FHandle);
end;

procedure TQTXHandleObject.ObjHandleChanged(const OldHandle, NewHandle: THandle);
begin
  // This method is called whenever the handle changes
end;

//#############################################################################
// TQTXStreamReader
//#############################################################################

constructor TQTXStreamReader.Create(const Stream: TStream);
begin
  inherited Create( Stream as IManagedData );
end;

//#############################################################################
// TQTXStreamWriter
//#############################################################################

constructor TQTXStreamWriter.Create(const Stream: TStream);
begin
  inherited Create( Stream as IManagedData );
end;

//#############################################################################
// TQTXWriter
//#############################################################################

constructor TQTXWriter.Create(const Access: IManagedData);
begin
  inherited Create;
  FAccess := Access;
  FOffset := FAccess.GetPosition();
  FTotalSize := FAccess.GetSize();
  FScaling := true;
  FCurEmulate := true;
end;

procedure TQTXWriter.SetCursorEmulation(const Value: boolean);
begin
  FCurEmulate := Value;
end;

procedure TQTXWriter.SetScaling(const Value: boolean);
begin
  FScaling := Value;
end;

function TQTXWriter.GetTotalSize: integer;
begin
  (* Do we emulate the cursor ? *)
  case FCurEmulate of
  true:   result := MAXINT;
  false:  result := FAccess.GetSize();
  end;
end;

function TQTXWriter.GetOffset:integer;
begin
  case FCurEmulate of
  true:   result := FOffset;
  false:  result := FAccess.GetPosition();
  end;
end;

function TQTXWriter.CheckWriteForEOF(const NumberOfBytes: integer): boolean;
begin
  (* Make sure its a valid number *)
  if (NumberOfBytes >= 1) then
  begin
    (* Does target support flexible boundaries? *)
    case FScaling of
    true:   result := false;
    false:  result := GetTotalFree() < NumberOfBytes;
    end;
  end;
end;

function TQTXWriter.GetTotalFree: integer;
begin
  result := FAccess.GetSize() - GetOffset();
end;

function TQTXWriter.Write(const Data: TUInt8Array): Integer;
begin
  var LBytesToWrite := Data.length;

  (* Anything to actually write ? *)
  if LBytesToWrite > 0 then
  begin

    (* Check if we need to care about growth and boundaries *)
    if FScaling  then
    begin
      (* Write, we dont need to care about breatch *)
      FAccess.WriteBuffer(GetOffset(), Data);

      (* Update emulated cursor if any *)
      if FCurEmulate then
        inc(FOffset, Data.length);
    end else
    begin
      (* Do we need to clip the data ? *)
      if CheckWriteForEOF(LBytesToWrite) then
      begin
        (* Figure out clip size *)
        var LBytesLeft := GetTotalSize - GetOffset;
        var LBytesMissing := abs( LBytesLeft - LBytesToWrite);

        (* Clip the data *)
        dec(LBytesToWrite, LBytesMissing);
        Data.SetLength(LBytesToWrite);
      end;

      if LBytesToWrite>1 then
      begin
        (* Write the data, clipped or not *)
        FAccess.WriteBuffer(GetOffset(), Data);

        (* Update emulated cursor if any *)
        if FCurEmulate then
          inc(FOffset, Data.length);
      end else
      raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[Data.length]);
    end;

    result := Data.length;

  end else
  raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_INVALID_DATASIZE,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteBool(const Value: boolean);
begin
  var LBytesToWrite := SizeOfType(dtBoolean);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(BooleanToBytes(Value))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteChar(const Value: Char);
begin
  var LBytesToWrite := SizeOfType(dtChar);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write( TString.EncodeUTF8(Value[1]) )
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteUInt8(const Value: Uint8);
begin
  var LValue := TInteger.EnsureRange(Value,0,255);
  var LBytesToWrite := 1;
  if not CheckWriteForEOF(LBytesToWrite) then
    Write([LValue])
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteInt8(const Value: Int8);
begin
  var LValue := TInteger.EnsureRange(Value,0,255);
  var LBytesToWrite := 1;
  if not CheckWriteForEOF(LBytesToWrite) then
    Write([LValue])
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteUInt16(const Value: word);
begin
  var LValue := TInteger.EnsureRange(Value, 0, 65536);
  var LBytesToWrite := SizeOfType(dtInt16);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(Int16ToBytes(LValue))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteInt16(const Value: word);
begin
  var LValue := TInteger.EnsureRange(Value, -32768, 32767);
  var LBytesToWrite := SizeOfType(dtInt16);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(Int16ToBytes(LValue))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteDateTime(const Value: TDateTime);
begin
  var LBytesToWrite := SizeOfType(dtFloat64);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(Float64ToBytes(Value))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteDouble(const Value: double);
begin
  var LBytesToWrite := SizeOfType(dtFloat64);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(Float64ToBytes(Value))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteSingle(const Value: float);
begin
  var LBytesToWrite := SizeOfType(dtFloat32);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(Float32ToBytes(Value))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteUint32(const Value: Uint32);
begin
  var LBytesToWrite := SizeOfType(dtInt32);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write( Int32ToBytes(Value) )
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

procedure TQTXWriter.WriteInt32(const Value: Int32);
begin
  var LBytesToWrite := SizeOfType(dtInt32);
  if not CheckWriteForEOF(LBytesToWrite) then
    Write(Int32ToBytes(Value))
  else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
end;

Procedure TQTXWriter.WriteStr(const Value: string);
begin
  var LBytes := StringToBytes(Value);
  var LBytesToWrite := LBytes.length;

  if LBytesToWrite > 0 then
  begin
    if not CheckWriteForEOF(LBytesToWrite) then
    begin
      try
        Write(LBytes);
      except
        on e: exception do
        raise EQTXWriterError.Create(e.message);
      end;
    end else
    raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LBytesToWrite]);
  end;
end;

procedure TQTXWriter.WriteString(const Value: string);
begin
  var LTotal := SizeOfType(dtInt32);  // Signature
  inc(LTotal, SizeOfType(dtInt32) );  // Length
  var LBytes := StringToBytes(Value);
  inc(LTotal, LBytes.length);

  if not CheckWriteForEOF(LTotal) then
  begin
    try
      WriteUInt32(CNT_STRING_SIGNATURE);  // Signature
      WriteUInt32(LBytes.Length);         // Length
      if (LBytes.Length > 0) then          // Bytes
        Write(LBytes);
    except
      on e: exception do
      raise EQTXWriterError.Create(e.message);
    end;
  end else
  raise EQTXWriterError.CreateFmt(CNT_WRITER_ERROR_EXCEEDS_BOUNDS,[LTotal]);
end;

procedure TQTXWriter.WriteVariant(const Value: variant);
begin
  var LDataType := Value.datatype;
  if LDataType = vdUnknown then
    WriteUInt32(Ord(vdUnknown))
  else
  begin
    var LBytes := VariantToBytes(Value);
    WriteUInt32(LDataType);
    WriteUInt32(LBytes.Length);
    Write(LBytes);
  end;
end;

//#############################################################################
// TQTXReader
//#############################################################################

constructor TQTXReader.Create(const Access: IManagedData);
begin
  inherited Create;
  FAccess := Access;
  FCurEmulate := true;
  FOffset := FAccess.GetPosition();
  FTotalSize := FAccess.GetSize();
end;

procedure TQTXReader.SetCursorEmulation(const Value: boolean);
begin
  FCurEmulate := value;
end;

procedure TQTXReader.Bookmark;
begin
  if FCurEmulate then
  begin
    FBookmarks.Push( GetReadOffset() );
  end else
    raise EQTXReaderError.Create(CNT_READER_ERROR_BOOKMARKS_NOTSUPPORTED)
end;

procedure TQTXReader.UnBookmark;
begin
  if FCurEmulate then
  begin
    if FBookmarks.Count > 0 then
      FOffset := FBookmarks.Pop()
    else
      raise EQTXReaderError.Create(CNT_READER_ERROR_BOOKMARKS_EMPTY);
  end else
    raise EQTXReaderError.Create(CNT_READER_ERROR_BOOKMARKS_NOTSUPPORTED);
end;

function TQTXReader.GetTotalSize: integer;
begin
  if FCurEmulate then
    result := FTotalSize
  else
    result := FAccess.GetSize();
end;

function TQTXReader.GetReadOffset: integer;
begin
  if FCurEmulate then
    result := FOffset
  else
    result := FAccess.GetPosition();
end;

function TQTXReader.CheckReadForEOF(const NumberOfBytes: Integer): boolean;
begin
  result := ( GetTotalSize() - GetReadOffset() ) < NumberOfBytes;
end;

function TQTXReader.Read(Count: integer): TUInt8Array;
begin
  if Count > 0 then
  begin
    result := FAccess.ReadBuffer(GetReadOffset(), Count);
    if FCurEmulate then
      inc(FOffset, result.length);
  end else
    raise EQTXReaderError.CreateFmt("Invalid read length (%s)",[Count]);
end;

function TQTXReader.ReadBool: boolean;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtBoolean);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToBoolean( Read(LTypeSize) )
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadChar: Char;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtChar);
  if not CheckReadForEOF(LTypeSize) then
    result := TString.DecodeUTF8( Read(LTypeSize) )
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadUint8: Byte;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtByte);
  if not CheckReadForEOF(LTypeSize) then
    result := Read(LTypeSize)[0]
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadInt8: Int8;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtByte);
  if not CheckReadForEOF(LTypeSize) then
  begin
    var LTemp: uint8 := Read(LTypeSize)[0];
    result := if (LTemp < -128) then -128 else if LTemp > 127 then 127 else LTemp;
  end else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadUint16: Uint16;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtInt16);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToUInt16(Read(LTypeSize))
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadInt16: Int16;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtInt16);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToInt16(Read(LTypeSize))
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadUint32: Uint32;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtInt32);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToUInt32(Read(LTypeSize))
  else
  raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadInt32: Int32;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtInt32);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToInt32(Read(LTypeSize))
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadDateTime: TDateTime;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtFloat64);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToFloat64(Read(LTypeSize))
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadDouble: Double;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtFloat64);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToFloat64(Read(LTypeSize))
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadSingle: Float;
begin
  var LTypeSize := SizeOfType(TJSVMDataType.dtFloat32);
  if not CheckReadForEOF(LTypeSize) then
    result := BytesToFloat32( Read(LTypeSize) )
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS,[LTypeSize]);
end;

function TQTXReader.ReadStr(Count: integer): string;
begin
  if Count > 0 then
  begin
    if not CheckReadForEOF(Count) then
      result := BytesToString( Read(Count) )
    else
      raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_EXCEEDS_BOUNDS, [Count]);
  end;
end;

function TQTXReader.ReadString: string;
begin
  if ReadInt32() = CNT_STRING_SIGNATURE then
    result := ReadStr( ReadInt32() )
  else
    raise EQTXReaderError.CreateFmt(CNT_READER_ERROR_INVALID_SIGNATURE, ["string"]);
end;

function TQTXReader.ReadVariant: variant;
begin
  var LVariantType := TVariantExportType( ReadInt32() );
  if LVariantType <> vdUnKnown then
  begin
    var StringLen_ := ReadInt32();
    if StringLen_ > 0 then
      result := BytesToVariant( Read(StringLen_) );
  end;
end;

//############################################################################
// TAsyncMemoryStream
//############################################################################

procedure TQTXErrorObject.SetErrorOptions(const NewOptions: TQTXErrorOptions);
begin
  FOptions := NewOptions;
end;

procedure TQTXErrorObject.SetLastError(const Text: string);
begin
  FLastError := Text;
  if (ooThrowException in FOptions) then
    raise EQTXException.Create(FLastError);
end;

procedure TQTXErrorObject.SetLastErrorF(const Text: string; const Values: array of const);
begin
  FLastError := format(Text, Values);
  if (ooThrowException in FOptions) then
    raise EQTXException.Create(FLastError);
end;

procedure TQTXErrorObject.ClearLastError;
begin
  FLastError := '';
end;

//############################################################################
// TAsyncMemoryStream
//############################################################################

constructor TAsyncMemoryStream.Create;
begin
  inherited Create;
  FBuffer := TManagedMemory.Create;
  FPos := -1;
end;

destructor TAsyncMemoryStream.Destroy;
begin
  FBuffer.free;
  inherited;
end;

procedure TAsyncMemoryStream.GetPositionA(TagValue: variant; const CB: TStreamGetPosCB);
begin
  if assigned(CB) then
    CB(TagValue, nil, FPos);
end;

procedure TAsyncMemoryStream.GetSizeA(TagValue: variant; const CB: TStreamGetSizeCB);
begin
  if assigned(CB) then
    CB(TagValue, nil, FBuffer.Size);
end;

procedure TAsyncMemoryStream.SetSizeA(TagValue: variant; const NewSize: int64; const CB: TStreamSetSizeCB);
begin
  if NewSize < 1 then
  begin
    FBuffer.Release();
    FPos := -1;
    if assigned(CB) then
      CB(TagValue, nil, 0);
    exit;
  end;

  try
    FBuffer.ScaleTo(NewSize);

    if FPos > FBuffer.Size then
      FPos := FBuffer.Size;

  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, e, FBuffer.Size)
      else
        raise;
      exit;
    end;
  end;

  if assigned(CB) then
    CB(TagValue, nil, FBuffer.Size);
end;

procedure TAsyncMemoryStream.SeekA(TagValue: variant; NewPosition: int64; const CB: TStreamSeekCB);
begin
  if FBuffer.Size > 0 then
  begin
    var LLastByte := FBuffer.Size;
    NewPosition := if NewPosition < 0 then 0 else if NewPosition > LLastByte then LLastByte else NewPosition;
    FPos := NewPosition;
    if assigned(CB) then
      CB(TagValue, nil, FPos);
  end else
  begin
    var LError := EException.Create(CNT_STREAM_ERROR_SEEK_EMPTY);
    if assigned(CB) then
      CB(TagValue, LError, FPos);
    LError.free;
  end;
end;

procedure TAsyncMemoryStream.WriteA(TagValue: variant; Data: TUInt8Array; const CB: TStreamWriteCB);
begin
  if Data.length > 0 then
  begin
    if Fpos < 0 then
      Fpos := 0;

    try
      FBuffer.WriteBuffer(FPos, Data);
      inc(FPos, Data.length);
    except
      on e: exception do
      begin
        if assigned(CB) then
          CB(TagValue, e)
        else
          raise;
        exit;
      end;
    end;

    if assigned(CB) then
      CB(TagValue, nil);
  end else
  begin
    if assigned(CB) then
      CB(TagValue, nil);
  end;
end;

procedure TAsyncMemoryStream.ReadA(TagValue: variant; BytesToRead: Integer; const CB: TStreamReadCB);
begin
  if FBuffer.Size > 0 then
  begin
    if Fpos < 0 then
      Fpos := 0;

    var LData: TUint8Array;

    try
      LData := FBuffer.ReadBuffer(Fpos, BytesToRead);
    except
      on e: exception do
      begin
        if assigned(CB) then
          CB(TagValue, e, LData)
        else
          raise;
        exit;
      end;
    end;

    inc(FPos, LData.length);

    if assigned(CB) then
      CB(TagValue, nil, LData);
  end else
  begin
    if BytesToRead > 0 then
    begin
      var LError := EException.Create(CNT_STREAM_ERROR_SEEK_EMPTY);
      if assigned(CB) then
        CB(TagValue, LError, []);
    end else
    begin
      if assigned(CB) then
        CB(TagValue, nil, []);
    end;
  end;
end;

procedure TAsyncMemoryStream.ReadAll(TagValue: variant; const CB: TStreamReadCB);
begin
  if FBuffer.Size > 0 then
  begin
    FPos := FBuffer.Size;
    if assigned(CB) then
      CB(TagValue, nil, FBuffer.ToBytes());
  end else
  begin
    FPos := -1;
    if assigned(CB) then
      CB(TagValue, nil, []);
  end;
end;

//############################################################################
// TMemoryStream
//############################################################################

constructor TMemoryStream.Create;
begin
  inherited Create;
  FBuffer := TManagedMemory.Create();
  FOffset := -1;
end;

Destructor TMemoryStream.Destroy;
begin
  FBuffer.free;
  inherited;
end;

function TMemoryStream.GetPosition: int64;
begin
  result := FOffset;
end;

procedure TMemoryStream.SetSize(NewSize: int64);
begin
  var LSize := GetSize();
  if NewSize > 0 then
  Begin
    if NewSize = LSize then
      exit;

    if NewSize > LSize then
    begin
      var LDiff := NewSize - LSize;
      if FBuffer.Size + LDiff > 0 then
        FBuffer.Grow(LDiff)
      else
        FBuffer.Release();
    end else
    begin
      var LDiff := LSize - NewSize;
      if FBuffer.Size - LDiff > 0 then
        FBuffer.Shrink(LDiff)
      else
        FBuffer.Release();
    end;
  end else
    Fbuffer.Release();

  LSize := GetSize();
  FOffset := if FOffset < 0 then 0 else if FOffset > LSize then LSize else FOffset;
end;

procedure TMemoryStream.SetBuffer(const Value: TManagedMemory);
begin
  if Value <> nil then
  begin
    FBuffer.Assign(Value as IManagedData);
    FOffset := if FBuffer.Size > 0 then 0 else -1;
  end else
  begin
    FBuffer.Release();
    FOffset := -1;
  end;
end;

procedure TMemoryStream.SetPosition(NewPosition: int64);
begin
  var LSize := GetSize();
  if LSize > 0 then
    FOffset := if (NewPosition < 0) then 0 else if (NewPosition > LSize) then LSize else NewPosition;
end;

function TMemoryStream.Seek(const Offset: int64; Origin: TSeekOrigin): int64;
begin
  var LSize := GetSize();
  if LSize < 1 then
    exit(0);

  case Origin of
  soFromBeginning:
    begin
      if Offset > -1 then
      begin
        var LNewPos := Offset;
        LNewPos := if (LNewPos < 0) then 0 else if LNewPos > LSize then LSize else LNewPos;
        SetPosition(LNewPos);
        result := LNewPos;
      end;
    end;
  soFromCurrent:
    begin
      var LNewPos := GetPosition();
      inc(LNewPos, Offset);
      result := if (LNewPos < 0) then 0 else if LNewPos > LSize then LSize else LNewPos;
      SetPosition(result);
    end;
  soFromEnd:
    begin
      var LNewPos := LSize;
      dec(LSize, ABS(offset) );
      result := if (LNewPos < 0) then 0 else if LNewPos > LSize then LSize else LNewPos;
      SetPosition(result);
    end;
  end;
end;

function TMemoryStream.GetSize: int64;
begin
  result := FBuffer.Size;
end;

function TMemoryStream.ReadBuffer(Offset, Count: int64): TUint8Array;
begin
  var LSize := GetSize();
  var LBytesLeft := if Position < LSize then (LSize - Position) else 0;
  if LBytesLeft > 0 then
  begin
    var LBytesToRead := if (Count > LBytesLeft) then LBytesLeft else Count;
    result := FBuffer.ReadBuffer(Offset, LBytesToRead);
    SetPosition(Offset + result.length);
  end;
end;

procedure TMemoryStream.WriteBuffer(Offset: int64; Buffer: TUint8Array);
begin
  if FBuffer.&Empty and (Offset < 1) then
  begin
    FBuffer.Append(Buffer);
    SetPosition(Buffer.Length);
  end else
  begin
    if EOF then
    begin
      FBuffer.Grow(Buffer.Length);
      FBuffer.WriteBuffer(Offset, Buffer);
    end else
    begin
      var LSize := FBuffer.Size;
      var LNewEnd := Offset + Buffer.length;
      if LNewEnd > LSize then
        FBuffer.grow(LNewEnd-LSize);
      FBuffer.WriteBuffer(Offset, Buffer);
    end;

    if Offset < 0 then
      Offset := 0;
    SetPosition(Offset + Buffer.length);
  end;
end;

//############################################################################
// TStream
//############################################################################

function  TStream.ToBytes: TUInt8Array;
begin
  result := ReadBuffer(0, GetSize() );
end;

procedure TStream.FromBytes(const Bytes: TUInt8Array);
begin
  if  GetSize() > 0 then
  begin
    SetSize(0);
    if Bytes.length > 0 then
      Append(Bytes);
  end;
end;

procedure TStream.Grow(const BytesToGrow: integer);
begin
  if BytesToGrow > 0 then
    SetSize( GetSize() + BytesToGrow);
end;

procedure TStream.Shrink(const BytesToShrink: integer);
begin
  if BytesToShrink > 0 then
  begin
    var LSize := GetSize();
    if LSize > 0 then
    begin
      dec(LSize, BytesToShrink);
      if BytesToShrink > 0 then
        SetSize(LSize)
      else
        SetSize(0);
    end;
  end;
end;

procedure TStream.Assign(const Memory: IManagedData);
begin
  if GetSize() > 0 then
    SetSize(0);
  if assigned(Memory) then
  begin
    var LSize := Memory.GetSize();
    if LSize > 0 then
    begin
      var LCache := Memory.ReadBuffer(0, LSize);
      WriteBuffer(0, LCache);
      LCache.Clear();
    end;
  end;
end;

procedure TStream.Append(const Data: TUInt8Array);
begin
  if Data.length > 0 then
    WriteBuffer(GetSize(), Data)
end;

function TStream.GetBOF: boolean;
begin
  result := GetPosition() <= 0;
end;

function TStream.GetEOF: boolean;
begin
  result := GetPosition() >= GetSize();
end;

function TStream.CopyFrom(const Source: TStream; Count: int64): int64;
begin
  result := Write( Source.Read(Count) );
end;

function TStream.Read(const Count: int64): TUint8Array;
begin
  result := ReadBuffer( GetPosition(), Count);
end;

function TStream.Write(const Buffer: TUint8Array): int64;
begin
  WriteBuffer(GetPosition(), Buffer);
  result := buffer.length;
end;

procedure TStream.GetPositionA(TagValue: variant; const CB: TStreamGetPosCB);
begin
  if assigned(CB) then
    CB(TagValue, nil, GetPosition() );
end;

procedure TStream.ReadA(TagValue: variant; BytesToRead: integer; const CB: TStreamReadCB);
begin
  var LBytes: TUint8Array;

  try
    LBytes := Read(BytesToRead);
  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, e, LBytes)
      else
        raise;
      exit;
    end;
  end;

  if assigned(CB) then
    CB(TagValue, nil, LBytes);
end;

procedure TStream.WriteA(TagValue: variant; Data: TUint8Array; const CB: TStreamWriteCB);
begin
  try
    Write(Data);
  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, e)
      else
        raise;
      exit;
    end;
  end;

  if assigned(CB) then
    CB(TagValue, nil);
end;

procedure TStream.GetSizeA(TagValue: variant; const CB: TStreamGetSizeCB);
begin
  if assigned(CB) then
    CB(TagValue, nil, GetSize() );
end;

procedure TStream.SetSizeA(TagValue: variant; const NewSize: int64; const CB: TStreamSetSizeCB);
begin
  try
    SetSize(NewSize);
  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, e, GetSize())
      else
        raise;
      exit;
    end;
  end;

  if assigned(CB) then
    CB(TagValue, nil, GetSize() );
end;

procedure TStream.SeekA(TagValue: variant; NewPosition: int64; const CB: TStreamSeekCB);
begin
  try
    SetPosition(NewPosition);
  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, e, GetPosition())
      else
        raise;
      exit;
    end;
  end;

  if assigned(CB) then
    CB(TagValue, nil, GetPosition() );
end;

procedure TStream.ReadAll(TagValue: variant; const CB: TStreamReadCB);
begin
  var LBytes: TUint8Array;

  try
    SetPosition(0);
    LBytes := Read(GetSize());
  except
    on e: exception do
    begin
      if assigned(CB) then
        CB(TagValue, e, LBytes)
      else
        raise;
      exit;
    end;
  end;

  if assigned(CB) then
    CB(TagValue, nil, LBytes);
end;

end.
