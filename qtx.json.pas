unit qtx.json;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  ECMA.Date,
  W3C.TypedArray,

  system.dateutils,
  qtx.sysutils,
  qtx.memory,
  qtx.classes;

type
  // Exception types
  EQTXJSONObject = class(EException);

  // Callbacks and events
  TQTXJSONEnumerator = function (const KeyName: string; var Data: variant): TEnumState;

  TQTXKeyValueProcessor = function (Key: String; Value: Variant): Variant;

  TQTXPropertyDataType =
    ( qdtInvalid,
      qdtBoolean,
      qdtinteger,
      qdtfloat,
      qdtstring,
      qdtSymbol,
      qdtFunction,
      qdtObject,
      qdtArray,
      qdtVariant );

  TJSON = class external 'JSON'
  public
    function Parse(Text: string): variant; overload; external 'parse';
    function Parse(Text: string; Reviver: TQTXKeyValueProcessor): variant; overload; external 'parse';
    function Stringify(const Value: variant): string; overload; external 'stringify';
    function Stringify(const Value: variant; Replacer: TQTXKeyValueProcessor): string; overload; external 'stringify';
    function Stringify(const Value: variant; Replacer: TQTXKeyValueProcessor; Space: integer): string; overload; external 'stringify';
    function Stringify(const Value: variant; Replacer: TQTXKeyValueProcessor; Space: string): string; overload; external 'stringify';
  end;

  JTextEncoder = class external "TextEncoder"
    property encoding: string;
    function encode(const text: string): JUint8Array;
    constructor Create(TextFormat: string); overload; //utf-8 | utf-16 | iso-8859-2 | koi8 | cp1261 | gbk | windows-1251
    constructor Create; overload;
  end;

  JTextDecoder = class external "TextDecoder"
    property  encoding: string;
    property  fatal: boolean;
    property  ignoreBOM: boolean;
    function  decode(const Data: JUint8Array): string; overload;
    constructor Create(TextFormat: string); overload; //utf-8 | utf-16 | iso-8859-2 | koi8 | cp1261 | gbk | windows-1251
    constructor Create; overload;
  end;

  TQTXJSONDataTypeResolver = static class
    class function  QueryObject(const Reference: THandle): boolean;
    class function  QueryArray(const Reference: THandle): boolean;
    class function  Queryinteger(const Reference: THandle): boolean;
    class function  Queryfloat(const Reference: THandle): boolean;
    class function  QueryBoolean(const Reference: THandle): boolean;
    class function  Querystring(const Reference: THandle): boolean;
    class function  QuerySymbol(const Reference: THandle): boolean;
    class function  QueryFunction(const Reference: THandle): boolean;
    class function  QueryUInt8Array(const Reference: THandle): boolean;
    class function  QueryDataType(const Reference: THandle): TQTXPropertyDataType;
  end;

  TQTXJSONObject = partial class(TObject)
  private
    FInstance:  THandle;
    function    GetPropertyCount: integer;
    function    GetProperty(const Index: integer): variant;
    function    GetPropertyByName(const PropertyName: string): variant;
  public
    property    Instance: THandle read FInstance;
    property    &Object: JObject read ( JObject(FInstance) );

    property    Count: integer read GetPropertyCount;
    property    Items[const &Index: integer]: variant read GetProperty;
    property    Values[const PropertyName: string]: variant read GetPropertyByName;

    function    Exists(const PropertyName: string): boolean;
    function    Examine(const PropertyName: string): TQTXPropertyDataType;

    function    Read(const PropertyName: string): variant;
    function    ReadString(const PropertyName: string): string;
    function    ReadInteger(const PropertyName: string): integer;
    function    ReadBoolean(const PropertyName: string): boolean;
    function    ReadDateTime(const PropertyName: string): TDateTime;
    function    ReadFloat(const PropertyName: string): float;

    procedure   Write(const PropertyName: string; const Data: variant);
    procedure   WriteOrAdd(const PropertyName: string; const Data: variant);
    procedure   WriteString(const PropertyName: string; const Data: string);
    procedure   WriteInteger(const PropertyName: string; const Data: integer);
    procedure   WriteBoolean(const PropertyName: string; const Data: boolean);
    procedure   WriteDateTime(const PropertyName: string; const Data: TDateTime);
    procedure   WriteFloat(const PropertyName: string; const Data: float);

    function    Branch(const ObjectName: string): TQTXJSONObject;
    function    Locate(const PropertyName: string; const AllowCreate: boolean): TQTXJSONObject;

    procedure   Delete(const PropertyName: string);

    function    GetInstanceKeys: TStrArray;

    function    ForEach(const CB: TQTXJSONEnumerator): TQTXJSONObject;

    function    ToJSON(const Indent: integer): string; overload;
    function    ToJSON(const Indent: integer; const Replacer: TQTXKeyValueProcessor): string; overload;
    procedure   FromJSON(const Text: string);

    procedure   SaveToStream(const Stream: TStream);
    procedure   LoadFromStream(const Stream: TStream);

    procedure   SaveToBuffer(const Buffer: TManagedMemory);
    procedure   LoadFromBuffer(const Buffer: TManagedMemory);

    procedure   Clear;

    constructor Create; overload; virtual;
    constructor Create(const Instance: THandle); overload;
    destructor  Destroy; override;
  end;


var
  JSON external 'JSON': TJSON;

implementation

resourcestring
  CNT_ERR_FailedWriteFind = 'Failed to write value, property [%s] not found error';
  CNT_ERR_FailedReadFind = 'Failed to read value, property [%s] not found error';
  CNT_ERR_FailedLocate = 'Failed to locate object, property [%s] not found error';

//############################################################################
// TQTXJSONObject
//###########################################################################

class function TQTXJSONDataTypeResolver.QueryDataType(const Reference: THandle): TQTXPropertyDataType;
var
  LType: string;
begin
  if (Reference) then
  begin
    LType := jsTypeOf(Reference);

    case LType.ToLower of
    "object":
      begin
        if not (Reference.length) then
          result := TQTXPropertyDataType.qdtObject
        else
          result := TQTXPropertyDataType.qdtArray;
      end;
    "function": result := TQTXPropertyDataType.qdtFunction;
    "symbol":   result := TQTXPropertyDataType.qdtSymbol;
    "boolean":  result := TQTXPropertyDataType.qdtBoolean;
    "string":   result := TQTXPropertyDataType.qdtstring;
    "number":
      begin
        if round(Reference) <> Reference then
          result := TQTXPropertyDataType.qdtFloat
        else
          result := TQTXPropertyDataType.qdtInteger;
      end;
    'array':  result := TQTXPropertyDataType.qdtArray;
    else
      result := TQTXPropertyDataType.qdtVariant;
    end;
  end else
  result := TQTXPropertyDataType.qdtInvalid;
end;

class function TQTXJSONDataTypeResolver.QueryUint8Array(const Reference: THandle): boolean;
begin
  if (Reference) then
  begin
    var LTypeName := '';
    asm
    @LTypeName = Object.prototype.toString.call(@Reference);
    end;
    result := LTypeName = "[object Uint8Array]"
  end;
end;

class function TQTXJSONDataTypeResolver.QueryObject(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference  === "object")
      && ((@Reference).length === undefined);
  end;
end;

class function TQTXJSONDataTypeResolver.QuerySymbol(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference === "symbol");
  end;
end;

class function TQTXJSONDataTypeResolver.QueryFunction(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference === "function");
  end;
end;

class function TQTXJSONDataTypeResolver.QueryBoolean(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference === "boolean");
  end;
end;

class function TQTXJSONDataTypeResolver.QueryString(const Reference: THandle): boolean;
Begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference  === "string");
  end;
end;

class function TQTXJSONDataTypeResolver.QueryFloat(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference  === "number")
      && (Number(@Reference) === @Reference && @Reference % 1 !== 0);
  end;
end;

class function TQTXJSONDataTypeResolver.QueryInteger(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (typeof @Reference  === "number")
      && (Number(@Reference) === @Reference && @Reference % 1 === 0);
  end;
end;

class function TQTXJSONDataTypeResolver.QueryArray(const Reference: THandle): boolean;
begin
  asm
    @result = (@Reference !== undefined)
      && (@Reference !== null)
      && (Array.isArray(@Reference) === true);
  end;
end;


//############################################################################
// TQTXJSONObject
//###########################################################################

constructor TQTXJSONObject.Create;
begin
  inherited Create;
  FInstance := TVariant.CreateObject();
end;

constructor TQTXJSONObject.Create(const Instance: THandle);
begin
  inherited Create;

  if (Instance) then
  begin
    if Instance.IsObject then
      FInstance := Instance
    else
      raise EQTXJSONObject.Create('Failed to attach to JSON instance, reference is not an object');
  end else
  raise EQTXJSONObject.Create('Failed to attach to JSON instance, reference was NIL error');
end;

destructor TQTXJSONObject.Destroy;
begin
  FInstance := nil;
  inherited destroy;
end;

procedure TQTXJSONObject.Delete(const PropertyName: string);
begin
  if FInstance.hasOwnProperty(PropertyName) then
  begin
    var LRef := FInstance;
    asm
      delete @LRef[@PropertyName];
    end;
  end;
end;

function TQTXJSONObject.Examine(const PropertyName: string): TQTXPropertyDataType;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := TQTXJSONDataTypeResolver.QueryDataType(FInstance[PropertyName])
  else
    raise EQTXJSONObject.CreateFmt
    ('Failed to examine datatype, property [%s] not found error',[PropertyName]);
end;

function TQTXJSONObject.GetPropertyByName(const PropertyName: string): variant;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := FInstance[PropertyName]
  else
    raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.GetProperty(const Index: integer): variant;
begin
 var LRef := FInstance;
 asm
  @result = Object.keys(@LRef)[@index];
 end;
end;

function TQTXJSONObject.GetPropertyCount: integer;
begin
 var LRef := FInstance;
 asm
  @result = Object.keys(@LRef).length;
 end;
end;

function TQTXJSONObject.ForEach(const CB: TQTXJSONEnumerator): TQTXJSONObject;
begin
  result := self;
  if assigned(CB) then
  begin
    for var el in GetInstanceKeys() do
    begin
      var LData := Read(el);
      if CB(el, LData) = esContinue then
        Write(el, LData)
      else
        break;
    end;
  end;
end;

function TQTXJSONObject.ToJSON(const Indent: integer): string;
begin
  result := JSON.stringify(FInstance, nil, Indent)
end;

function TQTXJSONObject.ToJSON(const Indent: integer; const Replacer: TQTXKeyValueProcessor): string;
begin
  result := JSON.stringify(FInstance, @Replacer, Indent)
end;

procedure TQTXJSONObject.FromJSON(const Text: string);
begin
  FInstance := JSon.Parse(Text);
end;

procedure TQTXJSONObject.SaveToBuffer(const Buffer: TManagedMemory);
begin
  var LText := JSON.stringify(FInstance) + "   ";
  var LBytes := TString.EncodeUTF8(LText);
  Buffer.Append(LBytes);
end;

procedure TQTXJSONObject.LoadFromBuffer(const Buffer: TManagedMemory);
begin
  var LBytes := Buffer.ToBytes();
  var LText := TString.DecodeUTF8(LBytes);
  FInstance := JSON.Parse(LText);
end;

procedure TQTXJSONObject.SaveToStream(const Stream: TStream);
begin
  var LText := JSON.stringify(FInstance);
  var LEncoder := new JTextEncoder();
  var LTyped := LEncoder.encode(LText);
  var LBytes := Stream.TypedArrayToBytes(LTyped);
  Stream.Write(LBytes);
end;

procedure TQTXJSONObject.LoadFromStream(const Stream: TStream);
begin
  var LBytes := Stream.Read(Stream.Size);
  var LDecoder := new JTextDecoder();
  var LText := LDecoder.decode( JUint8Array(Stream.BytesToTypedArray(LBytes)) );
  FInstance := JSON.Parse(LText);
end;

function TQTXJSONObject.GetInstanceKeys: TStrArray;
begin
  var LRef := FInstance;
  asm
    if (!(Object.keys === undefined)) {
      @result = Object.keys(@LRef);
      return @result;
    }

    if (!(Object.getOwnPropertyNames === undefined)) {
        @result = Object.getOwnPropertyNames(@LRef);
        return @result;
    }

    for (var qtxenum in @LRef) {
      if ( (@LRef).hasOwnProperty(qtxenum) == true )
        (@result).push(qtxenum);
    }
    return @result;
  end;
end;

procedure TQTXJSONObject.Clear;
begin
  // Just create a new object, let the GC clean up
  FInstance := TVariant.CreateObject();
end;

function TQTXJSONObject.Read(const PropertyName: string): variant;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := FInstance[PropertyName]
  else
    Raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.ReadString(const PropertyName: string): string;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := TVariant.AsString(FInstance[PropertyName])
  else
    raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.ReadInteger(const PropertyName: string): integer;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := TVariant.AsInteger(FInstance[PropertyName])
  else
    raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.ReadBoolean(const PropertyName: string): boolean;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := TVariant.AsBool(FInstance[PropertyName])
  else
    raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.ReadFloat(const PropertyName: string): float;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := TVariant.AsFloat(FInstance[PropertyName])
  else
    raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.ReadDateTime(const PropertyName: string): TDateTime;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := JDateToDateTime(JDate( FInstance[PropertyName]) )
  else
    raise EQTXJSONObject.CreateFmt
    (CNT_ERR_FailedReadFind,[PropertyName]);
end;

function TQTXJSONObject.Locate(const PropertyName: string; const AllowCreate: boolean): TQTXJSONObject;
begin
  if FInstance.hasOwnProperty(PropertyName) then
    result := TQTXJSONObject.Create( FInstance[PropertyName] )
  else
  begin
    if AllowCreate then
    begin
      FInstance[PropertyName] := TVariant.CreateObject();
      result := TQTXJSONObject.Create( FInstance[PropertyName] );
    end else
    raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedLocate,[PropertyName]);
  end;
end;

procedure TQTXJSONObject.WriteString(const PropertyName: string; const Data: string);
begin
  if FInstance.hasOwnProperty(PropertyName) then
    FInstance[PropertyName] := Data
  else
  raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedWriteFind,[PropertyName]);
end;

procedure TQTXJSONObject.WriteInteger(const PropertyName: string; const Data: integer);
begin
  if FInstance.hasOwnProperty(PropertyName) then
    FInstance[PropertyName] := Data.ToString()
  else
  raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedWriteFind,[PropertyName]);
end;

procedure TQTXJSONObject.WriteBoolean(const PropertyName: string; const Data: boolean);
begin
  if FInstance.hasOwnProperty(PropertyName) then
    FInstance[PropertyName] := Data
  else
  raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedWriteFind,[PropertyName]);
end;

procedure TQTXJSONObject.WriteDateTime(const PropertyName: string; const Data: TDateTime);
begin
  if FInstance.hasOwnProperty(PropertyName) then
    FInstance[PropertyName] := DateTimeToJDate(Data)
  else
    raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedWriteFind,[PropertyName]);
end;

procedure TQTXJSONObject.WriteFloat(const PropertyName: string; const Data: float);
begin
  if FInstance.hasOwnProperty(PropertyName) then
    FInstance[PropertyName] := FloatToStr(Data)
  else
  raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedWriteFind,[PropertyName]);
end;

function TQTXJSONObject.Branch(const ObjectName: string): TQTXJSONObject;
begin
  FInstance[ObjectName] := TVariant.CreateObject();
  result := TQTXJSONObject.Create( FInstance[ObjectName] );
end;

procedure TQTXJSONObject.Write(const PropertyName: string; const Data: variant);
begin
  if FInstance.hasOwnProperty(PropertyName) then
    FInstance[PropertyName] := Data
  else
  raise EQTXJSONObject.CreateFmt(CNT_ERR_FailedWriteFind,[PropertyName]);
end;

procedure TQTXJSONObject.WriteOrAdd(const PropertyName: string; const Data: variant);
begin
  FInstance[PropertyName] := Data
end;

function TQTXJSONObject.Exists(const PropertyName: string): boolean;
begin
  result := FInstance.hasOwnProperty(PropertyName);
end;


end.

