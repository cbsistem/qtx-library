unit qtx.fileapi.core;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils,
  qtx.classes,
  qtx.memory;

type

  EQTXStorage = class(EException);

  // forward declarations
  TQTXStorageDevice = class;

  // Standard unix file-permissions
  TQTXFilePermissionMask =
    (
    fpNone              = 0000,
    fpExecute           = 0111,
    fpWrite             = 0222,
    fpWriteExecute      = 0333,
    fpRead              = 0444,
    fpReadExecute       = 0555,
    fpDefault           = 0666,
    fpReadWriteExecute  = 0777,
    fpRWEGroupReadOnly  = 0740
    );

  /* Requirements for using a device */
  TQTXStorageDeviceOptions = set of (
      doRequireLogin, // Require authentication
      doReadOnly      // Device is read-only
    );

  TQTXStorageObjectType = (
    otUnknown,
    otFile,             // normal file
    otFolder,           // normal folder
    otBlockDevice,      // block-device [raw floppy-disk, tapedrive etc]
    otCharacterDevice,  // serial device
    otSymbolicLink,     // reference to system-object or link
    otFIFO,             // stream device
    otSocket            // network socket
  );

  TNJFileItemType = (
    wtFile = 0,
    wtFolder = 1
    );

  TNJFileItem = class(JObject)
    diFileName: string;
    diFileType: TNJFileItemType;
    diFileSize: int64;
    diFileMode: string;
    diCreated:  TDateTime;
    diModified: TDateTime;
  end;
  TNJFileItems = array of TNJFileItem;

  TNJFileItemList = class(JObject)
    dlPath: string;
    dlItems: TNJFileItems;
  end;

  TQTXDeviceAuthenticationData = class
  public
    // Covers basic-authentication
    property adUsername: string;
    property adPassword: string;

    // Covers certificate based access
    property adApplicationKey: string;
    property adApplicationSecret: string;

    // Covers REST / Ragnarok style authentication
    property adOAuthAccessToken: string;  // Identifier [session-id, login-token etc]
    property adEndpoint: string;          // full endpoint url ["wss://host:port/"]
  end;

  TQTXFileOperationExamineCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Path: string;
    DirList: TNJFileItemList;
    Error: EException);

  TQTXDeviceOptionsCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Options: TQTXStorageDeviceOptions;
    Error: EException);

  TQTXDeviceNameCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    DeviceName: string;
    Error: EException);

  TQTXDeviceIdCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    DeviceId: string;
    Error: EException);

  TQTXDeviceGetPathCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Path: string;
    Error: EException);

  TQTXDeviceGetFileExistsCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    FileName: string;
    Error: EException);

  TQTXDeviceMakeDirCB = procedure (
    Sender: TQTXStorageDevice;
    FileName: string;
    Error: EException);

  TQTXDeviceRemoveDirCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    FileName: string;
    Error: EException);

  TQTXDeviceChDirCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Path: string;
    Error: EException);

  TQTXDeviceCdUpCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Error: EException);

  TQTXDeviceLoadCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Filename: string;
    Data: IManagedData;
    Error: EException);

  TQTXDeviceSaveCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Filename: string;
    Error: EException);

  TQTXDeviceGetFileSizeCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    Filename: string;
    FileSize: int64;
    Error: EException);

  TQTXDeviceObjTypeCB = procedure (
    Sender: TQTXStorageDevice;
    TagValue: variant;
    ObjPath: string;
    ObjType: TQTXStorageObjectType;
    Error: EException);

  // Filesystem access rights
  TQTXStorageDeviceAccess  = set of
    (
      daNone,       // none
      daReadOnly,   // read files only
      daReadWrite,  // read and write [create]
      daExecute     // can execute
    );

  TQTXStorageDeviceMountEvent = procedure (Sender: TQTXStorageDevice);
  TQTXStorageDeviceUnMountEvent = procedure (Sender: TQTXStorageDevice);

  IQTXStorageDevice = interface
    function  GetActive: boolean;
    procedure GetName(TagValue: variant; CB: TQTXDeviceNameCB);
    procedure GetDeviceOptions(TagValue: variant; CB: TQTXDeviceOptionsCB);
    procedure GetDeviceId(TagValue: variant; CB: TQTXDeviceIdCB);
    procedure GetPath(TagValue: variant; CB: TQTXDeviceGetPathCB);
    procedure GetFileSize(TagValue: variant; Filename: string; CB: TQTXDeviceGetFileSizeCB);
    procedure GetStorageObjectType(TagValue: variant; ObjName: string; CB: TQTXDeviceObjTypeCB);

    procedure FileExists(TagValue: variant; Filename: string; CB: TQTXDeviceGetFileExistsCB);
    procedure DirExists(TagValue: variant; FolderName: string; CB: TQTXDeviceGetFileExistsCB);
    procedure MakeDir(TagValue: variant; FolderName: string; Mode: TQTXFilePermissionMask; CB: TQTXDeviceMakeDirCB);
    procedure RemoveDir(TagValue: variant; FolderName: string; CB: TQTXDeviceRemoveDirCB);
    procedure Examine(TagValue: variant; FolderPath: string; CB: TQTXFileOperationExamineCB);
    procedure ChDir(TagValue: variant; FolderPath: string; CB: TQTXDeviceChDirCB);
    procedure CdUp(TagValue: variant; CB: TQTXDeviceCdUpCB);
    procedure Load(TagValue: variant; Filename: string; CB: TQTXDeviceLoadCB);
    procedure Save(TagValue: variant; Filename: string; Data: IManagedData; CB: TQTXDeviceSaveCB);
  end;

  TQTXStorageDevice = class(TQTXErrorObject, IQTXStorageDevice)
  private
    FActive:      boolean;
    FIdentifier:  string;
    FName:        string;
    FOptions:     TQTXStorageDeviceOptions;
  protected
    procedure SetActive(const NewActiveState: boolean); virtual;
    function  GetActive: boolean; virtual;

    procedure SetName(TagValue: variant; NewValue: string; CB: TQTXDeviceNameCB); virtual;
    procedure SetDeviceOptions(TagValue: variant; NewOptions: TQTXStorageDeviceOptions; CB: TQTXDeviceOptionsCB); virtual;
    procedure SetDeviceId(TagValue: variant; NewDeviceId: string; CB: TQTXDeviceIdCB); virtual;

  public
    property  Active: boolean read GetActive;

    procedure GetName(TagValue: variant; CB: TQTXDeviceNameCB); virtual;
    procedure GetDeviceOptions(TagValue: variant; CB: TQTXDeviceOptionsCB); virtual;
    procedure GetDeviceId(TagValue: variant; CB: TQTXDeviceIdCB); virtual;

    procedure GetPath(TagValue: variant; CB: TQTXDeviceGetPathCB); virtual; abstract;
    procedure GetFileSize(TagValue: variant; Filename: string; CB: TQTXDeviceGetFileSizeCB); virtual; abstract;

    procedure FileExists(TagValue: variant; Filename: string; CB: TQTXDeviceGetFileExistsCB); virtual; abstract;
    procedure DirExists(TagValue: variant; FolderName: string; CB: TQTXDeviceGetFileExistsCB); virtual; abstract;
    procedure MakeDir(TagValue: variant; FolderName: string; Mode: TQTXFilePermissionMask; CB: TQTXDeviceMakeDirCB); virtual; abstract;
    procedure RemoveDir(TagValue: variant; FolderName: string; CB: TQTXDeviceRemoveDirCB); virtual; abstract;
    procedure Examine(TagValue: variant; FolderPath: string; CB: TQTXFileOperationExamineCB); overload; virtual; abstract;
    procedure Examine(TagValue: variant; CB: TQTXFileOperationExamineCB); overload; virtual;
    procedure ChDir(TagValue: variant; FolderPath: string; CB: TQTXDeviceChDirCB); virtual; abstract;
    procedure CdUp(TagValue: variant; CB: TQTXDeviceCdUpCB); virtual; abstract;

    procedure GetStorageObjectType(TagValue: variant; ObjName: string; CB: TQTXDeviceObjTypeCB); virtual; abstract;

    procedure Load(TagValue: variant; Filename: string; CB: TQTXDeviceLoadCB); virtual; abstract;
    procedure Save(TagValue: variant; Filename: string; Data: IManagedData; CB: TQTXDeviceSaveCB); virtual; abstract;

    procedure Mount(TagValue: variant; Authentication: TQTXDeviceAuthenticationData; CB: TQTXStorageDeviceMountEvent); virtual; abstract;
    procedure UnMount(TagValue: variant; CB: TQTXStorageDeviceUnMountEvent); virtual; abstract;

    constructor Create; virtual;
    destructor  Destroy; override;

  published
    property  OnMounted: TQTXStorageDeviceMountEvent;
    property  OnUnMounted: TQTXStorageDeviceMountEvent;
  end;

  TQTXStorageDeviceClass   = class of TQTXStorageDevice;

implementation



resourcestring
  CNT_ERR_FILESYSTEM_OPERATION =
  "File operation [%s] failed, system threw exception: %s";



//#############################################################################
// TQTXStorageDevice
//#############################################################################

constructor TQTXStorageDevice.Create;
begin
  inherited Create;
  FOptions := [doReadOnly];
  FIdentifier := TString.CreateGUID();
  FName := 'dh0';
end;

destructor  TQTXStorageDevice.Destroy;
begin
  if FActive then
    UnMount(nil, nil);
  inherited;
end;

procedure TQTXStorageDevice.GetDeviceOptions(TagValue: variant; CB: TQTXDeviceOptionsCB);
begin
  if assigned(CB) then
    CB(self, TagValue, FOptions, nil);
end;

procedure TQTXStorageDevice.SetDeviceOptions(TagValue: variant;
          NewOptions: TQTXStorageDeviceOptions; CB: TQTXDeviceOptionsCB);
begin
  FOptions := NewOptions;
  if assigned(CB) then
    CB(self, TagValue, FOptions, nil);
end;

procedure TQTXStorageDevice.SetName(TagValue: variant; NewValue: string; CB: TQTXDeviceNameCB);
begin
  NewValue := NewValue.trim();
  if NewValue <> FName then
  begin
    if NewValue.length > 0 then
    begin
      // Assign new name
      FName := NewValue;
      if assigned(CB) then
        CB(self, TagValue, NewValue, nil);
    end else
    begin
      // Invalid name, provide or throw exception
      var LError := EQTXStorage.CreateFmt('%s failed, invalid name (empty)', [{$I %FUNCTION%}]);
      if assigned(CB) then
        CB(self, TagValue, NewValue, LError)
      else
        raise LError;
    end;
  end else
  begin
    // Same name is not an error, so we just return positive
    if assigned(CB) then
      CB(self, TagValue, NewValue, nil);
  end;
end;

procedure TQTXStorageDevice.GetName(TagValue: variant; CB: TQTXDeviceNameCB);
begin
  if assigned(CB) then
    CB(self, TagValue, FName, nil);
end;

procedure TQTXStorageDevice.GetDeviceId(TagValue: variant; CB: TQTXDeviceIdCB);
begin
  if assigned(CB) then
    CB(self, TagValue, FIdentifier, nil);
end;

procedure TQTXStorageDevice.Examine(TagValue: variant; CB: TQTXFileOperationExamineCB);
begin
  if GetActive() then
  begin
    Examine('', @CB);
  end else
  begin
    // Create error object
    var LError := EQTXStorage.CreateFmt('%s failed, device not active error', [{$I %FUNCTION%}]);

    // Device not active, provide or throw exception
    if assigned(CB) then
    begin
      var LFiles: TNJFileItemList;
      LFiles.dlPath := '';
      CB(self, TagValue, '', LFiles, LError);
    end else
    raise LError;
  end;
end;

procedure TQTXStorageDevice.SetDeviceId(TagValue: variant; NewDeviceId: string; CB: TQTXDeviceIdCB);
begin
  NewDeviceId := NewDeviceId.trim();
  if NewDeviceId <> FIdentifier then
  begin
    if NewDeviceId.length > 0 then
    begin
      // set new identifier
      FIdentifier := NewDeviceId;
      if assigned(CB) then
        CB(self, TagValue, NewDeviceId, nil);
    end else
    begin
      // Invalid length, provide or throw exception
      var LError := EQTXStorage.CreateFmt('%s failed, invalid identifier (empty)', [{$I %FUNCTION%}]);
      if assigned(CB) then
        CB(self, TagValue, NewDeviceId, LError)
      else
        raise LError;
    end;
  end else
  begin
    // same identifier is not an error
    if assigned(CB) then
      CB(self, TagValue, NewDeviceId, nil);
  end;
end;

function TQTXStorageDevice.GetActive: boolean;
begin
  result := FActive;
end;

procedure TQTXStorageDevice.SetActive(const NewActiveState: boolean);
begin
  FActive := NewActiveState;
end;

end.
