unit qtx.network.service;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils, qtx.classes, qtx.logfile, qtx.network.bindings;

type
  // Exception types
  EQTXNetworkError = class(EException);

  // Custom event declarations
  TQTXNetworkServiceErrorEvent = procedure (Sender: TObject; Error: JError);

  // Base-class for all network IO classes [server, client, binding]
  TQTXNetworkService = class(TQTXLogHandleObject)
  private
    FActive:    boolean;
    FHost:      string;
    FPort:      integer;
  protected
    function    GetActive: boolean; virtual;
    procedure   SetActive(const NewValue: boolean); virtual;
    procedure   SetActiveDirect(const NewValue: boolean);

    function    GetAddress: string; virtual;
    procedure   SetAddress(const NewHost: string); virtual;

    function    GetPort: integer; virtual;
    procedure   SetPort(const NewPort: integer); virtual;
  protected
    procedure   BeforeStart; virtual;
    procedure   AfterStart; virtual;
    procedure   BeforeStop; virtual;
    procedure   AfterStop; virtual;

    // Must be implemented
    procedure   InitializeService; virtual; abstract;
    procedure   FinalizeService; virtual; abstract;

    // Depending on client or server, these must be exposed by
    // ancestors
    property    Address: string read GetAddress write SetAddress;
    property    Port: integer read GetPort write SetPort;
  public
    property    Active: boolean read GetActive write SetActive;
    destructor  Destroy; override;
  published
    property    OnBeforeStarted: TNotifyEvent;
    property    OnAfterStarted: TNotifyEvent;
    property    OnBeforeStopped: TNotifyEvent;
    property    OnAfterStopped: TNotifyEvent;
    property    OnError: TQTXNetworkServiceErrorEvent;
  end;

  TQTXBoundNetworkService = class(TQTXNetworkService)
  private
    FBindings:  TQTXHostBindings;
    function    GetBindings: IQTXHostBindings;
  protected
    procedure   InitializeService; override;
    procedure   FinalizeService; override;
  public
    property    Bindings: IQTXHostBindings read GetBindings;
    constructor Create; override;
    destructor  Destroy; override;
  end;

  TQTXNetworkServer = class(TQTXBoundNetworkService)
  public
    property  Port;
  end;

  TQTXNetworkClient = class(TQTXNetworkService)
  public
    property  Address;
    property  Port;
  end;

implementation

//############################################################################
// TQTXBoundNetworkService
//############################################################################

constructor TQTXBoundNetworkService.Create;
begin
  inherited;
  FBindings := TQTXHostBindings.Create();
end;

destructor TQTXBoundNetworkService.Destroy;
begin
  FBindings.free;
  inherited;
end;

function TQTXBoundNetworkService.GetBindings: IQTXHostBindings;
begin
  result := FBindings as IQTXHostBindings;
end;

procedure TQTXBoundNetworkService.InitializeService;
begin
  // Disable changes to the bindings-list
  var LAccess := FBindings as IQTXLockObject;
  LAccess.DisableAlteration();
end;

procedure TQTXBoundNetworkService.FinalizeService;
begin
  // Enable changes to the bindings list
  var LAccess := FBindings as IQTXLockObject;
  LAccess.EnableAlteration();
end;

//############################################################################
// TQTXNetworkService
//############################################################################

destructor TQTXNetworkService.Destroy;
begin
  if GetActive then
  begin
    try
      try
        FinalizeService;
      except
        on e: exception do;
      end;
    finally
      SetObjHandle(unassigned);
    end;
  end;
  inherited;
end;

procedure TQTXNetworkService.BeforeStart;
begin
  if assigned(OnBeforeStarted) then
    OnBeforeStarted(self);
end;

procedure TQTXNetworkService.AfterStart;
begin
  if assigned(OnAfterStarted) then
    OnAfterStarted(self);
end;

procedure TQTXNetworkService.BeforeStop;
begin
  if assigned(OnBeforeStopped) then
    OnBeforeStopped(self);
end;

procedure TQTXNetworkService.AfterStop;
begin
  if assigned(OnAfterStopped) then
    OnAfterStopped(self);
end;

function TQTXNetworkService.GetAddress: string;
begin
  result := FHost;
end;

procedure TQTXNetworkService.SetAddress(const NewHost: string);
begin
  if not GetActive() then
    FHost := NewHost.trim()
  else
    raise EQTXNetworkError.Create('Address cannot be altered while object is active error');
end;

function TQTXNetworkService.GetPort: integer;
begin
  result := FPort;
end;

procedure TQTXNetworkService.SetPort(const NewPort: Integer);
begin
  if not GetActive then
    FPort := NewPort
  else
    raise EQTXNetworkError.Create('Port cannot be altered while object is active error');
end;

function TQTXNetworkService.GetActive: boolean;
begin
  result := FActive;
end;

procedure TQTXNetworkService.SetActiveDirect(const NewValue: boolean);
begin
  FActive := NewValue;
end;

procedure TQTXNetworkService.SetActive(const NewValue: boolean);
begin
  if (NewValue <> GetActive) then
  begin
    case GetActive of
    false:
      begin
        BeforeStart;
        InitializeService;
        //AfterStart;
      end;
    true:
      begin
        BeforeStop;
        FinalizeService;
        //AfterStop;
      end;
    end;

  end;
end;

end.


end.