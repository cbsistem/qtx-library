unit qtx.codec.url;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  W3C.TypedArray,
  qtx.codec,
  qtx.sysutils,
  qtx.memory,
  qtx.classes;

type

  TQTXCodecURLMode = (umCompatible, umBuiltIn);

  TQTXCodecURL = class(TQTXCodec)
  private
    // internal method used by EncodeRawBytesToString to process
    // a fixed amount [chunk] of data
    function __ProcessChunk(const Buffer: TUint8Array): string;
  protected
    // IMPLEMENTS :: IQTXCodecProcess
    procedure EncodeData(const Source, Target: IManagedData); override;
    procedure DecodeData(const Source, Target: IManagedData); override;
    function MakeCodecInfo: TQTXCodecInfo; override;

    class function URLEncodeUTF8Bytes(const Data: TUint8Array): string; static;
  public
    // uses built-in methods (faster)
    function  Encode(TextToEncode: string): string;
    function  Decode(TextToDecode: string): string;

    // Performs URL encoding directly to array of uint8
    function  EncodeToBytes(const TextToEncode: string): TUint8Array;

    // This will encode a byte-buffer without any checking
    function EncodeRawBytesToString(Data: TUint8Array): string;

    // Change mode to switch between QTX implementation (more compatible)
    // and built-in version (faster). Please note that under node.js you
    // either have to expose decodeURIComponent etc. or stick to the QTX
    // version of the encode/decode methods.
    property  Mode: TQTXCodecURLMode;
  end;

  function JDecodeURIComponent(const text: string): string; external "decodeURIComponent";
  function JEncodeURIComponent(const text: string): string; external "encodeURIComponent";

implementation

var
  HexTable__ : array[0..255] of char = (
     '%00', '%01', '%02', '%03', '%04', '%05', '%06', '%07',
     '%08', '%09', '%0a', '%0b', '%0c', '%0d', '%0e', '%0f',
     '%10', '%11', '%12', '%13', '%14', '%15', '%16', '%17',
     '%18', '%19', '%1a', '%1b', '%1c', '%1d', '%1e', '%1f',
     '%20', '%21', '%22', '%23', '%24', '%25', '%26', '%27',
     '%28', '%29', '%2a', '%2b', '%2c', '%2d', '%2e', '%2f',
     '%30', '%31', '%32', '%33', '%34', '%35', '%36', '%37',
     '%38', '%39', '%3a', '%3b', '%3c', '%3d', '%3e', '%3f',
     '%40', '%41', '%42', '%43', '%44', '%45', '%46', '%47',
     '%48', '%49', '%4a', '%4b', '%4c', '%4d', '%4e', '%4f',
     '%50', '%51', '%52', '%53', '%54', '%55', '%56', '%57',
     '%58', '%59', '%5a', '%5b', '%5c', '%5d', '%5e', '%5f',
     '%60', '%61', '%62', '%63', '%64', '%65', '%66', '%67',
     '%68', '%69', '%6a', '%6b', '%6c', '%6d', '%6e', '%6f',
     '%70', '%71', '%72', '%73', '%74', '%75', '%76', '%77',
     '%78', '%79', '%7a', '%7b', '%7c', '%7d', '%7e', '%7f',
     '%80', '%81', '%82', '%83', '%84', '%85', '%86', '%87',
     '%88', '%89', '%8a', '%8b', '%8c', '%8d', '%8e', '%8f',
     '%90', '%91', '%92', '%93', '%94', '%95', '%96', '%97',
     '%98', '%99', '%9a', '%9b', '%9c', '%9d', '%9e', '%9f',
     '%a0', '%a1', '%a2', '%a3', '%a4', '%a5', '%a6', '%a7',
     '%a8', '%a9', '%aa', '%ab', '%ac', '%ad', '%ae', '%af',
     '%b0', '%b1', '%b2', '%b3', '%b4', '%b5', '%b6', '%b7',
     '%b8', '%b9', '%ba', '%bb', '%bc', '%bd', '%be', '%bf',
     '%c0', '%c1', '%c2', '%c3', '%c4', '%c5', '%c6', '%c7',
     '%c8', '%c9', '%ca', '%cb', '%cc', '%cd', '%ce', '%cf',
     '%d0', '%d1', '%d2', '%d3', '%d4', '%d5', '%d6', '%d7',
     '%d8', '%d9', '%da', '%db', '%dc', '%dd', '%de', '%df',
     '%e0', '%e1', '%e2', '%e3', '%e4', '%e5', '%e6', '%e7',
     '%e8', '%e9', '%ea', '%eb', '%ec', '%ed', '%ee', '%ef',
     '%f0', '%f1', '%f2', '%f3', '%f4', '%f5', '%f6', '%f7',
     '%f8', '%f9', '%fa', '%fb', '%fc', '%fd', '%fe', '%ff');

//############################################################################
// TQTXCodecURL
//############################################################################

function TQTXCodecURL.MakeCodecInfo: TQTXCodecInfo;
begin
  result := TQTXCodecInfo.Create;

  var LVersion := TQTXCodecVersionInfo.Create(0, 1, 0);

  var LAccess := result as ICodecInfo;
  LAccess.SetName("URLCodec");
  LAccess.SetMime("text/url-encoded");
  LAccess.SetVersion(LVersion);
  LAccess.SetDataFlow([cdRead, cdWrite]);

  LAccess.SetInput(cdText);   // Consumes text
  LAccess.SetOutput(cdText);  // Emits text
end;

function TQTXCodecURL.__ProcessChunk(const Buffer: TUint8Array): string;
begin
  var I := 0;
  while I < Buffer.length do
  begin
    result += HexTable__[ Buffer[i] ];
    Inc(I);
  end;
end;

function TQTXCodecURL.EncodeRawBytesToString(Data: TUint8Array): string;
begin
  if Data.length > 0 then
  begin
    while Data.length > 0 do
    begin
      var LChunkSize := 1024;
      if LChunkSize > Data.length then
        LChunkSize := Data.length;

      if LChunkSize > 0 then
      begin
        var LChunk := Data.Copy(0, LChunkSize);
        Data.Delete(0, LChunkSize);
        result += __ProcessChunk(LChunk);
      end else
      break;
    end;

  end;
end;

function TQTXCodecURL.Encode(TextToEncode: string): string;
begin
  case Mode of
  umCompatible:
    result := JEncodeURIComponent(TextToEncode);
  umBuiltIn:
    begin
      if TextToEncode.length > 0 then
        result := BytesToString( EncodeToBytes(TextToEncode) );
    end;
  end;
end;

function TQTXCodecURL.EncodeToBytes(const TextToEncode: string): TUint8Array;
begin
  if TextToEncode.length > 0 then
  begin
    var LBytes := StringToBytes(TextToEncode);
    var LTemp := TManagedMemory.Create;
    try
      var LAccess := LTemp as IManagedData;
      LTemp.Append(LBytes);
      LBytes := [];

      EncodeData(LAccess, LAccess);
      result := LTemp.ToBytes();
    finally
      LTemp.free;
    end;
  end;
end;

function TQTXCodecURL.Decode(TextToDecode: string): string;
begin
  case Mode of
  umCompatible:
    result := JDecodeURIComponent(TextToDecode);
  umBuiltIn:
    begin
      if TextToDecode.length > 0 then
      begin
        var LBytes := StringToBytes(TextToDecode);
        var LTemp := TManagedMemory.Create;
        try
          var LAccess := LTemp as IManagedData;
          LTemp.Append(LBytes);
          LBytes := [];

          DecodeData(LAccess, LAccess);
          result := BytesToString( LTemp.ToBytes() );
        finally
          LTemp.free;
        end;
      end;
    end;
  end;
end;

class function TQTXCodecURL.URLEncodeUTF8Bytes(const Data: TUint8Array): string;
begin
  // Ensure that there is data to process
  if Data.length > 0 then
  begin
    var I := 0;
    while I < Data.length do
    begin
      var LSafe := (Data[I] in [65..90]) or (Data[I] in [97..122]) or (Data[I] in [48..57]) or (Data[I] = 95);
      case LSafe of
      true:   result += TString.FromCharCode( Data[i] );
      false:  result += HexTable__[ Data[i] ];
      end;
      Inc(I);
    end;
  end;
end;

procedure TQTXCodecURL.EncodeData(const Source, Target: IManagedData);
begin
  var LData := Source.ToBytes();
  if LData.length > 0 then
    Target.FromBytes( StringToBytes( URLEncodeUTF8Bytes(LData) )  );
end;

procedure TQTXCodecURL.DecodeData(const Source, Target: IManagedData);
begin
  var LData := Source.ToBytes();
  if LData.length > 0 then
    Target.FromBytes( StringToBytes( JDecodeURIComponent( TString.decodeUTF8(LData) )));
end;

initialization
begin
  CodecManager.RegisterCodec(TQTXCodecURL);
end;

end.
