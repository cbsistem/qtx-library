unit qtx.codec.utf8;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  W3C.TypedArray,
  qtx.codec,
  qtx.sysutils,
  qtx.memory,
  qtx.classes;

type

  TQTXBomTypeUTF = (
    bomNone,    // No byte-order-mark found
    bomUTF8,    // UTF8 identified
    bomUTF16,   // UTF16 identified
    bomUTF32    // UTF32 identified
    );

  TQTXByteOrderMarkUTF8 = class
  public
    function  CheckUTF8(const Bytes: TUInt8Array): boolean;
    function  CheckUTF16(const Bytes: TUInt8Array): boolean;
    function  CheckUTF32(const Bytes: TUInt8Array): boolean;
    function  StripBOM(const Bytes: TUInt8Array): TUInt8Array;
    function  GetUTF8BOM: TUInt8Array;
    function  GetUTF16BOM: TUInt8Array;
    function  GetUTF32BOM: TUInt8Array;
    function  CheckBOM(const Bytes: TUInt8Array): TQTXBomTypeUTF;
  end;

  TQTXCodecUTF8 = class(TQTXCodec)
  private
    class function  CanUseClampedArray: boolean; static;
    class function  CanUseNativeConverter: boolean; static;
  protected

    procedure EncodeData(const Source, Target: IManagedData); override;
    procedure DecodeData(const Source, Target: IManagedData); override;

    function MakeCodecInfo: TQTXCodecInfo; override;
  public
    function Encode(TextToEncode: string): TUInt8Array; virtual;
    function Decode(const BytesToDecode: TUInt8Array): string; virtual;
  end;

implementation


//############################################################################
// TQTXByteOrderMarkUTF8
//############################################################################

function TQTXByteOrderMarkUTF8.GetUTF8BOM: TUInt8Array;
begin
  result := [$EF,$BB,$BF];
end;

function TQTXByteOrderMarkUTF8.GetUTF16BOM: TUInt8Array;
begin
  result := [$FE, $FF];
end;

function TQTXByteOrderMarkUTF8.GetUTF32BOM: TUInt8Array;
begin
  result := [$00, $00, $FE, $FF];
end;

function TQTXByteOrderMarkUTF8.CheckUTF8(const Bytes: TUInt8Array): boolean;
begin
  if Bytes.length >= 3 then
  begin
    // Check for both endian notations
    result := ( (Bytes[0] = $EF) and (Bytes[1] = $BB) and (Bytes[2] = $BF) )
           or ( (Bytes[0] = $BF) and (Bytes[1] = $BB) and (Bytes[2] = $EF) );
  end;
end;

function TQTXByteOrderMarkUTF8.CheckUTF16(const Bytes: TUInt8Array): boolean;
begin
  if Bytes.Length >= 2 then
  begin
    // Check for both endian notations
    result := ( (bytes[0] = $FE) and (bytes[1] = $FF) )
           or ( (bytes[0] = $FF) and (bytes[1] = $FE) );
  end;
end;

function TQTXByteOrderMarkUTF8.CheckUTF32(const Bytes: TUInt8Array): boolean;
begin
  if Bytes.Length >= 4 then
  begin
    // Check for both endian notations
    result := ( (bytes[0] = $00) and (bytes[1] = $00) and (bytes[2] = $Fe) and (bytes[3] = $FF) )
           or ( (bytes[0] = $FF) and (bytes[1] = $FE) and (bytes[2] = $00) and (bytes[3] = $00) );
  end;
end;

function TQTXByteOrderMarkUTF8.CheckBOM(const Bytes: TUInt8Array): TQTXBomTypeUTF;
begin
  result := bomNone;
  if CheckUTF8(bytes)  then result := bomUTF8 else
  if CheckUTF16(bytes) then result := bomUTF16 else
  if CheckUTF32(bytes) then result := bomUTF32;
end;

function TQTXByteOrderMarkUTF8.StripBOM(const Bytes: TUInt8Array): TUInt8Array;
begin
  result := bytes;
  if CheckUTF8(Bytes)  then result.delete(0, 3) else
  if CheckUTF16(Bytes) then result.delete(0, 2) else
  if CheckUTF32(Bytes) then result.delete(0, 4);
end;

//############################################################################
// TBase64Codec
//############################################################################

function TQTXCodecUTF8.MakeCodecInfo: TQTXCodecInfo;
begin
  result := TQTXCodecInfo.Create;

  var LVersion := TQTXCodecVersionInfo.Create(0, 2, 0);

  var LAccess := result as ICodecInfo;
  LAccess.SetName("UTF8Codec");
  LAccess.SetMime("text/utf8");
  LAccess.SetVersion(LVersion);
  LAccess.SetDataFlow([cdRead, cdWrite]);

  LAccess.SetInput(cdText);     // Consumes text [non encoded bytes]
  LAccess.SetOutput(cdBinary);  // Emits binary [UTF8 encoded bytes]
end;

class function TQTXCodecUTF8.CanUseClampedArray: boolean;
begin
  var LTemp: THandle;
  try
    LTemp  := new JUint8ClampedArray(10);
  except
    on e: exception do
      exit;
  end;
  if (LTemp) then
    result := true;
end;

class function TQTXCodecUTF8.CanUseNativeConverter: boolean;
begin
  var LTemp: JTextEncoder;
  try
    LTemp := new JTextEncoder("utf8");
  except
    on e: exception do
      exit(false);
  end;
  result := LTemp <> nil;
end;

function TQTXCodecUTF8.Encode(TextToEncode: string): TUInt8Array;
begin
  // Make sure there is something to convert
  if TextToEncode.Length < 1 then
    exit([]);

  // Check if browser supports native codec objects
  case CanUseNativeConverter() of
  true:
    begin
      // Use native codec object
      var LEncoder := new JTextEncoder("utf8");
      var LTyped := LEncoder.encode(TextToEncode);
      result := TypedArrayToBytes(LTyped);
      LEncoder := nil;
      LTyped := nil;
    end;
  false:
    begin
      // Use JS based implementation

      var LClip: JIntegerTypedArray;
      var LTemp: JIntegerTypedArray;

      case CanUseClampedArray() of
      true:
        begin
          LClip := new JUint8ClampedArray(1);
          LTemp := new JUint8ClampedArray(1);
        end;
      false:
        begin
          LClip := new JUint8Array(1);
          LTemp := new JUint8Array(1);
        end;
      end;

      for var n := TextToEncode.low to TextToEncode.high do
      begin
        LClip[0] := TString.CharCodeFor( TextToEncode[n] );
        if (LClip[0] < 128) then
        begin
          result += LClip[0];
        end else
        if ( (LClip[0] > 127) and (LClip[0] < 2048) ) then
        begin
          LTemp[0] := (LClip[0] shr 06) or 192;
          result += LTemp[0];

          LTemp[0] := (LClip[0] and 63) or 128;
          result += LTemp[0];
        end else
        begin
          LTemp[0] := (LClip[0] shr 12) or 224;
          result += LTemp[0];

          LTemp[0] := ((LClip[0] shr 6) and 63) or 128;
          result += LTemp[0];

          result += (LClip[0] and 63) or 128;
          result += LTemp[0];
        end;
      end;

    end;
  end;
end;

function TQTXCodecUTF8.Decode(const BytesToDecode: TUInt8Array): string;
begin
  // Make sure there is something to convert
  if BytesToDecode.Length < 1 then
    exit("");

  // Check if browser supports native codec objects
  case CanUseNativeConverter() of
  true:
    begin
      // use native codec
      var LDecoder := new JTextDecoder("utf8");
      var LTyped := BytesToTypedArray(BytesToDecode);
      result := LDecoder.decode(JUint8Array(LTyped));
      LTyped := nil;
      LDecoder := nil;
    end;
  false:
    begin
      // use JS implementation
      var i := 0;
      var bytelen := BytesToDecode.length;
      while (i < bytelen) do
      begin
        var c := BytesToDecode[i];
        inc(i);

        if c < 128 then
        begin
          result += TString.FromCharCode(c);
        end else

        if ((c > 191) and (c < 224)) then
        begin
          var c2 := BytesToDecode[i]; inc(i);
          result += TString.FromCharCode( ((c and 31) shl 6) or c2 and 63 );
        end else

        if ( (c > 239) and (c < 365) ) then
        begin
          var c2 := BytesToDecode[i]; inc(i);
          var c3 := BytesToDecode[i]; inc(i);
          var c4 := BytesToDecode[i]; inc(i);

          var u :=((c and 7) shl 18)
            or ((c2 and 63) shl 12)
            or ((c3 and  63) shl 6)
            or (c4 and 63) - $10000;

          result += TString.FromCharCode( $D800 + (u shr 10));
          result += TString.FromCharCode( $DC00 + (u and 1023));
        end else
        begin
          var c2 := BytesToDecode[i]; inc(i);
          var c3 := BytesToDecode[i]; inc(i);
          result += TString.FromCharCode(((c and 15) shl 12) or ((c2 and 63) shl 6 or (c3 and 63)));
        end;
      end;

    end;
  end;
end;

// Note: We should just use a View here to extract a valid string
// we have no idea how odd languages behave
procedure TQTXCodecUTF8.EncodeData(const Source, Target: IManagedData);
begin
end;

procedure TQTXCodecUTF8.DecodeData(const Source, Target: IManagedData);
begin
end;

initialization
begin
  CodecManager.RegisterCodec(TQTXCodecUTF8);
end;

end.
