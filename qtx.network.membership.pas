unit qtx.network.membership;

//#############################################################################
// Quartex RTL for DWScript
// Written by Jon L. Aasenden, all rights reserved
// This code is released under modified LGPL (see license.txt)
//#############################################################################

interface

uses
  qtx.sysutils,
  qtx.logfile,
  qtx.classes;

type

  // Forward declarations
  TQTXHostMemberships  = class;
  TQTXHostMembership   = class;

  //Exception classes
  EQTXHostMembershipError = class(EQTXException);

  TQTXHostMembership = class(TQTXOwnedLockedObject)
  private
    FHost:      string;
  protected
    function    GetOwner: TQTXHostMemberships; reintroduce;
    function    AcceptOwner(const CandidateObject: TObject): boolean; override;

    procedure   SetHost(const NewHost: string); virtual;
  public
    property    Owner: TQTXHostMemberships read GetOwner;
    property    Host: string read FHost write SetHost;
    constructor Create(const AOwner: TQTXHostMemberships); reintroduce; virtual;
  end;

  TQTXHostMembershipList = array of TQTXHostMembership;

  IQTXHostMemberships = interface
    ['{2C227CD1-BC69-4107-A8FB-D0E3A50CD3C0}']
    function    GetMemberCount: int32;
    function    GetMember(const index: int32): TQTXHostMembership;

    function    Add: TQTXHostMembership;
    function    AddEx(Addresse: string): TQTXHostMembership;
    function    IndexOf(const Item: TQTXHostMembership): int32;

    function    FindByHost(Address: string): TQTXHostMembership;

    procedure   Delete(const index: int32);
    procedure   DeleteEx(const Item: TQTXHostMembership);
    procedure   Clear;
  end;

  TQTXHostMemberships = class(TQTXLockedObject, IQTXHostMemberships)
  private
    FItems:     TQTXHostMembershipList;
    function    GetMemberCount: int32;
    function    Getmember(const index: int32): TQTXHostMembership;

  protected
    procedure   ObjectLocked; override;
    procedure   ObjectUnLocked; override;

  public
    property    Locked;
    property    Items[const index: int32]: TQTXHostMembership read GetMember; default;
    property    Count: int32 read GetMemberCount;

    function    Add: TQTXHostMembership; virtual;
    function    AddEx(Addresse: string): TQTXHostMembership; virtual;
    function    IndexOf(const Item: TQTXHostMembership): int32; virtual;

    function    FindByHost(Address: string): TQTXHostMembership; virtual;

    procedure   Delete(const index: uint32); virtual;
    procedure   DeleteEx(const Item: TQTXHostMembership); virtual;
    procedure   Clear; virtual;

    destructor  Destroy; override;
  end;


implementation

//############################################################################
// TQTXHostMemberships
//############################################################################

destructor TQTXHostMemberships.Destroy;
begin
  if FItems.Count > 0 then
    Clear();
  inherited;
end;

procedure TQTXHostMemberships.Clear;
begin
  if not Locked then
  begin
    try
      for var x := 0 to FItems.Count-1 do
      begin
        FItems[x].free;
        FItems[x] := nil;
      end;
    finally
      FItems.Clear();
    end;
  end else
  raise EQTXLockError.Create('Membership cannot be altered while active error');
end;

function TQTXHostMemberships.GetMemberCount: int32;
begin
  result := FItems.Count;
end;

function TQTXHostMemberships.GetMember(const index: int32): TQTXHostMembership;
begin
  result := FItems[index];
end;

function TQTXHostMemberships.FindByHost(Address: String): TQTXHostMembership;
begin
  Address := Address.ToLower().Trim();
  if Address.length > 0 then
  begin
    for var x := 0 to FItems.Count-1 do
    begin
      var LItem := FItems[x];
      if LItem.FHost = Address then
      begin
        result := LItem;
        break;
      end;
    end;
  end;
end;

function TQTXHostMemberships.IndexOf(const Item: TQTXHostMembership): int32;
begin
  if Item <> nil then
  begin
    result := FItems.indexOf(Item);
  end else
  raise EQTXHostMembershipError.CreateFmt('%s failed, item was NIL error', [{$I %FUNCTION%}]);
end;

function TQTXHostMemberships.Add: TQTXHostMembership;
begin
  if not Locked then
  begin
    result := TQTXHostMembership.Create(self);
    FItems.add(result);
  end else
  raise EQTXHostMembershipError.CreateFmt('%s failed, memberships cannot be altered while active error', [{$I %FUNCTION%}]);
end;

function TQTXHostMemberships.AddEx(Addresse: string): TQTXHostMembership;
begin
  if not Locked then
  begin
    if FindByHost(Addresse) <> nil then
      raise EQTXHostMembershipError.CreateFmt('Failed to add membership, host [%d] is already in pool', [Addresse]);

    result := TQTXHostMembership.Create(self);
    result.Host := Addresse;
    FItems.add(result);
  end else
  raise EQTXHostMembershipError.CreateFmt('%s failed, bindings cannot be altered while active error', [{$I %FUNCTION%}]);
end;

procedure TQTXHostMemberships.Delete(const index: int32);
begin
  if not Locked then
  begin
    try
      FItems.Delete(Index);
    except
      on e: exception do
      raise EQTXHostMembershipError.CreateFmt
      ("Failed to delete membership, system threw exception %s with message %s", [e.classname, e.message]);
    end;
  end else
  raise EQTXLockError.Create('Memberships cannot be altered while active error');
end;

procedure TQTXHostMemberships.DeleteEx(const Item: TQTXHostMembership);
begin
  if Item <> nil then
  begin
    Delete( FItems.IndexOf(Item) );
  end else
  raise EQTXHostMembershipError.CreateFmt('%s failed, item was NIL error', [{$I %FUNCTION%}]);
end;

procedure TQTXHostMemberships.ObjectLocked;
begin
  for var x := 0 to FItems.Count-1 do
  begin
    var LAccess := (FItems[x] as IQTXLockObject);
    LAccess.DisableAlteration();
  end;
end;

procedure TQTXHostMemberships.ObjectUnLocked;
begin
  for var x := 0 to FItems.Count-1 do
  begin
    var LAccess := (FItems[x] as IQTXLockObject);
    LAccess.EnableAlteration();
  end;
end;

//############################################################################
// TQTXHostMembership
//############################################################################

constructor TQTXHostMembership.Create(const AOwner: TQTXHostMemberships);
begin
  inherited Create(AOwner);
end;

function TQTXHostMembership.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  result := assigned(CandidateObject) and (CandidateObject is TQTXHostMemberships);
end;

function TQTXHostMembership.GetOwner: TQTXHostMemberships;
begin
  result := TQTXHostMemberships(inherited Owner);
end;

procedure TQTXHostMembership.SetHost(const NewHost: string);
begin
  if NewHost <> FHost then
  begin
    if not Locked then
      FHost := NewHost
    else
      raise EQTXLockError.Create('Property cannot be altered while active error');
  end;
end;

end.
